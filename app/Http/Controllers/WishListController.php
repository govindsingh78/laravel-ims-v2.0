<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 5/24/2017
 * Time: 10:12 PM
 */
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\PurchaseOrder;
use App\Vendors;
use App\Product;
use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\gstsetting;
use App\warranty;
use App\orderinvoices;
use App\site;
use App\floor;
use App\location;
use Session;
use DB;
use PDF;
 
class WishListController extends Controller
{
    public function index()
    {
        $manageVendor  = Vendors::all();
        $manageProduct = Product::all();
        $manageBrand = brand::all();
        $manageSpecification = specification::all();
        $manageSitelocation = sitelocation::all();
        $manageGst = gst::all();
        $manageWarranty = warranty::all(); 
        if(request()->ajax())
        {
            $items = [];
            $wish_list = app('wishlist');
            $wish_list->getContent()->each(function($item) use (&$items)
            {
                $items[] = $item;
            });
            return response(array(
                'success' => true,
                'data' => $items,
                'message' => 'wishlist get items success'
            ),200,[]);
        }
        else{
            $statusPo = orderinvoices::where([
                'invoiceid' => Session::get('invoiceid'),
                'status' => 1
            ])->get();

        if(count($statusPo) > 0){
            Session::forget('invoiceid'); 
            Session::forget('88uuiioo99888_cart_items');
         }

         $sites = DB::table("sites")->pluck("name","id");
         return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty, 'sites' => $sites] );
        }
    }

    public function add()
    {
        $wish_list = app('wishlist');
        $id = request('id');
        $price = request('price');
        $specification = request('specification');
        $qty = request('qty');
        $gst = request('gst');
        $vendor = request('vendor');
        $brand = request('brand');
        $sitelocation1 = request('sitelocation');
        $warranty = request('warranty');
        $site = request('site');
        $floor = request('floor');
        $location = request('location');
        $invoicedate =   date('d/m/Y', strtotime(request('invoicedate')));
        $companyname_req = request('companyname');
        $delivaryinstruction = request('delivaryinstruction');
        $manageCs = gstsetting::find($companyname_req);
        $companyname = $manageCs->company_name;
        $gstno = $manageCs->gst_no;
        $companyaddress = $manageCs->company_address;
        $vendoraddress = request('vendoraddress');
        $manageProduct  = Product::find($id);
        $manageVendor  = Vendors::find($vendor);
        $manageBrand = brand::find($brand);
        $manageSitelocation = sitelocation::find($sitelocation1);

        if(!empty($warranty)){
            $manageWarranty = warranty::find($warranty); 
            $managewarrantyvalue = $manageWarranty -> warranty_in_years;
        }else{
            $managewarrantyvalue = 0;
        }

        $manageSite = site::find($site);
        $sitelocation = $manageSite->name;
        $delivarylocation = $manageSite->name;
        $invoiceid = "#12345";
           
        if(Session::get('qty_'.$id) == null || Session::get('qty_'.$id) == -1){
            Session::put('qty_'.$id, $qty);
            Session::get('qty_'.$id);
        }else{
            $initial_qty = Session::get('qty_'.$id);
            Session::put('qty_'.$id, $initial_qty + $qty);
            Session::get('qty_'.$id);
        }

        if(Session::get('invoiceid') == null || Session::get('invoiceid') == -1){
            $today = date('YmdHi');
            $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
            $range = $today - $startDate;
            $rand = rand(0, $range);
            $sid = ($startDate + $rand);
            Session::put('invoiceid', $sid);
            $manageProduct  = Product::all();
            foreach ( $manageProduct as $key => $mp) {
                $mpid = $mp->id;
                Session::forget('qty_'.$mpid);
            }
         
        }
        if(!empty($specification)){
              $specificationofdata = DB::table('specifications')
                    ->whereIn('id', $specification)->get(); 
              $arr = "";
              for ($i = 0; $i < count($specificationofdata); $i++) {
                    $j = $i+1;
              $arr .= $j." ) ".$specificationofdata[$i]->description." ";
        }
              $specification = $arr;
        }else{
              $specification = "";
        }
            
             $invoiceidpo = Session::get('invoiceid');
             $gstamountperitem = $gst/100 * ($price* Session::get('qty_'.$id));
             $totalperitem = ($price* Session::get('qty_'.$id)) + $gstamountperitem;

        $wish_list->add($id, $manageProduct -> name, $price, $qty, $gst,  $manageBrand -> name, $manageVendor -> name,  $managewarrantyvalue ,  $sitelocation,  $specification, $invoiceid, $invoicedate, $companyname, $gstno, $companyaddress, $manageVendor -> address, $delivarylocation, $delivaryinstruction, $invoiceidpo,$gstamountperitem, $totalperitem,  array());


        // return response()->json(array($id, $manageProduct -> name, $price, $qty, $gst,  $manageBrand -> name, $manageVendor -> name,  $managewarrantyvalue ,  $sitelocation,  $specification, $invoiceid, $invoicedate, $companyname, $gstno, $companyaddress, $manageVendor -> address, $delivarylocation, $delivaryinstruction, $invoiceidpo,$gstamountperitem, $totalperitem));
        // die;
          
        $session_var = serialize(session()->get('88uuiioo99888_cart_items'));
        $dataList = orderinvoices::where('invoiceid',Session::get('invoiceid'))->first();
            if($dataList!=null){
               DB::table('orderinvoices')
                ->where('invoiceid', Session::get('invoiceid'))
                ->update(['session_var' => $session_var]);
            }else{
                //insert data for the first time
                $orderinvoices = new orderinvoices();
                $orderinvoices->invoiceid = Session::get('invoiceid');
                $orderinvoices->session_var = $session_var;
                $orderinvoices->save();
            }

    }

    public function delete($id)
    {
        $wish_list = app('wishlist');
        if($wish_list->remove($id) == true){
            Session::forget('qty_'.$id);
        $session_var = serialize(session()->get('88uuiioo99888_cart_items'));
            DB::table('orderinvoices')
            ->where('invoiceid', Session::get('invoiceid'))
            ->update(['session_var' => $session_var]);
        }

        return response(array(
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed."
        ),200,[]);
    }



    public function saveinvoice()
    {
               
                $manageVendor  = Vendors::all();
                $manageProduct = Product::all();
                $manageBrand = brand::all();
                $manageSpecification = specification::all();
                $manageSitelocation = sitelocation::all();
                $manageGst = gst::all();
                $manageWarranty = warranty::all();  

                //bring total quantity
                $totalQuantity = 0;
                $cartArray = Session::get('88uuiioo99888_cart_items');
                foreach ($cartArray as $key => $value) {
                  $totalQuantity += $value->quantity;

                }

                $countMailDt = DB::table('orderinvoices')
                ->where('invoiceid', Session::get('invoiceid'))
                ->first();

                 $mailStatus = $countMailDt->mail_status;

                $poidapprovalStatus = Session::get('invoiceid');
                $countDt = DB::table('purchase_order_approves')
                ->where('poid', $poidapprovalStatus)
                ->count();
                if($countDt > 0){
                $getDt = DB::table('purchase_order_approves')
                ->select('approval_status', 'updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();
                $getDtStatus = $getDt->approval_status;
                }else{
                $getDtStatus = 0;
                $getDt = DB::table('purchase_order_approves')
                ->select('approval_status', 'updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();

                }
                $poapprovestatus = $getDtStatus;





                
                $affectedRows = DB::table('orderinvoices')
                ->where('invoiceid', Session::get('invoiceid'))
                ->update(['totalQuantity' => $totalQuantity, 'status' => 1]);

                 $extractedRows = DB::table('orderinvoices')
                ->where('invoiceid', Session::get('invoiceid'))
                ->first();
                 
                if($affectedRows > 0){
                    return view('cartdetail')->with( ['invoiceid' => Session::get('invoiceid'), 'stock_status' => $extractedRows->stock_status,'poapprovestatus' => $poapprovestatus, 'mailStatus' => $mailStatus, 'getDt' => $getDt]);    
                }else{
                      Session::forget('invoiceid'); 
                      Session::forget('88uuiioo99888_cart_items');
                      return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty] ); 
                }

                return response(array(
                'success' => $affectedRows,
                'data' => Session::get('invoiceid'),
                'message' => "saved invoice"
                ),200,[]);

    }


    public function movetostock($id){
      
                $manageVendor  = Vendors::all();
                $manageProduct = Product::all();
                $manageBrand = brand::all();
                $manageSpecification = specification::all();
                $manageSitelocation = sitelocation::all();
                $manageGst = gst::all();
                $manageWarranty = warranty::all();  
                $affectedRows = DB::table('orderinvoices')
                ->where('invoiceid', $id)
                ->update(['stock_status' => 1]);

                $extractedRows = DB::table('orderinvoices')
                ->where('invoiceid', $id)
                ->first();
 
                if($affectedRows > 0){
                    return view('cartdetail')->with( ['invoiceid' => $id, 'stock_status' => $extractedRows->stock_status] );    
                }else{

                      return view('cartdetail')->with(['invoiceid' => $id, 'stock_status' => $extractedRows->stock_status]); 
                }
                
                return response(array(
                'success' => $affectedRows,
                'data' => Session::get('invoiceid'),
                'message' => "saved invoice"
                ),200,[]);

    }


    public function details()
    {
        $wish_list = app('wishlist');
        return response(array(
            'success' => true,
            'data' => array(
                'total_quantity' => $wish_list->getTotalQuantity(),
                'sub_total' => $wish_list->getSubTotal(),
                'total' => $wish_list->getTotal(),
            ),
            'message' => "Get wishlist details success."
        ),200,[]);
    }
}