<?php 
namespace App\Http\Controllers;
use App\PurchaseOrder;
use App\Vendors;
use App\Product;
use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;
use App\orderinvoices; 
use App\stockdatas;
use App\site;
use App\floor;
use App\location;
use App\stockmovement;
use App\availablestock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use DB;
use Session;
use PDF;
use Mail;

class ManageStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');

    }

   ###### ---- function used for managing purchase order starts ---- ######

    public function indexStock(Request $request)
    {


        ### New Approach ###


       $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));

        $request->session()->put('stockId', $request->has('stockId') ? $request->get('stockId') : ($request->session()->has('stockId') ? $request->session()->get('stockId') : -1));

        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'stockId'));

        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));


         $request->session()->put('typefield', $request->has('typefield') ? $request->get('typefield') : ($request->session()->has('typefield') ? $request->session()->get('typefield') : 'stockId'));

       

         if (Session::get('search') != -1 || Session::get('stockId') != -1){

           // $stockmovements = DB::table('stockmovements')
           //      ->select('stockmovements.id as smId','stockmovements.stockdatas_id as sdId','stockmovements.site_moved_to as movedSiteId','stockmovements.floor_moved_to as movedFloorId','stockmovements.location_moved_to as movedLocationId','sites.id as siteID','sites.name as siteName','floors.id as floorID','floors.name as floorName','locations.id as locationID','locations.name as locationName', 'stockmovements.created_at as created_at', 'stockmovements.updated_at as dateMoved')
           //      ->join('stockdatas', 'stockdatas.id', '=', 'stockmovements.stockdatas_id')
           //      ->join('sites', 'sites.id', '=', 'stockmovements.site_moved_to')
           //      ->join('floors', 'floors.id', '=', 'stockmovements.floor_moved_to')
           //      ->join('locations', 'locations.id', '=', 'stockmovements.location_moved_to')

           //      ->where('stockmovements.stockdatas_id', '=',   $id)
               
           //      ->orderBy('stockmovements.id', 'DESC')
           //      ->get();




           $stockdatas = DB::table('stockdatas')
                ->select('stockdatas.id as stockId','stockdatas.poid as stockPoid','stockdatas.receiveddate as receivedDate','stockdatas.productname as productId','stockdatas.serialno as serialNo','stockdatas.vendorname as vendorId','stockdatas.warrantyyears as warrantyYrs','stockdatas.site as siteId','stockdatas.floor as floorId','stockdatas.location as locationId','products.id as prodID','products.name as prodName','vendors.id as vendID','vendors.name as vendName', 'orderinvoices.id as orderID', 'orderinvoices.invoiceid as invoiceID', 'stockdatas.created_at as created_at', 'sites.id as siteIdn', 'floors.id as floorIdn', 'locations.id as locationIdn', 'sites.name as site_moved_to', 'floors.name as floor_moved_to', 'locations.name as location_moved_to')
                ->leftjoin('orderinvoices', 'orderinvoices.id', '=', 'stockdatas.poid')
                ->leftjoin('products', 'products.id', '=', 'stockdatas.productname')
                ->leftjoin('vendors', 'vendors.id', '=', 'stockdatas.vendorname')
                
                ->leftjoin('sites', 'sites.id', '=', 'stockdatas.site')
                ->leftjoin('floors', 'floors.id', '=', 'stockdatas.floor')
                ->leftjoin('locations', 'locations.id', '=', 'stockdatas.location')
                ->where('orderinvoices.invoiceid', '=',   $request->session()->get('name'))
                ->orwhere('orderinvoices.invoiceid', 'like', '%' . $request->session()->get('search') . '%')
                ->orwhere('products.name', 'like', '%' . $request->session()->get('search') . '%')
                ->orwhere('vendors.name', 'like', '%' . $request->session()->get('search') . '%')
                ->orderBy('stockdatas.id', $request->session()->get('sort'))
                ->paginate(5);

               

               } 
                else{
                
                $stockdatas = DB::table('stockdatas')
                ->select('stockdatas.id as stockId','stockdatas.poid as stockPoid','stockdatas.receiveddate as receivedDate','stockdatas.productname as productId','stockdatas.serialno as serialNo','stockdatas.vendorname as vendorId','stockdatas.warrantyyears as warrantyYrs','stockdatas.site as siteId','stockdatas.floor as floorId','stockdatas.location as locationId','products.id as prodID','products.name as prodName','vendors.id as vendID','vendors.name as vendName', 'orderinvoices.id as orderID', 'orderinvoices.invoiceid as invoiceID', 'stockdatas.created_at as created_at', 'sites.id as siteIdn', 'floors.id as floorIdn', 'locations.id as locationIdn', 'sites.name as site_moved_to', 'floors.name as floor_moved_to', 'locations.name as location_moved_to')
               ->leftjoin('orderinvoices', 'orderinvoices.id', '=', 'stockdatas.poid')
                ->leftjoin('products', 'products.id', '=', 'stockdatas.productname')
                ->leftjoin('vendors', 'vendors.id', '=', 'stockdatas.vendorname')
                
                ->leftjoin('sites', 'sites.id', '=', 'stockdatas.site')
                ->leftjoin('floors', 'floors.id', '=', 'stockdatas.floor')
                ->leftjoin('locations', 'locations.id', '=', 'stockdatas.location')
                ->orderBy('stockdatas.id', $request->session()->get('sort'))
                ->paginate(5);


            }

       
            //dd($stockdatas);



        if ($request->ajax())
        return view('indexStock', compact('stockdatas'));
        else
        return view('ajaxStock', compact('stockdatas'));

        ### New Approach ###
        

    }

    public function createStock(Request $request)
    {
     
        //All Tables Data in order to manage category & their respective values

                $manageVendor  = Vendors::all();
                $manageProduct = Product::all();
                $manageBrand = brand::all();
                $manageSpecification = specification::all();
                $manageSitelocation = sitelocation::all();
                $manageGst = gst::all();
                $manageWarranty = warranty::all(); 



               //dd($manageVendor);

        
        if ($request->isMethod('get'))
            //return view('formPurchaseOrder');
            return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty] );
        else {




            $rules = [

                'product_id' => 'required',
                'vendor_id' => 'required',
                'brand_id' => 'required',
                'specification_ids' => 'required',
                'qty' => 'required',
                'unit_price' => 'required',
                'gst_id' => 'required',
                'total_price' => 'required',
                'warranty_id' => 'required',
                'site_location_id' => 'required',
                'stock_status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);


                //Here we will be manipulating respective values
                $nameVendor  = Vendors::find($request->vendor_id);
                $nameProduct = Product::find($request->product_id);
                $nameBrand = brand::find($request->brand_id);
                $nameSitelocation = sitelocation::find($request->site_location_id);
                $valueGst = gst::find($request->gst_id);
                $valueWarranty = warranty::find($request->warranty_id); 
               // $valueSpecification = specification::find($request->specification_ids); 

                $purchaseOrder = new PurchaseOrder();
                $purchaseOrder->product_id = $request->product_id;
                $purchaseOrder->product_name = $nameProduct->name;
                $purchaseOrder->vendor_id = $request->vendor_id;
                $purchaseOrder->vendor_name = $nameVendor->name;
                $purchaseOrder->brand_id = $request->brand_id;
                $purchaseOrder->brand_name = $nameBrand->name;

                $purchaseOrder->specification_ids = $request->specification_ids;
                $purchaseOrder->qty = $request->qty;

                $purchaseOrder->unit_price = $request->unit_price;


                $purchaseOrder->gst_id = $request->gst_id;
                $purchaseOrder->gst_value = $valueGst->gst_in_percentage;

                
                $purchaseOrder->total_price = $request->total_price;
                $purchaseOrder->warranty_id = $request->warranty_id;
                $purchaseOrder->warranty_value = $valueWarranty->warranty_in_years;
                $purchaseOrder->site_location_id = $request->site_location_id;
                $purchaseOrder->site_location_name = $nameSitelocation->site." ".$nameSitelocation->floor." ".$nameSitelocation->location;
                $purchaseOrder->stock_status = $request->stock_status;
                
                $purchaseOrder->save();

            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }  

        
    }

    public function deleteStock($id)
    {
        orderinvoices::destroy($id);
        return redirect('/manage-stock');
    }

    public function updateStock(Request $request, $id)
    {



        //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($id);


        //var_dump($orderinvoices); die();
        


        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);


               


        if ($request->isMethod('get'))
            return view('cartdetail', ['purchaseOrder' => PurchaseOrder::find($id), 'manageVendor' => Vendors::all(), 'manageProduct' => Product::all(), 'manageBrand' => brand::all(), 'manageSpecification' => specification::all(), 'manageSitelocation' => sitelocation::all(),  'manageGst' => gst::all(),  'manageWarranty' => warranty::all(), 'invoiceid' => $orderinvoices->invoiceid,  'stock_status' => $orderinvoices->stock_status]);

        else {
            $rules = [
              
                'stock_status' => 'required',
               
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $purchaseOrder = PurchaseOrder::find($id);
            $purchaseOrder->stock_status = $request->stock_status;
            $purchaseOrder->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-stock')
            ]);
        }
    }

 
########## ----- Function starts for mannual entry of stock Starts ----- #######


     public function addStockItem(Request $request)
    {
     


            $vendorname  = Vendors::all();
            $warrantyyears  = warranty::orderBy('warranty_in_years', 'ASC')->get();
            $productname = Product::all();
            $poid  = orderinvoices::all();
            $sites = site::all()->pluck('name', 'id');




            
            
        if ($request->isMethod('get'))
            return view('formStockItem', ['sites' => $sites, 'poid' => $poid, 'productname' => $productname, 'vendorname' => $vendorname, 'warrantyyears' => $warrantyyears]);
        else {
            $rules = [
                'poid' => 'required',
                'receiveddate' => 'required',
                'productname' => 'required',
                'vendorname' => 'required',
                'site' => 'required',
                'floor' => 'required',
                'location' => 'required',
                     
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);


            //dd($request->receiveddate);


            $stockdata = new stockdatas();
            $stockdata->poid = $request->poid;
            $stockdata->receiveddate = $request->receiveddate;
            $stockdata->productname = $request->productname;
            $stockdata->serialno = $request->serialno;
            $stockdata->vendorname = $request->vendorname;
            $stockdata->warrantyyears = $request->warrantyyears;
            $stockdata->site = $request->site;
            $stockdata->floor = $request->floor;
            $stockdata->location = $request->location;
            $stockdata->save();

            //get the id just created in stockdata table
            //update case add new entry to stockmovements table
            $stockdatas_id = $stockdata->id;
            $site_moved_to = $request->site;
            $floor_moved_to = $request->floor;
            $location_moved_to = $request->location;

            $stockmovements = new stockmovement();
            $stockmovements->stockdatas_id = $stockdatas_id;
            $stockmovements->site_moved_to = $site_moved_to;
            $stockmovements->floor_moved_to = $floor_moved_to;
            $stockmovements->location_moved_to = $location_moved_to;
            $stockmovements->save();




            //request poid #59 get received quantity and add +1 each time

            $getReceivedQty = DB::table('orderinvoices')
            ->select('receivedStock')
            ->where('id', '=', $request->poid)
            ->first();

            $receivedStock = $getReceivedQty->receivedStock + 1;
 

             $updateReceivedQty = DB::table('orderinvoices')
            ->where('id', $request->poid)
            ->update(['receivedStock' => $receivedStock]);










            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-stock')
            ]);
        }  

        
    }

    public function deleteStockItem($id)
    {


        $getStockPoid = stockdatas::find($id);
        $poidVal = $getStockPoid->poid;
        $getReceivedQty = DB::table('orderinvoices')
        ->select('receivedStock')
        ->where('id', '=', $poidVal)
        ->first();

        $receivedStock = $getReceivedQty->receivedStock - 1;


        $updateReceivedQty = DB::table('orderinvoices')
        ->where('id', $poidVal)
        ->update(['receivedStock' => $receivedStock]);


         stockdatas::destroy($id);

        return redirect('/manage-stock');
    }

    public function updateStockItem(Request $request, $id)
    {
       

            $vendorname  = Vendors::all();
            $warrantyyears  = warranty::orderBy('warranty_in_years', 'ASC')->get();
            $productname = Product::all();
            $poid  = orderinvoices::all();
            $sites = site::all()->pluck('name', 'id');
            $floors = floor::all()->pluck('name', 'id');
            $locations = location::all()->pluck('name', 'id');
            $stock_datas  = stockdatas::find($id);



          






     
        if ($request->isMethod('get'))
            return view('formStockItem', ['sites' => $sites, 'poid' => $poid, 'productname' => $productname, 'vendorname' => $vendorname, 'warrantyyears' => $warrantyyears,'stock_datas' => $stock_datas, 'floors' => $floors, 'locations' => $locations]);
        else {
            $rules = [
                'poid' => 'required',
                
                'productname' => 'required',
                'vendorname' => 'required',
                'site' => 'required',
                'floor' => 'required',
                'location' => 'required',
                     
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);


            $stockdata = stockdatas::find($id);
            $stockdata->poid = $request->poid;
            //$stockdata->receiveddate = $request->receiveddate;
            $stockdata->productname = $request->productname;
            $stockdata->serialno = $request->serialno;
            $stockdata->vendorname = $request->vendorname;
            $stockdata->warrantyyears = $request->warrantyyears;
            $stockdata->site = $request->site;
            $stockdata->floor = $request->floor;
            $stockdata->location = $request->location;
            $stockdata->save();



            //update case add new entry to stockmovements table
            $stockdatas_id = $id;
            $site_moved_to = $request->site;
            $floor_moved_to = $request->floor;
            $location_moved_to = $request->location;

            $stockmovements = new stockmovement();
            $stockmovements->stockdatas_id = $stockdatas_id;
            $stockmovements->site_moved_to = $site_moved_to;
            $stockmovements->floor_moved_to = $floor_moved_to;
            $stockmovements->location_moved_to = $location_moved_to;
            $stockmovements->save();





             //request poid #59 get received quantity and add +1 each time








            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-stock')
            ]);
        }  

    }



    ########### --- Function starts for mannual entry of stock Ends --- #########



     public function viewStockMovement(Request $request, $id)
    {

            $stockmovements = DB::table('stockmovements')
                ->select('stockmovements.id as smId','stockmovements.stockdatas_id as sdId','stockmovements.site_moved_to as movedSiteId','stockmovements.floor_moved_to as movedFloorId','stockmovements.location_moved_to as movedLocationId','sites.id as siteID','sites.name as siteName','floors.id as floorID','floors.name as floorName','locations.id as locationID','locations.name as locationName', 'stockmovements.created_at as created_at', 'stockmovements.updated_at as dateMoved')
                ->join('stockdatas', 'stockdatas.id', '=', 'stockmovements.stockdatas_id')
                ->join('sites', 'sites.id', '=', 'stockmovements.site_moved_to')
                ->join('floors', 'floors.id', '=', 'stockmovements.floor_moved_to')
                ->join('locations', 'locations.id', '=', 'stockmovements.location_moved_to')

                ->where('stockmovements.stockdatas_id', '=',   $id)
               
                ->orderBy('stockmovements.id', 'DESC')
                ->get();



            // make one more query here for seperate detail for all the data from stock data table


                $stockdatashistory = DB::table('stockdatas')
                ->select('stockdatas.id as stockId','stockdatas.poid as stockPoid','stockdatas.receiveddate as receivedDate','stockdatas.productname as productId','stockdatas.serialno as serialNo','stockdatas.vendorname as vendorId','stockdatas.site as siteId','stockdatas.floor as floorId','stockdatas.location as locationId','products.id as prodID','products.name as prodName','vendors.id as vendID','vendors.name as vendName', 'orderinvoices.id as orderID', 'orderinvoices.invoiceid as invoiceID', 'stockdatas.created_at as created_at', 'stockdatas.serialno as serialno')
                ->join('orderinvoices', 'orderinvoices.id', '=', 'stockdatas.poid')
                ->join('products', 'products.id', '=', 'stockdatas.productname')
                ->join('vendors', 'vendors.id', '=', 'stockdatas.vendorname')
                ->where('stockdatas.id', '=',   $id)
                ->get();


               

                //select warranty in years seperately

                  $stockdatasfirst = DB::table('stockdatas')
                  ->where('id', '=',   $id)
                  ->first();

                  $warnty_id = $stockdatasfirst->warrantyyears;

                  if($warnty_id !== null){
                        $stockdataslast = DB::table('warranties')
                        ->where('id', '=',   $warnty_id)
                        ->first();

                        $wiy =  $stockdataslast->warranty_in_years;
                  }else{
                         $wiy =  "N/A";
                  }
                  


               
     
             if ($request->isMethod('get')){
                return view('formStockMoved', ['stockmovements' => $stockmovements, 'stockdatashistory' => $stockdatashistory, 'wiy' => $wiy]);
   
             }
             
        ### New Approach ###
           
    }


 public function deleteStockMoved($id)
    {
        stockmovement::destroy($id);
        return redirect('/manage-stock');
    }



public function ajaxRequestPost()

    {

        
        $prodid = request()->prodid;
        $poorderid = request()->poorderid;
        $totalQty = request()->totalQty;
        $availqnty = request()->availqnty;

        // create a table availablestock
 


            // $stockUnitSaved = availablestock::updateOrCreate(['poorderid' => request()->poorderid], 
            // ['prodid' => request()->prodid],['totalQty' => request()->totalQty],['availqnty' => request()->availqnty]);



            $isDuplicate = DB::table('availablestocks')->where('poorderid',$poorderid)->where('prodid',$prodid)->count();

            if($isDuplicate > 0){
            
                $availablestock = DB::table('availablestocks')->where('poorderid',$poorderid)->where('prodid',$prodid)->update(['availqnty'=>$availqnty]);


            }else{
            $availablestock = new availablestock();
            $availablestock->poorderid = $poorderid;
            $availablestock->prodid = $prodid;
            $availablestock->totalQty = $totalQty;
            $availablestock->availqnty = $availqnty;
            $availablestock->save();
            }
        
 
       
        return response()->json(['success'=>'Stock Updated Successfully !!']);

    }


    public function checkStockStatus()

    {
        
        $prodid = request()->prodid;
        $poorderid = request()->poorderid;
        $totalQty = request()->totalQty;
        $availqnty = request()->availqnty;

        $iscountable = DB::table('availablestocks')
        ->where('poorderid',$poorderid)
        ->where('prodid',$prodid)
        ->count();

        if($iscountable > 0){

                $isCompleteorNot = DB::table('availablestocks')
                ->where('poorderid',$poorderid)
                ->where('prodid',$prodid)
                ->get();


                if($isCompleteorNot[0]->totalQty == $isCompleteorNot[0]->availqnty){

                $complete = "<span style='color: green'>Complete Stock(s) ".$isCompleteorNot[0]->availqnty."/".$isCompleteorNot[0]->totalQty." Units Received from Vendor.</span>";

                }else{
                $complete = "<span style='color: #ca8523'>Partial Stock(s) ".$isCompleteorNot[0]->availqnty."/".$isCompleteorNot[0]->totalQty." Units Received from Vendor.</span>";
                }

        }else{
                 $complete = "<span style='color: red'>No Stock Units Received from Vendor.</span>";
        }
            
        return response()->json(['success'=>$complete]);

    }


    public function checkAjaxforPoid(){

        Session::forget('88uuiioo99888_cart_items');
        Session::forget('invoiceid');

        $prodid = request()->prodid;
        $orderinvoices  = orderinvoices::find($prodid);
        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);

        return response()->json(['success'=>'changing data']);

    }




public function checkStockReceived(){

        $poid = request()->poid;

        //get total number of item for specific po

        $isPoid = DB::table('orderinvoices')
            ->where('invoiceid',$poid)
            ->first();


        $poidGet = $isPoid->totalQuantity;

         
        $isCounted = DB::table('availablestocks')
            ->where('poorderid',$poid)
            ->count();

        if($isCounted !== 0){

        
            $getAvailStock = DB::table('availablestocks')->select(DB::raw('SUM(availqnty) as availqnty'))
              ->where('poorderid',$poid)
              ->groupBy('poorderid')
              ->get();


            

            $availqnty = $getAvailStock[0]->availqnty;
 

            if($availqnty < $poidGet){

                $isStatus = $availqnty."/".$poidGet." Received";
                $isStatusCode = 1;
            }else{

                $isStatus = $availqnty."/".$poidGet." Received";
                $isStatusCode = 2;
            }

 

        }else{

            $isStatus = "0/".$poidGet." Received";
            $isStatusCode = 0;
        }

        return response()->json(['success'=>$isStatus,'statuscode'=>$isStatusCode]);

    }






    public function checkAjaxforStockValidation(){

        $poidforajax = request()->poidforajax;
        $productid = request()->productid;

        //find po id #232323233233232
        $orderinvoices  = orderinvoices::find($poidforajax);
        $invoiceid      = $orderinvoices->invoiceid;

        //check if poid and prodct id has stocks added to availablestocks using invoice id and product id

        $isCounted = DB::table('availablestocks')
            ->where('poorderid',$invoiceid)
            ->where('prodid',$productid)
            ->count();

        if($isCounted !== 0){

            $getAvailStock = availablestock::where('poorderid','=',$invoiceid)
            ->where('prodid','=',$productid)
            ->first();

            $totalQty = $getAvailStock->totalQty;
            $availqnty = $getAvailStock->availqnty;


            $countAddedStock = DB::table('stockdatas')
            ->where('poid',$poidforajax)
            ->where('productname',$productid)
            ->count();

            if($countAddedStock < $availqnty){

                $isStatus = "Please add the product unit to stock !!";
                $isStatusCode = 1;
            }else{

                $isStatus = "Sorry you have already added all the item(s) to stock.";
                $isStatusCode = 0;
            }

 

        }else{

            $isStatus = "We have not received this product unit for this specific #PO if we did please make sure to update stock recieved by visiting [Purchase Order >> view >> Stock received >> Received]";
            $isStatusCode = 0;
        }

        return response()->json(['success'=>$isStatus,'statuscode'=>$isStatusCode]);

    }


     public function ajaxMail(){

        $emails = request()->mailMultiple;
        $email = request()->email;

        $newEmail_0 = array_filter(array_values($emails));
        $newEmail_1 = array_filter(array_values($email));
        ///merge array and email value together 



        $description = request()->description;
        $poorderid = request()->poorderid;
        $title = "Purchase Order No. #".$poorderid." for Approval - HK Department Dotsquares";
        $orderinvoices=DB::table('orderinvoices')->Where('invoiceid',$poorderid)->first();
        $pid      = $orderinvoices->id;
        $linktoaccess = url("/manage-approval-process/poforApproval/{$pid}");

        //to mail section
        Mail::send('approval_email', ['linktoaccess' => $linktoaccess,  'title' => $title, 'description' => $description], function ($message) use ($newEmail_0, $poorderid) {

             $message->to($newEmail_0)->subject("Purchase Order No. #".$poorderid." for Approval - HK Department Dotsquares");


        });


        //cc mail section
        Mail::send('approval_email', ['linktoaccess' => $linktoaccess,  'title' => $title, 'description' => $description], function ($message) use ($newEmail_1, $poorderid) {

             $message->cc($newEmail_1)->subject("Purchase Order No. #".$poorderid." for Approval - HK Department Dotsquares");


        });










        // $email = request()->email;
        // $description = request()->description;
        // $poorderid = request()->poorderid;
        // $title = "Purchase Order No. #".$poorderid." Approval - HK Department Dotsquares Pvt. Ltd.";
        // $orderinvoices=DB::table('orderinvoices')->Where('invoiceid',$poorderid)->first();
        // $pid      = $orderinvoices->id;
        // $linktoaccess = url("/manage-approval-process/poforApproval/{$pid}");


        // Mail::send('approval_email', ['linktoaccess' => $linktoaccess, 'email' => $email, 'title' => $title, 'description' => $description], function ($message) use ($email, $poorderid) {

        //     $message->to($email)->subject("Purchase Order No. #".$poorderid." Approval - HK Department Dotsquares Pvt. Ltd.");
        // });


        //update mail status

        $availablestock = DB::table('orderinvoices')->where('id',$pid)->update(['mail_status'=>1]);


        return response()->json(['success'=>'Mail has been sent for Approval!']);

    }







 ###### ---- function used for managing purchase order  ends ---- ######
}
