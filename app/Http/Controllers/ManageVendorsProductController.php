<?php
namespace App\Http\Controllers;
use App\ManageVendorsProduct;

use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;

use App\gstsetting;

use App\warranty;

use App\site;
use App\floor;
use App\location;


use App\mailsetting;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;


use DB;
use Session;


class ManageVendorsProductController extends Controller
{



     public function __construct()
    {
        $this->middleware('auth');
    }


   ###### ---- function used for managing vendors category starts ---- ######

    public function index(Request $request)
    {

        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $vendor = new Vendors();
            if(Schema::hasColumn('vendors', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('name') != -1)
                $vendor = $vendor->where('name', $request->session()->get('name'));

                $vendor = $vendor->where('name', 'like', '%' . $request->session()->get('search') . '%')
                ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
                ->paginate(5);

             
            }else{

                    
                    if ($request->session()->get('name') != -1)
                    $vendor = $vendor->where('name', $request->session()->get('name'));

                    $vendor = $vendor->where('name', 'like', '%' . $request->session()->get('search') . '%')
                    ->orderBy('id', $request->session()->get('sort'))
                    ->paginate(5);

            }

      


        if ($request->ajax())
            return view('index', compact('vendor'));
        else
            return view('ajax', compact('vendor'));
    }

    public function create(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('form');
        else {
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
                'contact_no' => 'required|numeric',
                'status' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $vendor = new Vendors();
            $vendor->name = $request->name;
            $vendor->email = $request->email;
            $vendor->address = $request->address;
            $vendor->contact_no = $request->contact_no;
            $vendor->status = $request->status;
            $vendor->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-vendor-and-product-category')
            ]);
        }  

        
    }

    public function delete($id)
    {
        Vendors::destroy($id);
        return redirect('/manage-vendor-and-product-category');
    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('form', ['vendor' => Vendors::find($id)]);
        else {
            $rules = [
               'name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
                'contact_no' => 'required|numeric',
                'status' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $vendor = Vendors::find($id);
            $vendor->name = $request->name;
            $vendor->email = $request->email;
            $vendor->address = $request->address;
            $vendor->contact_no = $request->contact_no;
            $vendor->status = $request->status;
            $vendor->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-vendor-and-product-category')
            ]);
        }
    }


 ###### ---- function used for managing vendors category ends ---- ######



 ###### ---- function used for managing product category starts ---- ######

    public function indexProduct(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $product = new Product();





     if(Schema::hasColumn('products', $request->session()->get('field')))  {
                
        if ($request->session()->get('name') != -1)
            $product = $product->where('name', $request->session()->get('name'));

             $product = $product->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
        } else{

            if ($request->session()->get('name') != -1)
            $product = $product->where('name', $request->session()->get('name'));

             $product = $product->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);

        }


        if ($request->ajax())
            return view('indexProduct', compact('product'));
        else
            return view('ajaxProduct', compact('product'));
    }

    public function createProduct(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formProduct');
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
            
            $product->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-product-category')
            ]);
        }  

        
    }

    public function deleteProduct($id)
    {
        Product::destroy($id);
        return redirect('/manage-product-category');
    }

    public function updateProduct(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formProduct', ['product' => Product::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $product = Product::find($id);
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
             
            $product->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-product-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ######





   ###### ---- function used for managing brand category starts ---- ######

    public function indexBrand(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $brand = new brand();



         if(Schema::hasColumn('brands', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('name') != -1)
            $brand = $brand->where('name', $request->session()->get('name'));

        $brand = $brand->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);

             
            }else{

                  if ($request->session()->get('name') != -1)
            $brand = $brand->where('name', $request->session()->get('name'));

        $brand = $brand->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }



      



        if ($request->ajax())
            return view('indexBrand', compact('brand'));
        else
            return view('ajaxBrand', compact('brand'));
    }

    public function createBrand(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formBrand');
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $brand = new brand();
            $brand->name = $request->name;
            $brand->description = $request->description;
            $brand->status = $request->status;
            
            $brand->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-brand-category')
            ]);
        }  

        
    }

    public function deleteBrand($id)
    {
        brand::destroy($id);
        return redirect('/manage-brand-category');
    }

    public function updateBrand(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formBrand', ['brand' => brand::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $brand = brand::find($id);
            $brand->name = $request->name;
            $brand->description = $request->description;
            $brand->status = $request->status;
             
            $brand->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-brand-category')
            ]);
        }
    }


###### ---- function used for managing brand category Ends ---- ###### 






     ###### ---- function used for managing specification category starts ---- ######

    public function indexSpecification(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $specification = new specification();
         $product = Product::all();
       




             if(Schema::hasColumn('specifications', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('name') != -1)
            $specification = $specification->where('name', $request->session()->get('name'));

        $specification = $specification->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);

             
            }else{

                 if ($request->session()->get('name') != -1)
            $specification = $specification->where('name', $request->session()->get('name'));

        $specification = $specification->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }


        if ($request->ajax())
            return view('indexSpecification', compact('specification', 'product'));
        else
            return view('ajaxSpecification', compact('specification', 'product'));
    }

    public function createSpecification(Request $request)
    {
     
     //echo "create"; die;
         $product = Product::all();
         
        if ($request->isMethod('get'))
            return view('formSpecification', compact('product'));
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'related_to_product_id' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $specification = new specification();
            $specification->name = $request->name;
            $specification->description = $request->description;
            $specification->related_to_product_id = $request->related_to_product_id;
            $specification->status = $request->status;
            
            $specification->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-specification-category')
            ]);
        }  

        
    }

    public function deleteSpecification($id)
    {
        specification::destroy($id);
        return redirect('/manage-specification-category');
    }

    public function updateSpecification(Request $request, $id)
    {

         
        if ($request->isMethod('get'))
            return view('formSpecification', ['specification' => specification::find($id), 'product' => Product::all()]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'related_to_product_id' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $specification = specification::find($id);
            $specification->name = $request->name;
            $specification->description = $request->description;
            $specification->related_to_product_id = $request->related_to_product_id;
            $specification->status = $request->status;
            $specification->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-specification-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 


      ###### ---- function used for managing sitelocation category starts ---- ######

    public function indexSitelocation(Request $request)
    {


        ######################################################


       $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));

        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));

        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));

        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));

        if($request->session()->get('field') == 'site' || $request->session()->get('field') == 'location' || $request->session()->get('field') == 'floor'){
         $request->session()->put('typefield', $request->has('typefield') ? $request->get('typefield') : ($request->session()->has('typefield') ? $request->session()->get('typefield') : 'site'));
        }else{
            $request->session()->put('typefield', 'site');
        }

     // echo $request->session()->get('field'); die;
      
            
    if(Schema::hasColumn('sites', $request->session()->get('field')) || Schema::hasColumn('floors', $request->session()->get('field')) || Schema::hasColumn('locations', $request->session()->get('field'))) {

         //if (Session::get('search') != -1 || Session::get('name') != -1){

           $sitelocation = DB::table('sites')
                ->select('sites.id as siteID','floors.id as floorID','locations.id as locationID','sites.name as siteName','floors.name as floorName', 'locations.name as locationName', 'floors.site_id as floorSiteid', 'locations.floor_id as locationFloorid')
                ->join('floors', 'floors.site_id', '=', 'sites.id')
                ->join('locations', 'locations.floor_id', '=', 'floors.id')
                ->where('sites.name', '=',   $request->session()->get('name'))
                ->orwhere('sites.name', 'like', '%' . $request->session()->get('search') . '%')
                ->orwhere('floors.name', 'like', '%' . $request->session()->get('search') . '%')
                ->orwhere('locations.name', 'like', '%' . $request->session()->get('search') . '%')
                ->orderBy($request->session()->get('typefield').'s.'.$request->session()->get('field'), $request->session()->get('sort'))
                ->paginate(5);

       // }  

}
        else{
         
              $sitelocation = DB::table('sites')
                ->select('sites.id as siteID','floors.id as floorID','locations.id as locationID','sites.name as siteName','floors.name as floorName', 'locations.name as locationName', 'floors.site_id as floorSiteid', 'locations.floor_id as locationFloorid')
                ->join('floors', 'floors.site_id', '=', 'sites.id')
                ->join('locations', 'locations.floor_id', '=', 'floors.id')
                ->orderBy('locations.id', $request->session()->get('sort'))
                ->paginate(5);


            }

       

       
          
        if ($request->ajax())
        return view('indexSitelocation', compact('sitelocation'));
        else
        return view('ajaxSitelocation', compact('sitelocation'));

               
    }

    public function createSitelocation(Request $request)
    {
     
          
        if ($request->isMethod('get'))
            return view('formSitelocation');
        else {
            $rules = [
                'site' => 'required',
                'location' => 'required',
                'floor' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);



            $site_data = new site();
            $site_data->name = $request->site;
            $site_data->save();
            $site_data->id;
            if(!empty($site_data->id)){

            $floor_data = new floor();
            $floor_data->name = $request->floor;
            $floor_data->site_id = $site_data->id;
            $floor_data->save();
            $floor_data->id;

                
            }
            
            if(!empty($floor_data->id)){

            $location_data = new location();
            $location_data->name = $request->location;
            $location_data->floor_id = $floor_data->id;
            $location_data->save();
            $location_data->id;

            }
          
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-sitelocation-category')
            ]);
        }  

        
    }
    public function addSite(Request $request)
    {
     
          
        if ($request->isMethod('get'))
            return view('formSite');
        else {
            $rules = [
                'site' => 'required',
                                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);



            $site_data = new site();
            $site_data->name = $request->site;
            $site_data->save();
                     
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-sitelocation-category')
            ]);
        }  

        
    }

    public function addFloor(Request $request)
    {
     
        
         $sites = site::all()->pluck('name', 'id');


        if ($request->isMethod('get'))
            return view('formFloor', compact('sites'));
        else {
            $rules = [
                'site' => 'required',
                'floor' => 'required',
               
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);



           

            $floor_data = new floor();
            $floor_data->name = $request->floor;
            $floor_data->site_id = $request->site;
            $floor_data->save();
             

                
         
          
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-sitelocation-category')
            ]);
        }  

        
    }

    public function addLocation(Request $request)
    {
     
         $sites = site::all()->pluck('name', 'id');
          
        if ($request->isMethod('get'))
            return view('formLocation', compact('sites'));
        else {
            $rules = [
                'site' => 'required',
                'floor' => 'required',
                'location' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
 
            $location_data = new location();
            $location_data->name = $request->location;
            $location_data->floor_id = $request->floor;
            $location_data->save();
            
            
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-sitelocation-category')
            ]);
        }  

        
    }

    public function deleteSitelocation($id)
    {
        location::destroy($id);
        return redirect('/manage-sitelocation-category');
    }

    public function updateSitelocation(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formSitelocation', ['sitelocation' => sitelocation::find($id)]);
        else {
            $rules = [
                'site' => 'required',
                'location' => 'required',
                'floor' => 'required',
                 'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $sitelocation = sitelocation::find($id);
            $sitelocation->site = $request->site;
            $sitelocation->location = $request->location;
            $sitelocation->floor = $request->floor;
            $sitelocation->status = $request->status;
            $sitelocation->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-sitelocation-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 


 ###### ---- function used for managing gst category starts ---- ######

    public function indexGst(Request $request)
    {




        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('gst_in_percentage', $request->has('gst_in_percentage') ? $request->get('gst_in_percentage') : ($request->session()->has('gst_in_percentage') ? $request->session()->get('gst_in_percentage') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $gst = new gst();
      

          if(Schema::hasColumn('gsts', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('gst_in_percentage') != -1)
            $gst = $gst->where('gst_in_percentage', $request->session()->get('gst_in_percentage'));

        $gst = $gst->where('gst_in_percentage', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                 if ($request->session()->get('gst_in_percentage') != -1)
            $gst = $gst->where('gst_in_percentage', $request->session()->get('gst_in_percentage'));

        $gst = $gst->where('gst_in_percentage', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }

        if ($request->ajax())
            return view('indexGst', compact('gst'));
        else
            return view('ajaxGst', compact('gst'));
    }




    public function createGst(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formGst');
        else {
            $rules = [
                'gst_in_percentage' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gst = new gst();
            $gst->gst_in_percentage = $request->gst_in_percentage;
            $gst->status = $request->status;
                    
            $gst->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-gst-category')
            ]);
        }  

        
    }

    public function deleteGst($id)
    {
        gst::destroy($id);
        return redirect('/manage-gst-category');
    }

    public function updateGst(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formGst', ['gst' => gst::find($id)]);
        else {
            $rules = [
                'gst_in_percentage' => 'required',
                'status' => 'required',
                 
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gst = gst::find($id);
             
            $gst->gst_in_percentage = $request->gst_in_percentage;
            $gst->status = $request->status;
           
            $gst->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-gst-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 















###### ---- function used for managing gst setting starts ---- ######

    public function indexGstSetting(Request $request)
    {




        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('company_name', $request->has('company_name') ? $request->get('company_name') : ($request->session()->has('company_name') ? $request->session()->get('company_name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $gstSetting = new gstsetting();
      

          if(Schema::hasColumn('gstsetting', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('company_name') != -1)
            $gstSetting = $gstSetting->where('company_name', $request->session()->get('company_name'));

        $gstSetting = $gstSetting->where('company_name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                 if ($request->session()->get('company_name') != -1)
            $gstSetting = $gstSetting->where('company_name', $request->session()->get('company_name'));

        $gstSetting = $gstSetting->where('company_name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }

        if ($request->ajax())
            return view('indexGstSetting', compact('gstSetting'));
        else
            return view('ajaxGstSetting', compact('gstSetting'));
    }


     

    public function createGstSetting(Request $request)
    {
     
              
        if ($request->isMethod('get'))
            return view('formGstSetting');
        else {
            $rules = [
                'company_name' => 'required',
                'gst_no' => 'required',
                'company_address' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gstSetting = new gstsetting();
            $gstSetting->company_name = $request->company_name;
            $gstSetting->gst_no = $request->gst_no;
            $gstSetting->company_address = $request->company_address;
            $gstSetting->status = $request->status;
                    
            $gstSetting->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-gst-setting')
            ]);
        }  

        
    }

    public function deleteGstSetting($id)
    {
        gstsetting::destroy($id);
        return redirect('/manage-gst-setting');
    }

    public function updateGstSetting(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formGstSetting', ['gstSetting' => gstsetting::find($id)]);
        else {
            $rules = [
                'company_name' => 'required',
                'gst_no' => 'required',
                'company_address' => 'required',
                'status' => 'required',
                 
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gstSetting = gstsetting::find($id);
             
            $gstSetting->company_name = $request->company_name;
            $gstSetting->gst_no = $request->gst_no;
            $gstSetting->company_address = $request->company_address;
            $gstSetting->status = $request->status;
           
            $gstSetting->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-gst-setting')
            ]);
        }
    }


###### ---- function used for managing gst setting ends ---- ###### 






















    ###### ---- function used for managing warranties category starts ---- ######

    public function indexWarranty(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('warranty_in_years', $request->has('warranty_in_years') ? $request->get('warranty_in_years') : ($request->session()->has('warranty_in_years') ? $request->session()->get('warranty_in_years') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $warranty = new warranty();
      

             if(Schema::hasColumn('warranties', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('warranty_in_years') != -1)
            $warranty = $warranty->where('warranty_in_years', $request->session()->get('warranty_in_years'));

        $warranty = $warranty->where('warranty_in_years', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                   if ($request->session()->get('warranty_in_years') != -1)
            $warranty = $warranty->where('warranty_in_years', $request->session()->get('warranty_in_years'));

        $warranty = $warranty->where('warranty_in_years', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }


        if ($request->ajax())
            return view('indexWarranty', compact('warranty'));
        else
            return view('ajaxWarranty', compact('warranty'));
    }

    public function createWarranty(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formWarranty');
        else {
            $rules = [
                'warranty_in_years' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $warranty = new warranty();
            $warranty->warranty_in_years = $request->warranty_in_years;
            $warranty->status = $request->status;
                    
            $warranty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-warranties-category')
            ]);
        }  

        
    }

    public function deleteWarranty($id)
    {
        warranty::destroy($id);
        return redirect('/manage-warranties-category');
    }

    public function updateWarranty(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formWarranty', ['warranty' => warranty::find($id)]);
        else {
            $rules = [
                'warranty_in_years' => 'required',
                 'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $warranty = warranty::find($id);
             
            $warranty->warranty_in_years = $request->warranty_in_years;
             $warranty->status = $request->status;
            $warranty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-warranties-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 






###### ---- function used for managing emails Ends ---- ###### 

    public function indexMail(Request $request)
    {




        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('email', $request->has('email') ? $request->get('email') : ($request->session()->has('email') ? $request->session()->get('email') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $mail = new mailsetting();
      

          if(Schema::hasColumn('mailsettings', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('email') != -1)
            $mail = $mail->where('email', $request->session()->get('email'));

        $mail = $mail->where('email', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                 if ($request->session()->get('email') != -1)
            $mail = $mail->where('email', $request->session()->get('email'));

        $mail = $mail->where('email', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }

        if ($request->ajax())
            return view('indexMail', compact('mail'));
        else
            return view('ajaxMail', compact('mail'));
    }




    public function createMail(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formMail');
        else {
            $rules = [
                'email' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $mailsetting = new mailsetting();
            $mailsetting->email = $request->email;
            $mailsetting->status = $request->status;
                    
            $mailsetting->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-mail-setting')
            ]);
        }  

        
    }

    public function deleteMail($id)
    {
        mailsetting::destroy($id);
        return redirect('/manage-mail-setting');
    }

    public function updateMail(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formMail', ['mail' => mailsetting::find($id)]);
        else {
            $rules = [
                'email' => 'required',
                'status' => 'required',
                 
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $mailsetting = mailsetting::find($id);
             
            $mailsetting->email = $request->email;
            $mailsetting->status = $request->status;
           
            $mailsetting->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-mail-setting')
            ]);
        }
    }


###### ---- function used for managing emails Ends ---- ###### 
































}




 