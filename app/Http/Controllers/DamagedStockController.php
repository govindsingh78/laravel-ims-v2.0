<?php

namespace App\Http\Controllers;

use App\DamagedStock;
use Illuminate\Http\Request;

class DamagedStockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function __construct()
    {
        $this->middleware('auth');
    }



    public function index()
    {
        return view('damaged-stock');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DamagedStock  $damagedStock
     * @return \Illuminate\Http\Response
     */
    public function show(DamagedStock $damagedStock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DamagedStock  $damagedStock
     * @return \Illuminate\Http\Response
     */
    public function edit(DamagedStock $damagedStock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DamagedStock  $damagedStock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DamagedStock $damagedStock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DamagedStock  $damagedStock
     * @return \Illuminate\Http\Response
     */
    public function destroy(DamagedStock $damagedStock)
    {
        //
    }
}
