<?php
namespace App\Http\Controllers;
use App\PurchaseOrder;


use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;
use App\orderinvoices; 


use App\PurchaseOrderApprove;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Redirect;

use DB;
use Session;
use PDF;

class PurchaseOrderApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        //$this->middleware('auth');

    }

   ###### ---- function used for managing purchase order starts ---- ######

     


    public function poforApproval(Request $request, $id)
    {



        //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($id);
        
        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);


        // send the status

        $poidapprovalStatus = $orderinvoices->invoiceid;

         $countDt = DB::table('purchase_order_approves')
                ->where('poid', $poidapprovalStatus)
                ->count();
        if($countDt > 0){
            $getDt = DB::table('purchase_order_approves')
                ->select('approval_status', 'updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();

         $getDtStatus = $getDt->approval_status;
    
        }else{

            $getDtStatus = 0;
             $getDt = DB::table('purchase_order_approves')
                ->select('approval_status', 'updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();
        }
        
        $poapprovestatus = $getDtStatus;



        if ($request->isMethod('get'))
            return view('cartdetailforApproval', ['purchaseOrder' => PurchaseOrder::find($id), 'manageVendor' => Vendors::all(), 'manageProduct' => Product::all(), 'manageBrand' => brand::all(), 'manageSpecification' => specification::all(), 'manageSitelocation' => sitelocation::all(),  'manageGst' => gst::all(),  'manageWarranty' => warranty::all(), 'invoiceid' => $orderinvoices->invoiceid,  'stock_status' => $orderinvoices->stock_status, 'poapprovestatus' => $poapprovestatus, 'getDt'=>$getDt]);

        else {
            $rules = [
              
                'stock_status' => 'required',
               
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $purchaseOrder = PurchaseOrder::find($id);
            $purchaseOrder->stock_status = $request->stock_status;
            $purchaseOrder->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }
    }

     public function approvePurchase(Request $request){

               $id = $request->approvepoid; 

            if(!is_null($id)) {
      
    
                $countDt = DB::table('purchase_order_approves')
                ->where('poid', $request->approvepoid)
                ->count();
                    if($countDt == 0){
                        $PurchaseOrderApprove = new PurchaseOrderApprove();
                        $PurchaseOrderApprove->poid = $request->approvepoid;
                        $PurchaseOrderApprove->approval_status = 1;
                        $PurchaseOrderApprove->save();
                         // return Redirect::back()->with(['msg', 'PO has been Approved !!']);
                         $request->session()->flash('success', 'PO has been Approved !!');
                         return Redirect::back();
                    }else{
                        $updateDt = DB::table('purchase_order_approves')
                        ->where('poid', $request->approvepoid)
                        ->update(['approval_status'=>1,'updated_at'=>now()]);
                        
                        $request->session()->flash('success', 'PO has been Approved !!');
                        return Redirect::back();
                    }
                
            }   

    }
     public function rejectPurchase(Request $request){

            


                $id = $request->rejectpoid; 

               if(!is_null($id)) {
      
    
                $countDt = DB::table('purchase_order_approves')
                ->where('poid', $request->rejectpoid)
                ->count();
                    if($countDt == 0){
                        $PurchaseOrderApprove = new PurchaseOrderApprove();
                        $PurchaseOrderApprove->poid = $request->rejectpoid;
                        $PurchaseOrderApprove->approval_status = 2;
                        $PurchaseOrderApprove->save();
                         //return Redirect::back()->with(['msg', 'PO has been Rejected !!']);
                         $request->session()->flash('danger', 'PO has been Rejected !!');
                         return Redirect::back();
                    }else{
                        $updateDt = DB::table('purchase_order_approves')
                        ->where('poid', $request->rejectpoid)
                        ->update(['approval_status'=>2, 'updated_at'=>now()]);
                        $request->session()->flash('danger', 'PO has been Rejected !!');
                        return Redirect::back();
                    }
                
            }   
        
    }


    


 ###### ---- function used for managing purchase order  ends ---- ######
}
