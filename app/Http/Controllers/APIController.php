<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\gstsetting;
use App\site;
use App\floor;
use App\location;
use App\Vendors;
use DB;

class APIController extends Controller
{
    public function index(){
        $sites = DB::table("sites")->pluck("name","id");
        return view('formSite', compact('sites'));
    }

    public function getFloorsList(Request $request){
        $floors = DB::table("floors")
                    ->where("site_id",$request->site_id)
                    ->pluck("name","id");
        return response()->json($floors);
    }

    public function getLocationsList(Request $request){
        $locations = DB::table("locations")
                    ->where("floor_id",$request->floor_id)
                    ->pluck("name","id");
        return response()->json($locations);
    }

    public function getSites(){
        $sites = site::all();
        return response(array(
            'success' => true,
            'data' => $sites,
            'message' => 'wishlist get items success 123'
        ),200,[]);
    }

    public function getCompanies(){
        $companies = gstsetting::all();
        return response(array(
            'success' => true,
            'data' => $companies,
            'message' => 'Got list of Companies'
        ),200,[]);
    }

    public function getVendors(){
        $vendors = Vendors::all();
        return response(array(
            'success' => true,
            'data' => $vendors,
            'message' => 'Got list of Vendors'
        ),200,[]);
     }

    public function getFloors(Request $request, $id){
        $floors = floor::where('site_id', '=',$id)->get();
        return response(array(
            'success' => true,
            'data' => $floors,
            'message' => 'wishlist get items success 997987'
        ),200,[]);
    }

    public function getCompaniesDetails(Request $request, $id){
        $companiesdetails = gstsetting::where('id', '=',$id)->get();
        return response(array(
                'success' => true,
                'data' => $companiesdetails,
                'message' => 'Got GST No. & Address'
         ),200,[]);
    }

    public function getVendorsDetails(Request $request, $id){
        $vendoraddresses = Vendors::where('id', '=',$id)->get();
        return response(array(
                'success' => true,
                'data' => $vendoraddresses,
                'message' => 'Got Vendor Address'
        ),200,[]);
    }

    public function getLocations(Request $request, $id){
        $locations = location::where('floor_id', '=',$id)->get();
        return response(array(
                'success' => true,
                'data' => $locations,
                'message' => 'wishlist get items success 997987'
        ),200,[]);
     }
 

}