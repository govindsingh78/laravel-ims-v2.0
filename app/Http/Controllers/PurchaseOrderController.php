<?php
namespace App\Http\Controllers;
use App\PurchaseOrder;


use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;
use App\orderinvoices; 
use App\PurchaseOrderApprove;
use App\mailsetting;
use App\stockmovement;
use App\stockdatas;
use App\availablestock;



use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;

use DB;
use Session;
use PDF;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');

    }

   ###### ---- function used for managing purchase order starts ---- ######

    public function indexPurchaseOrder(Request $request)
    {


        Session::forget('invoiceid'); 
        Session::forget('88uuiioo99888_cart_items');

        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('invoiceid', $request->has('invoiceid') ? $request->get('invoiceid') : ($request->session()->has('invoiceid') ? $request->session()->get('invoiceid') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));


            //$purchaseOrder = new orderinvoices();

            $postatus = PurchaseOrderApprove::all(); 




            if(Schema::hasColumn('orderinvoices', $request->session()->get('field'))) {
               
                // ->leftjoin('availablestocks', 'availablestocks.poorderid', '=', 'orderinvoices.invoiceid')
                // ,'availablestocks.poorderid',DB::raw('SUM(availablestocks.totalQty) as sumtotalQty'), DB::raw('SUM(availablestocks.availqnty) as sumavailqnty')
               $purchaseOrder = DB::table('orderinvoices')
                ->select('orderinvoices.id as id','orderinvoices.invoiceid as invoiceid','orderinvoices.session_var as session_var','orderinvoices.totalQuantity as totalQuantity','orderinvoices.receivedStock as receivedStock','orderinvoices.mail_status as mail_status','orderinvoices.stock_status as stock_status','orderinvoices.status as status','orderinvoices.created_at as created_at',  'purchase_order_approves.poid as poid', 'purchase_order_approves.approval_status as approval_status')
                ->leftjoin('purchase_order_approves', 'purchase_order_approves.poid', '=', 'orderinvoices.invoiceid')
                
                ->where('orderinvoices.invoiceid', '=',   $request->session()->get('invoiceid'))
                ->orwhere('orderinvoices.invoiceid', 'like', '%' . $request->session()->get('search') . '%');
                if($request->session()->get('field') ==  'status'){
                   
                    $purchaseOrder = $purchaseOrder->orderBy('purchase_order_approves.approval_status', $request->session()->get('sort'));
                }else{
                     $purchaseOrder = $purchaseOrder->orderBy('orderinvoices.'.$request->session()->get('field'), $request->session()->get('sort'));
                }
               
               
                $purchaseOrder = $purchaseOrder->get()
                ->groupBy('purchase_order_approves.approval_status'); 
                



                //dd($request->session()->get('field'));
              




                //   $purchaseOrder = DB::table('orderinvoices')
                // ->select('orderinvoices.id as id','orderinvoices.invoiceid as invoiceid','orderinvoices.session_var as session_var','orderinvoices.totalQuantity as totalQuantity','orderinvoices.receivedStock as receivedStock','orderinvoices.mail_status as mail_status','orderinvoices.stock_status as stock_status','orderinvoices.status as status','orderinvoices.created_at as created_at',  'purchase_order_approves.poid as poid', 'purchase_order_approves.approval_status as approval_status','availablestocks.poorderid as poorderid',DB::raw('SUM(availablestocks.totalQty) as sumtotalQty'), DB::raw('SUM(availablestocks.availqnty) as sumavailqnty'))
                // ->leftjoin('purchase_order_approves', 'purchase_order_approves.poid', '=', 'orderinvoices.invoiceid')
                // ->leftjoin('availablestocks', 'availablestocks.poorderid', '=', 'orderinvoices.invoiceid')
                // ->groupBy('availablestocks.poorderid')
                // ->orderBy('orderinvoices.id', $request->session()->get('sort'))
                // ->paginate(5);





 
            }else{

               // ,'availablestocks.poorderid',DB::raw('SUM(availablestocks.totalQty) as sumtotalQty'), DB::raw('SUM(availablestocks.availqnty) as sumavailqnty')
                  // ->leftjoin('availablestocks', 'availablestocks.poorderid', '=', 'orderinvoices.invoiceid')

                $purchaseOrder = DB::table('orderinvoices')
                    ->select('orderinvoices.id as id','orderinvoices.invoiceid as invoiceid','orderinvoices.session_var as session_var','orderinvoices.totalQuantity as totalQuantity','orderinvoices.receivedStock as receivedStock','orderinvoices.mail_status as mail_status','orderinvoices.stock_status as stock_status','orderinvoices.status as status','orderinvoices.created_at as created_at','purchase_order_approves.poid as poid', 'purchase_order_approves.approval_status as approval_status')
                    ->leftjoin('purchase_order_approves', 'purchase_order_approves.poid', '=', 'orderinvoices.invoiceid')
                                    

                    ->orderBy('purchase_order_approves.approval_status', $request->session()->get('sort'))
                    ->get()
                    ->groupBy('purchase_order_approves.approval_status');






                    // $purchaseOrder = DB::table('orderinvoices')
                    // ->select('orderinvoices.id as id','orderinvoices.invoiceid as invoiceid','orderinvoices.session_var as session_var','orderinvoices.totalQuantity as totalQuantity','orderinvoices.receivedStock as receivedStock','orderinvoices.mail_status as mail_status','orderinvoices.stock_status as stock_status','orderinvoices.status as status','orderinvoices.created_at as created_at','purchase_order_approves.poid as poid', 'purchase_order_approves.approval_status as approval_status','availablestocks.poorderid as poorderid',DB::raw('SUM(availablestocks.totalQty) as sumtotalQty'), DB::raw('SUM(availablestocks.availqnty) as sumavailqnty'))
                    // ->leftjoin('purchase_order_approves', 'purchase_order_approves.poid', '=', 'orderinvoices.invoiceid')
                    // ->leftjoin('availablestocks', 'availablestocks.poorderid', '=', 'orderinvoices.invoiceid')
                  
                    // ->groupBy('availablestocks.poorderid')
                    // ->orderBy('orderinvoices.id', $request->session()->get('sort'))
                   
                    
                    // ->paginate(5);

            }


               
      
             $totalPoGenerated = DB::table('orderinvoices')
                           ->whereIn('mail_status', [0,1])
                           ->count();
                
               
                $poApproved = DB::table('purchase_order_approves')
                ->where('approval_status', 1)
                ->count();

                $poRejected = DB::table('purchase_order_approves')
                ->where('approval_status', 2)
                ->count();

                //dd($purchaseOrder);

        if ($request->ajax())
            return view('indexPurchaseOrder', compact('purchaseOrder', 'postatus', 'totalPoGenerated', 'poApproved', 'poRejected'));
        else
            return view('ajaxPurchaseOrder', compact('purchaseOrder','postatus', 'totalPoGenerated', 'poApproved', 'poRejected'));
    }

    public function createPurchaseOrder(Request $request)
    {
     
        //All Tables Data in order to manage category & their respective values

                $manageVendor  = Vendors::all();
                $manageProduct = Product::all();
                $manageBrand = brand::all();
                $manageSpecification = specification::all();
                $manageSitelocation = sitelocation::all();
                $manageGst = gst::all();
                $manageWarranty = warranty::all(); 



               //dd($manageVendor);

        
        if ($request->isMethod('get'))
            //return view('formPurchaseOrder');
            return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty] );
        else {




            $rules = [

                'product_id' => 'required',
                'vendor_id' => 'required',
                'brand_id' => 'required',
                'specification_ids' => 'required',
                'qty' => 'required',
                'unit_price' => 'required',
                'gst_id' => 'required',
                'total_price' => 'required',
                'warranty_id' => 'required',
                'site_location_id' => 'required',
                'stock_status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);


                //Here we will be manipulating respective values
                $nameVendor  = Vendors::find($request->vendor_id);
                $nameProduct = Product::find($request->product_id);
                $nameBrand = brand::find($request->brand_id);
                $nameSitelocation = sitelocation::find($request->site_location_id);
                $valueGst = gst::find($request->gst_id);
                $valueWarranty = warranty::find($request->warranty_id); 
               // $valueSpecification = specification::find($request->specification_ids); 

                $purchaseOrder = new PurchaseOrder();
                $purchaseOrder->product_id = $request->product_id;
                $purchaseOrder->product_name = $nameProduct->name;
                $purchaseOrder->vendor_id = $request->vendor_id;
                $purchaseOrder->vendor_name = $nameVendor->name;
                $purchaseOrder->brand_id = $request->brand_id;
                $purchaseOrder->brand_name = $nameBrand->name;

                $purchaseOrder->specification_ids = $request->specification_ids;
                $purchaseOrder->qty = $request->qty;

                $purchaseOrder->unit_price = $request->unit_price;


                $purchaseOrder->gst_id = $request->gst_id;
                $purchaseOrder->gst_value = $valueGst->gst_in_percentage;

                
                $purchaseOrder->total_price = $request->total_price;
                $purchaseOrder->warranty_id = $request->warranty_id;
                $purchaseOrder->warranty_value = $valueWarranty->warranty_in_years;
                $purchaseOrder->site_location_id = $request->site_location_id;
                $purchaseOrder->site_location_name = $nameSitelocation->site." ".$nameSitelocation->floor." ".$nameSitelocation->location;
                $purchaseOrder->stock_status = $request->stock_status;
                
                $purchaseOrder->save();

            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }  

        
    }

    public function deletePurchaseOrder($id)
    {

        $orderinvoices  = orderinvoices::find($id);
        $invoiceid = $orderinvoices->invoiceid;
      
        $deletePoa = DB::table('purchase_order_approves')->where('poid','=', $invoiceid)->delete();
        $deleteAvailstocks = DB::table('availablestocks')->where('poorderid','=', $invoiceid)->delete();

       $stockdatas = DB::table('stockdatas')
                ->select('id')
                ->where('poid', $id)
                ->get();
        foreach ($stockdatas as  $value) {
           $deleteAvailstocks = DB::table('stockmovements')->where('stockdatas_id','=', $value)->delete();
        }
       

        $deleteAvailstocks = DB::table('stockdatas')->where('poid','=', $id)->delete();


        
        
        
        orderinvoices::destroy($id);

        return redirect('/manage-purchase-order');

    }

    public function updatePurchaseOrder(Request $request, $id)
    {



        //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($id);
        


        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);

        $poidapprovalStatus = $orderinvoices->invoiceid;

        $mailStatus = $orderinvoices->mail_status;

         $countDt = DB::table('purchase_order_approves')
                ->where('poid', $poidapprovalStatus)
                ->count();
        if($countDt > 0){
            $getDt = DB::table('purchase_order_approves')
                ->select('approval_status','updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();

         $getDtStatus = $getDt->approval_status;

    
        }else{

            $getDtStatus = 0;
            $getDt = DB::table('purchase_order_approves')
                ->select('approval_status','updated_at')
                ->where('poid', $poidapprovalStatus)
                ->first();
        }
        
        $poapprovestatus = $getDtStatus;

               
        $mailDetails = DB::table('mailsettings')
                ->select('id','email','status')
                ->where('status', 1)
                ->get();


              

        if ($request->isMethod('get'))
            return view('cartdetail', ['purchaseOrder' => PurchaseOrder::find($id), 'manageVendor' => Vendors::all(), 'manageProduct' => Product::all(), 'manageBrand' => brand::all(), 'manageSpecification' => specification::all(), 'manageSitelocation' => sitelocation::all(),  'manageGst' => gst::all(),  'manageWarranty' => warranty::all(), 'invoiceid' => $orderinvoices->invoiceid,  'stock_status' => $orderinvoices->stock_status, 'poapprovestatus' => $poapprovestatus,'mailStatus' => $mailStatus, 'mailDetails'=>$mailDetails, 'getDt'=> $getDt]);

        else {
            $rules = [
              
                'stock_status' => 'required',
               
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $purchaseOrder = PurchaseOrder::find($id);
            $purchaseOrder->stock_status = $request->stock_status;
            $purchaseOrder->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }
    }




    public function poforApproval(Request $request, $id)
    {



        //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($id);
        


        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);


               


        if ($request->isMethod('get'))
            return view('cartdetailforApproval', ['purchaseOrder' => PurchaseOrder::find($id), 'manageVendor' => Vendors::all(), 'manageProduct' => Product::all(), 'manageBrand' => brand::all(), 'manageSpecification' => specification::all(), 'manageSitelocation' => sitelocation::all(),  'manageGst' => gst::all(),  'manageWarranty' => warranty::all(), 'invoiceid' => $orderinvoices->invoiceid,  'stock_status' => $orderinvoices->stock_status]);

        else {
            $rules = [
              
                'stock_status' => 'required',
               
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $purchaseOrder = PurchaseOrder::find($id);
            $purchaseOrder->stock_status = $request->stock_status;
            $purchaseOrder->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }
    }




    public function updateajaxPurchaseOrder(Request $request, $id)
    {
      
        
        $orderinvoices  = orderinvoices::find($id);
        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);



        $poidapprovalStatus = $orderinvoices->invoiceid;
        $countDt = DB::table('purchase_order_approves')
        ->where('poid', $poidapprovalStatus)
        ->count();
        if($countDt > 0){
        $getDt = DB::table('purchase_order_approves')
        ->select('approval_status','updated_at')
        ->where('poid', $poidapprovalStatus)
        ->first();
        $getDtStatus = $getDt->approval_status;
        }else{
        $getDtStatus = 0;
        }
        $poapprovestatus = $getDtStatus;



        if ($request->isMethod('get'))
        return view('myAjaxPo', compact('poapprovestatus', 'getDt'));


        //return response()->json([Session::get('88uuiioo99888_cart_items')]);

        
    }




     public function generatePDF()
    {


       $extractedRows = DB::table('orderinvoices')
                ->where('invoiceid', Session::get('invoiceid'))
                ->first();


         //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($extractedRows->id);
        $session_var = unserialize($orderinvoices -> session_var);
        
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);


        $poidapprovalStatus = $orderinvoices->invoiceid;

        $countDt = DB::table('purchase_order_approves')
        ->where('poid', $poidapprovalStatus)
        ->count();
        if($countDt > 0){
        $getDt = DB::table('purchase_order_approves')
        ->select('approval_status', 'updated_at')
        ->where('poid', $poidapprovalStatus)
        ->first();

        $getDtStatus = $getDt->approval_status;

        }else{

        $getDtStatus = 0;
        $getDt = DB::table('purchase_order_approves')
        ->select('approval_status', 'updated_at')
        ->where('poid', $poidapprovalStatus)
        ->first();
        }

        $poapprovestatus = $getDtStatus;




       
       // return view('cartdetail');
         $pdf = PDF::loadView('myPDF', ['stock_status' => $orderinvoices->stock_status, 'invoiceid' => $orderinvoices->invoiceid, 'poapprovestatus' => $poapprovestatus, 'getDt'=>$getDt]);
         return $pdf->download('purchase-order'."-".Session::get('invoiceid').'.pdf');


                

    }


 ###### ---- function used for managing purchase order  ends ---- ######
}
