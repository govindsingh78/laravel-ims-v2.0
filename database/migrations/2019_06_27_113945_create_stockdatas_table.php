<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockdatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockdatas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('poid');
            $table->string('receiveddate');
            $table->string('productname');
            $table->string('serialno');
            $table->string('vendorname');
            $table->string('warrantyyears');
            $table->string('site');
            $table->string('floor');
            $table->string('location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockdatas');
    }
}
