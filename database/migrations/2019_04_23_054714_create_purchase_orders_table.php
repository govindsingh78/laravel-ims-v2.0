<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('product_id');
                    $table->string('product_name');
                    $table->string('vendor_id');
                    $table->string('vendor_name');
                    $table->string('brand_id');
                    $table->string('brand_name');
                    $table->string('specification_ids');
                    $table->string('qty', 150);
                    $table->string('unit_price', 150);
                    $table->string('gst_id', 150);
                    $table->string('gst_value', 150);
                    $table->string('total_price');
                    $table->string('warranty_id');
                    $table->string('warranty_value');
                    $table->string('site_location_id', 150);
                    $table->string('site_location_name');
                    $table->enum('stock_status', ['0','1'])->default(0);
                    $table->enum('status', ['0','1'])->default(1);
                    $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
