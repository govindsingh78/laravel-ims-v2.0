<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 5/24/2017
 * Time: 10:12 PM
 */

namespace App\Http\Controllers;
use App\PurchaseOrder;


use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;
use App\orderinvoices;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Input;
use Session;




 
class WishListController extends Controller
{
    public function index()
    {

        $manageVendor  = Vendors::all();
        $manageProduct = Product::all();
        $manageBrand = brand::all();
        $manageSpecification = specification::all();
        $manageSitelocation = sitelocation::all();
        $manageGst = gst::all();
        $manageWarranty = warranty::all(); 

        if(request()->ajax())
        {
            $items = [];
            $wish_list = app('wishlist');

            $wish_list->getContent()->each(function($item) use (&$items)
            {
                $items[] = $item;
            });


            // Session::put('cartGD', $this->items);



            return response(array(
                'success' => true,
                'data' => $items,
                'message' => 'wishlist get items success'
            ),200,[]);
           
             
        }
        else
        {
           // var_dump((session()->get('88uuiioo99888_cart_items')));
           // return view('cart');

             return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty] );
        }

        

    }

    public function add()
    {
        $wish_list = app('wishlist');
        $id = request('id');
        // $name = request('name');
        $price = request('price');
        $specification = request('specification');
        $qty = request('qty');
        $gst = request('gst');


        $vendor = request('vendor');
        $brand = request('brand');
        $sitelocation1 = request('sitelocation');
        $warranty = request('warranty');



        $manageProduct  = Product::find($id);
        $manageVendor  = Vendors::find($vendor);
        $manageBrand = brand::find($brand);
        $manageSitelocation = sitelocation::find($sitelocation1);
        $manageWarranty = warranty::find($warranty); 


        
        
        $sitelocation = $manageSitelocation->site ." ". $manageSitelocation->floor ." ". $manageSitelocation->location;
        
        
           
            $invoiceid = "#12345";

            if(Session::get('invoiceid') == null){
            $today = date('YmdHi');
            $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
            $range = $today - $startDate;
            $rand = rand(0, $range);
            $sid = ($startDate + $rand);
            Session::put('invoiceid', $sid);
            }
           


            

        $wish_list->add($id, $manageProduct -> name, $price, $qty, $gst,  $manageBrand -> name, $manageVendor -> name,  $manageWarranty -> warranty_in_years ,  $sitelocation,  $specification, $invoiceid,  array());

            //here add the cart data to database with flag inactive
 

            $session_var = serialize(session()->get('88uuiioo99888_cart_items'));




             
            $orderinvoices = new orderinvoices();
            $orderinvoices->invoiceid = Session::get('invoiceid');
            $orderinvoices->session_var = $session_var;
            $orderinvoices->save();

            
            



         
    }

    public function delete($id)
    {
        $wish_list = app('wishlist');

        $wish_list->remove($id);

        return response(array(
            'success' => true,
            'data' => $id,
            'message' => "cart item {$id} removed."
        ),200,[]);
    }

    public function details()
    {
        $wish_list = app('wishlist');


        return response(array(
            'success' => true,
            'data' => array(
                'total_quantity' => $wish_list->getTotalQuantity(),
                'sub_total' => $wish_list->getSubTotal(),
                'total' => $wish_list->getTotal(),


            ),
            'message' => "Get wishlist details success."
        ),200,[]);
    }
}