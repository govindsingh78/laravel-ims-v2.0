<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderinvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        

           Schema::create('orderinvoices', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('invoiceid');
                    // $table->string('product_id');
                    // $table->string('product_name');
                    // $table->string('vendor_name');
                    // $table->string('brand_name');
                    // $table->string('specification');
                    // $table->string('qty', 150);
                    // $table->string('unit_price', 150);
                    // $table->string('gst_value', 150);
                    // $table->string('total_price');
                    // $table->string('warranty_in_years');
                    // $table->string('site_location_id', 150);
                    $table->longtext('session_var');
                    $table->enum('stock_status', ['0','1'])->default(0);
                    $table->enum('status', ['0','1'])->default(0);
                    $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderinvoices');
    }
}
