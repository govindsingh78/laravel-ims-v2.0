<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitelocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sitelocations', function (Blueprint $table) {
                    $table->increments('id');
                    $table->string('site', 150);
                    $table->string('location', 150);
                    $table->string('floor', 150);
                    $table->enum('status', ['0','1'])->default(1);
                    $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sitelocations');
    }
}
