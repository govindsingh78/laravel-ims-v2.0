
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>{{ $title }}</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

</head>
<body>
<div class="row">
<div class="cart"  style="border: 2px solid orange !important; border-radius: 20px; padding: 10px; margin: auto;">


<!-- New PDF Format Starts-->
<table class="table">
<tr>
<td colspan="3" style="text-align: center; font-size: 20px"><img src="{{url('/images/ds-logo-pdf.png')}}" alt="Dotsquares Logo" height="100" width="130" ></td>
</tr>
<tr>
<th colspan="3" style="text-align: left;"><p>Subject: {{ $title }}</p></th>
</tr>
<tr>
<td colspan="3" style="text-align: left;">
<b>Dear Sir,</b>
</td>
</tr>
<tr>
<td colspan="3" style="text-align: left;">
<p>{{ $description }}<br/>
Please <a href="{{ $linktoaccess }}" target="_blank"> Click Here </a> to Visit the Purchase Order which has been processed by HK Department in order to approve/reject.</p>
</td>
</tr>
<tr>
<td colspan="3" style="text-align: left;">
<p>Regards,<br/>
HK Department<br> 
Dotsquares Pvt. Ltd. 
<br>
India
</p></td>
</tr>
</table>


</div>

</div>

</div>
</div>

<table class="table">
<tr>
<td colspan="2" style="text-align: center"> <span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
</td>
</tr>
</table>
</body>
</html>



