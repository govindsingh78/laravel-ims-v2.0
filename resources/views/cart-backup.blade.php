
<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

     
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        .cart {
            padding-bottom: 20px;
            padding-top: 20px;
        }
    </style>



</head>
<body>
 

 <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/images/logo.png')}}" height="30" width="150" alt="Dotsquares Logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <!-- New Nav Bars Added for IMS -->
                             
                             <!-- New Nav Bars Added for IMS -->
                              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Manage <span class="caret"></span>
                                </a>

                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('manage-vendors-product') }}">{{ __('Vendor') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-product-category') }}">{{ __('Product') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-brand-category') }}">{{ __('Brand') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-specification-category') }}">{{ __('Specification') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-warranties-category') }}">{{ __('Warranty') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-gst-category') }}">{{ __('GST') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-sitelocation-category') }}">{{ __('Site Location') }}</a>
                                    
                                  </div>

                                
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-purchase-order') }}">{{ __('Purchase Order') }}</a>
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-stock') }}">{{ __('Stock Management') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                  

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('login') }}">{{ __('Dashboard') }}</a>
                                         
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
<div class="row" id="wishlist">
    <div class="container cart">
        <div class="row">
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-lg-12">
                        <h5>Create Purchase Order</h5>

                      

                         
                        <div class="form-group form-group-sm">
                            <label>Product Name</label>
                            <!-- <input v-model="item.id" class="form-control" placeholder="Id"> -->

                             <select v-model="item.id" class="form-control">
                                  <option value="null" selected="selected">Choose Product</option>
                                   @foreach($manageProduct as $manageProd)
                                    <option :key="{{ $manageProd->id }}" :value="{{ $manageProd->id }}">{{ $manageProd->name }}</option>
                                    @endforeach  
                            </select>
                           
                        </div>

                         <div class="form-group form-group-sm">
                            <label>Product Specification</label>
                            <!-- <input v-model="item.id" class="form-control" placeholder="Id"> -->

                             <select v-model="item.specification" :multiple="true" class="form-control">
                                <option value="null" selected="selected">Choose Specification</option>
                                  @foreach($manageSpecification as $manageSpecific)
                                    <option :key="{{ $manageSpecific->id }}" :value="{{ $manageSpecific->id }}">{{ $manageSpecific->description }}</option>
                                    @endforeach  
                            </select>
                           
                        </div>



                         


                          <div class="form-group form-group-sm">
                            <label>Brand Name</label>
                                       
                            <!-- <input v-model="item.name" class="form-control" placeholder="Name"> -->
                             <select v-model="item.brand" class="form-control">
                                <option value="null" selected="selected">Choose Brand</option>
                                   @foreach($manageBrand as $manageBr)
                                    <option :key="{{  $manageBr->id }}" :value="{{  $manageBr->id }}">{{ $manageBr->name  }}</option>
                                    @endforeach  
                            </select>
 
                        </div>



                         <div class="form-group form-group-sm">
                            <label>Vendor Name</label>
                                       
                            <!-- <input v-model="item.name" class="form-control" placeholder="Name"> -->
                             <select v-model="item.vendor" class="form-control">
                                <option value="null" selected="selected">Choose Vendor</option>
                                   @foreach($manageVendor as $manageVend)
                                    <option :key="{{  $manageVend->id }}" :value="{{ $manageVend->id }}">{{ $manageVend->name  }}</option>
                                    @endforeach  
                            </select>
 
                        </div>



                          <div class="form-group form-group-sm">
                            <label>Warranty in Years</label>
                                       
                            <!-- <input v-model="item.name" class="form-control" placeholder="Name"> -->
                             <select v-model="item.warranty" class="form-control">
                                <option value="0" selected="selected">Choose Warranty</option>
                                   @foreach($manageWarranty as $manageWarrant)
                                    <option :key="{{ $manageWarrant->id }}" :value="{{ $manageWarrant->id }}">{{ $manageWarrant->warranty_in_years  }}</option>
                                    @endforeach  
                            </select>
 
                        </div>



                     

                        <div class="form-group">
                        <label for="site"></label>
                        <select v-model="item.site" name="site" class="form-control" @change="loadFloors(item.site)">
                      
                        <option value="null" selected="selected">Choose Site</option>
                        <option v-for="site in sites" :value="site.id">@{{ site.name }}</option>
                        </select>
                        </div>
                        <div class="form-group">
                        <label for="floor"></label>
                        <select v-model="item.floor" name="floor" class="form-control" @change="loadLocations(item.floor)">
                        <option value="null" selected="selected">Choose Floor</option>
                        <option v-for="floor in floors" :value="floor.id">@{{ floor.name }}</option>
                        </select>
                        </div>

                         <div class="form-group">
                        <label for="location"></label>
                        <select v-model="item.location" name="location" class="form-control">
                        <option value="null" selected="selected">Choose Location</option>
                        <option v-for="location in locations" :value="location.id"  @change="loadFloors = null">@{{ location.name }}</option>
                        </select>
                        </div>
                         


                        <div class="form-group form-group-sm">
                            <label>Price</label>
                            <input v-model="item.price" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Qty</label>
                            <input v-model="item.qty" class="form-control" placeholder="Quantity">
                        </div>
                        <!-- Adding Extra GST Feature -->
                         <div class="form-group form-group-sm">
                            <label>GST (%)</label>
                            <!-- <input v-model="item.gst" class="form-control" placeholder="gst"> -->

                            <select v-model="item.gst" class="form-control">

                                    <option value="0" selected="selected">Choose GST</option>
                                    @foreach($manageGst as $manageGs)
                                    <option :key="{{ $manageGs->gst_in_percentage }}" :value="{{ $manageGs->gst_in_percentage }}">{{ $manageGs->gst_in_percentage }}</option>
                                    @endforeach  
                            </select>
                         

                        </div>



                           <div class="form-group form-group-sm">
                            <label>Invoice Date</label>
                            <!-- <input v-model="item.id" class="form-control" placeholder="Id"> -->

                            <input type="date" v-model="item.invoicedate" class="form-control">                           
                        </div>


                        <button v-on:click="addItem()" class="btn btn-primary" style="float: right">Add Item</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <h5>Generate Invoice   


                     <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' style="float: right; font-style: italic; font-weight: 700; font-size: small; margin-top: -20px"> Invoice Date : @{{ items[0].invoicedate }} </span>
                     </div>

                </h5>
                <table class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>GST</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>#@{{ item.id }}</td>
                        <td>@{{ item.name }} 
                            <br/><i>Brand Name  :  @{{ item.brand }} </i>
                            <br/><i>Vendor Name : @{{ item.vendor }} </i>
                            <br/><i>Warranty in Years : @{{ item.warranty }} <i>
                            <br/><i>Site Location : @{{ item.sitelocation }} </i>
                            

                            
                            <br/><i>Specification : @{{ item.specification }} </i>
                        </td>
                        <td>@{{ item.quantity }}</td>
                        <td>@{{ item.price }}</td>
                        <td>@{{ item.gst }}%</td>
                        <td>
                            <button v-on:click="removeItem(item.id)" class="btn btn-sm btn-danger">remove</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <th>Items Purchased</th>
                        <td>@{{itemCount}}</td>
                    </tr>
                    <tr>
                        <th>Total Qty:</th>
                        <td>@{{ details.total_quantity }}</td>
                    </tr>
                    <tr>
                        <th>Sub Total:</th>
                        <td>@{{ 'INR. ' + details.sub_total.toFixed(2) }}</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>@{{ 'INR. ' + details.total.toFixed(2) }}</td>
                    </tr>
                </table>
                 

                <form  action="{{ url('wishlist/saveinvoice') }}" method="POST">
                @csrf
                <input type="submit" name="submit" value="Save Invoice" class="btn btn-primary" style="float: right; clear: both">
                </form>

            
            </div>


           



        </div>
    </div>
</div>

                        
                
                    

 <footer class="footer">
        <div class="container" style="text-align: center;">

        <span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
        </div>
        </footer>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>


 
<script>

    (function($) {

        var _token = '<?php echo csrf_token() ?>';

        $(document).ready(function() {
     

           
            var wishlist = new Vue({
                el: '#wishlist',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    sites: [],
                    floors: [],
                    locations: [],
                   
                    itemCount: 0,
                    items: [],
                    item: {
                        
                        id: null,
                        name: '',
                        specification: null,
                        brand: null,
                        vendor: null,
                        warranty: 0,
                        price: '',
                        qty: '',
                        gst: 0,
                        site: null,
                        floor: null,
                        location: null,
                        invoicedate: 0,
                        
                    },
                    options: {
                        id: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]

                        
                    },
                      options: {
                        
                        name: [
                            {label: 'AC', key: 'AC'}
                            
                        ]

                      
                    },
                      options: {
                         

                        gst: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    },
                     options: {
                         

                        specification: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    }
                },
                mounted:function(){
                    this.loadItems();
                    this.loadSites();
                },
                methods: {

                




                     loadSites: function() {

                        var _this = this;

                        this.$http.get('/sites',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.sites = success.body.data;
                             this.loadFloors();
                              this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },


                       loadFloors: function(id) {
                        console.log(id)
                        var _this = this;

                        this.$http.get('/floors/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.floors = success.body.data;
                             this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },

                     loadLocations: function(id) {
                        console.log(id)
                        var _this = this;

                        this.$http.get('/locations/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.locations = success.body.data;
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    

                    addItem: function() {

                        var _this = this;

                        console.log(_this.item.invoicedate);

                        this.$http.post('/wishlist',{
                            _token:_token,
                           
                            id:_this.item.id,
                            name:_this.item.name,
                            specification:_this.item.specification,
                            brand:_this.item.brand,
                            vendor:_this.item.vendor,
                            warranty:_this.item.warranty,
                            
                            price:_this.item.price,
                            qty:_this.item.qty,
                            gst:_this.item.gst,
                            site:_this.item.site,
                            floor:_this.item.floor,
                            location:_this.item.location,
                            invoicedate:_this.item.invoicedate,
                        }).then(function(success) {
                            _this.loadItems();



                        }, function(error) {
                            console.log(error);
                        });
                    },
                    removeItem: function(id) {

                        var _this = this;

                        this.$http.delete('/wishlist/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {

                        var _this = this;

                        this.$http.get('/wishlist',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {

                        var _this = this;

                        this.$http.get('/wishlist/details').then(function(success) {
                            _this.details = success.body.data;
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });

        });

    })(jQuery);
</script>
</body>
</html>

 