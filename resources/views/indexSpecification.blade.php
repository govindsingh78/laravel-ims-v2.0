<div class="container">
    
    
   
    <div class="row">
       
        <div class="col-md-6 form-group pull-left">
         <h5 style="padding-top: 10px">Product Specification List</h5>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-specification-category')}}?search='+this.value)"
                       placeholder="Search by Specification Name" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-specification-category')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
        <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-specification-category/createSpecification')}}"
        class="btn btn-success">Add Specification</a>
        </div>
    </div>
    
    </div>


   
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-specification-category?field=name&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Name
                </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-specification-category?field=description&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Description
                </a>
                {{request()->session()->get('field')=='description'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-specification-category?field=related_to_product_id&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Product Name
                </a>
                {{request()->session()->get('field')=='related_to_product_id'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-specification-category?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-specification-category?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
             
        @endphp
        @foreach($specification as $specific)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $specific->name }}</td>
                <td style="vertical-align: middle">{{ $specific->description }}</td>


               
                 <td style="vertical-align: middle;"> @foreach ($product as $prod) @if($prod->id == $specific->related_to_product_id) {{ $prod->name }} @endif @endforeach</td>
               

               
                <td style="vertical-align: middle;  font-style: italic;">{{$specific->status==1?'Active': 'Invactive'}}</td>
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($specific->created_at))}}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-specification-category/updateSpecification/'.$specific->id)}}">
                        Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$specific->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$specification->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>
