<style type="text/css">
    #modal_content{
    width: 1099px;
    left: -60%;
}


</style>





@if(isset($stock_datas))
    {!! Form::model($stock_datas,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($stock_datas)?'Edit/Move':'Add New'}} Stock</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

            <div class="row">
                <div class="col-md-8">

 
                    <div id="div-details"><table class="table" style="border-right: 1px solid #000;
    border-radius: 20px;
    margin-top: 5%;">
          <tr>
          <td colspan="3" style="text-align: center; font-size: 20px; border-top: 0px"><img src="images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" ></td>
          </tr>
           
          </table>

         </div>


                </div>
                <div class="col-md-4">
            <div class="form-group row required">
            {!! Form::label("poid","PO #ID",["class"=>"col-form-label col-md-12"]) !!}
            <div class="col-md-12">
            <select class="form-control" name="poid" id="poidforajax" onchange="poidAjax(this.value);  validateStockEntry();" >
            
            @if(isset($stock_datas))

            @foreach ($poid as $pd) 

            @if($stock_datas->poid == $pd->id)     
            <option value="{{ $pd->id }}"  <?php if($stock_datas->poid == $pd->id ){ echo "selected"; } else{ echo ""; } ?>>{{ $pd->invoiceid }}</option>
            @endif
            @endforeach

            @else
            <option value="">Choose PO</option>
            @foreach ($poid as $pd) 
                @if($pd->totalQuantity !== $pd->receivedStock)     
                <option value="{{ $pd->id }}" >{{ $pd->invoiceid }}</option>
                @endif
            @endforeach
            @endif
            </select>
              <span id="error-poid" class="invalid-feedback"></span>
            </div>
            </div>



            <div class="form-group row required">
            {!! Form::label("receiveddate","Date Received",["class"=>"col-form-label col-md-12"]) !!}
            <div class="col-md-12">
            




            <span> 
            @if(isset($stock_datas))
            <input type="text" name="receiveddate" id="receiveddate"  class="form-control" value="{{ date('Y-m-d',strtotime($stock_datas->receiveddate)) }}" disabled="true" />
            
            @else
            <input type="date" name="receiveddate" id="receiveddate"  class="form-control"/>
            @endif
            </span>
            <span id="error-receiveddate" class="invalid-feedback"></span>
            </div>
            </div>




            <div class="form-group row required">
            {!! Form::label("productname","Product Name",["class"=>"col-form-label col-md-12"]) !!}
            <div class="col-sm-12">
            <select class="form-control" name="productname" id="productid" onchange="validateStockEntry()">
           
            @if(isset($stock_datas))
            @foreach ($productname as $prod)  
           
             @if($stock_datas->productname == $prod->id)
                 
            <option value="{{ $prod->id }}"  <?php if($stock_datas->productname == $prod->id ){ echo "selected"; } else{ echo ""; } ?>>{{ $prod->name }}</option>
             @endif
             
            @endforeach

            @else
             <option value="">Choose Product</option>
            @foreach ($productname as $prod)      
            <option value="{{ $prod->id }}" >{{ $prod->name }}</option>
            @endforeach
            @endif
            </select>
            <span id="error-productname" class="invalid-feedback"></span>
            </div>
            </div>




        <div class="form-group row">
        {!! Form::label("name","Serial No.",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            
           @if(isset($stock_datas))
           {!! Form::text("serialno",$stock_datas->serialno,["class"=>"form-control".($errors->has('serialno')?" is-invalid":""),'placeholder'=>'Serial No.','id'=>'focus', 'disabled'=>'true']) !!}

            @else
           {!! Form::text("serialno",null,["class"=>"form-control".($errors->has('serialno')?" is-invalid":""),'placeholder'=>'Serial No.','id'=>'focus']) !!}
            @endif

            <span id="error-name" class="invalid-feedback"></span>
        </div>
        </div>




            <div class="form-group row required">
            {!! Form::label("vendorname","Vendor Name",["class"=>"col-form-label col-md-12"]) !!}
            <div class="col-sm-12">
            <select class="form-control" name="vendorname">
           
           
            @if(isset($stock_datas))
            @foreach ($vendorname as $vend)    
            <?php if($stock_datas->vendorname == $vend->id ) { ?>  
            <option value="{{ $vend->id }}"  <?php if($stock_datas->vendorname == $vend->id ){ echo "selected"; } else{ echo ""; } ?>>{{ $vend->name }}</option>
            <?php } ?> 
            @endforeach

            @else
             <option value="">Choose Vendor</option>
            @foreach ($vendorname as $vend)      
            <option value="{{ $vend->id }}" >{{ $vend->name }}</option>
            @endforeach
            @endif


            </select>
            <span id="error-vendorname" class="invalid-feedback"></span>
            </div>
            </div>




            <div class="form-group row">
            {!! Form::label("warrantyyears","Warranty (Years)",["class"=>"col-form-label col-md-12"]) !!}
            <div class="col-sm-12">
            <select class="form-control" name="warrantyyears">
           
            
            @if(isset($stock_datas))
            @foreach ($warrantyyears as $warrant)   
            <?php if($stock_datas->warrantyyears == $warrant->id ){ ?>   
            <option value="{{ $warrant->id }}"  <?php if($stock_datas->warrantyyears == $warrant->id ){ echo "selected"; } else{ echo ""; } ?>>{{ $warrant->warranty_in_years }}</option>
            <?php } ?>
            @endforeach

            @else
             <option value="">Choose Warranty</option>
            @foreach ($warrantyyears as $warrant)      
            <option value="{{ $warrant->id }}" >{{ $warrant->warranty_in_years }}</option>
            @endforeach
            @endif



            </select>
            <span id="error-warrantyyears" class="invalid-feedback"></span>
            </div>
            </div>



            <hr style="background-color: #000000" />
            <span> 

            @if(isset($stock_datas))
            
            <p><i> Please update the following site detail in case to move this stock !! </i></p>
            
            @else
            
            <p><i> Please provide the site where stock needs to be moved !!</i> </p>

            @endif
            </span>
            <hr style="background-color: #000000" />
     
      <div class="form-group row required">
        {!! Form::label("site","Site Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">


 


         <!-- {!! Form::select('site', ['' => 'Select'] +$sites->toArray(),'',array('class'=>'form-control','id'=>'site','style'=>''));!!} -->



          <select class="form-control" name="site" id="site">
            <option value="">Choose Site</option>
            
            @if(isset($stock_datas))
            @foreach ($sites as $key => $val)      
            <option value="{{ $key }}"  <?php if($stock_datas->site == $key ){ echo "selected"; } else{ echo ""; } ?>>{{ $val }}</option>
            @endforeach

            @else
           @foreach ($sites as $key => $val)      
            <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
            @endif



            </select>

               
        </div>
    </div>
    <div class="form-group row required">
        <label for="floor" class="col-form-label col-md-12">Select Floor:</label>
        <div class="col-md-12">
        <select name="floor" id="floor" class="form-control">
            

            
            
            @if(isset($stock_datas))
            @foreach ($floors as $key => $val)
            @if($key == $stock_datas->floor)      
            <option value="{{ $key }}">{{ $val }}</option>
            @endif
            @endforeach
            @else
            <option value="">Choose Floor</option>
            @endif



            </select>


        </select>

    </div>
    </div>

     <div class="form-group row required">
        <label for="location" class="col-form-label col-md-12">Select Location:</label>
        <div class="col-md-12">
        <select name="location" id="location" class="form-control">
            

            @if(isset($stock_datas))
            @foreach ($locations as $key => $val)
            @if($key == $stock_datas->location)      
            <option value="{{ $key }}">{{ $val }}</option>
            @endif
            @endforeach
            @else
            <option value="">Choose Location</option>
            @endif


        </select>

    </div>
    </div>
    
   </div>
</div>



</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary", "id"=>"btnSubmit"])!!}
</div>
{!! Form::close() !!}
 



<script type="text/javascript">
    $('#site').change(function(){
    var siteID = $(this).val();    
    if(siteID){
        $.ajax({
           type:"GET",
           url:"{{url('api/get-floor-list')}}?site_id="+siteID,
           success:function(res){               
            if(res){
                $("#floor").empty();
                $("#floor").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#floor").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#floor").empty();
            }
           }
        });
    }else{
        $("#floor").empty();
        $("#location").empty();
    }      
   });
    $('#floor').on('change',function(){
    var floorID = $(this).val();    
    if(floorID){
        $.ajax({
           type:"GET",
           url:"{{url('api/get-location-list')}}?floor_id="+floorID,
           success:function(res){               
            if(res){
                $("#location").empty();
                $.each(res,function(key,value){
                    $("#location").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#location").empty();
            }
           }
        });
    }else{
        $("#location").empty();
    }
        
   });


    $(document).ready(function(){
        return poidAjax(id = null);
    })

    function poidAjax(id){
        
        if(id == null){
            var poidforajax = $("#poidforajax").val();
        }else{
            var poidforajax = id;
        }
        // send ajax request to controller
        ajaxforPoid(poidforajax);
    }

function ajaxforPoid(id) {

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

$.ajax({
type:'POST',
url:'/checkAjaxforPoid',
data:{prodid:id},
success:function(data){
console.log(id);

$('#div-details').load('{{URL::to("manage-purchase-order/updateajaxPurchaseOrder")}}/'+id);


}
});
}



function validateStockEntry() {



var productid = $("#productid").val();
var poidforajax = $("#poidforajax").val(); 

if(productid == '' || poidforajax == ''){
    alert("Please make sure if both PO #ID & Product Name has been selected");
    return false;
}



$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

$.ajax({
type:'POST',
url:'/checkAjaxforStockValidation',
data:{poidforajax:poidforajax, productid:productid},
success:function(data){
 
if(data.statuscode == 1){
    $("#btnSubmit").attr("disabled", false);
    alert(data.success);
    
}else{
    $("#btnSubmit").attr("disabled", true);
    alert(data.success);
    
}




}
});
}




    $(function(){
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#receiveddate').attr('max', maxDate);
    });
</script> 