@if(isset($gstSetting))
    {!! Form::model($gstSetting,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($gstSetting)?'Edit':'New'}} GST Setting</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row required">
        {!! Form::label("company_name","Company Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("company_name",null,["class"=>"form-control".($errors->has('company_name')?" is-invalid":""),'placeholder'=>'Company Name','id'=>'focus']) !!}
            <span id="error-company_name" class="invalid-feedback"></span>
        </div>
    </div>

    <div class="form-group row required">
        {!! Form::label("gst_no","GST No.",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("gst_no",null,["class"=>"form-control".($errors->has('gst_no')?" is-invalid":""),'placeholder'=>'GST Number e.g. xxxxxxxxxx ','id'=>'focus']) !!}
            <span id="error-gst_no" class="invalid-feedback"></span>
        </div>
    </div>


    <div class="form-group row required">
        {!! Form::label("company_address","Company Address",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("company_address",null,["class"=>"form-control".($errors->has('company_address')?" is-invalid":""),'placeholder'=>'Company Address','id'=>'focus']) !!}
            <span id="error-company_address" class="invalid-feedback"></span>
        </div>
    </div>

     

     <div class="form-group row required">
        {!! Form::label("status","GST Status",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="status">
        <option value="">Choose Status</option>
        @if(isset($gstSetting))
              <option value="0" <?php if($gstSetting->status == 0 ){ echo "selected"; } else{ echo ""; } ?>>Inactive</option>
              <option value="1" <?php if($gstSetting->status == 1 ){ echo "selected"; } else{ echo ""; } ?>>Active</option>
                
        @else
               <option value="0" >Inactive</option>
               <option value="1" >Active</option>
                
        @endif

        </select>
          <span id="error-status" class="invalid-feedback"></span>
        </div>
          
        </div>
    
   
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}