@if(isset($sitelocation))
    {!! Form::model($sitelocation,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($sitelocation)?'Edit':'New'}} Site Location</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row required">
        {!! Form::label("site","Site Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("site",null,["class"=>"form-control".($errors->has('site')?" is-invalid":""),'placeholder'=>'Site Name','id'=>'focus']) !!}
            <span id="error-site" class="invalid-feedback"></span>
        </div>
    </div>
    <div class="form-group row required">
        {!! Form::label("floor","Floor Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("floor",null,["class"=>"form-control".($errors->has('floor')?" is-invalid":""),'placeholder'=>'Floor Name','id'=>'focus']) !!}
            <span id="error-floor" class="invalid-feedback"></span>
        </div>
    </div>
    <div class="form-group row required">
        {!! Form::label("location","Location Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("location",null,["class"=>"form-control".($errors->has('location')?" is-invalid":""),'placeholder'=>'Location Name','id'=>'focus']) !!}
            <span id="error-location" class="invalid-feedback"></span>
        </div>
    </div>
     
 



     
     

<div class="form-group row required">
        {!! Form::label("status","Sitelocation Status",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="status">
        <option value="">Choose Status</option>
        @if(isset($sitelocation))
              <option value="0" <?php if($sitelocation->status == 0 ){ echo "selected"; } else{ echo ""; } ?>>Inactive</option>
              <option value="1" <?php if($sitelocation->status == 1 ){ echo "selected"; } else{ echo ""; } ?>>Active</option>
                
        @else
               <option value="0" >Inactive</option>
               <option value="1" >Active</option>
                
        @endif

        </select>
          <span id="error-status" class="invalid-feedback"></span>
        </div>
          
        </div>
      

		




</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}




<!-- 

<script type="text/javascript">
    $('#site').change(function(){
    var siteID = $(this).val();    
    if(siteID){
        $.ajax({
           type:"GET",
           url:"{{url('api/get-floor-list')}}?site_id="+siteID,
           success:function(res){               
            if(res){
                $("#floor").empty();
                $("#floor").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#floor").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#floor").empty();
            }
           }
        });
    }else{
        $("#floor").empty();
        $("#location").empty();
    }      
   });
    $('#floor').on('change',function(){
    var floorID = $(this).val();    
    if(floorID){
        $.ajax({
           type:"GET",
           url:"{{url('api/get-location-list')}}?floor_id="+floorID,
           success:function(res){               
            if(res){
                $("#location").empty();
                $.each(res,function(key,value){
                    $("#location").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#location").empty();
            }
           }
        });
    }else{
        $("#location").empty();
    }
        
   });
</script> -->