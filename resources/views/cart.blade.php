
<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMS::Dotsquares</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        .cart {
            padding-bottom: 20px;
            padding-top: 20px;
        }
        body{
            margin: 0;
            padding: 0;
            width: 100%;
            overflow-x: hidden;
            height: auto;
         }
         .form-control, .row{
            font-size: 12px !important;
         }
         .row label{
            font-weight: 900 !important;
         }
    </style>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/images/logo.png')}}" height="30" width="150" alt="Dotsquares Logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto"></ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        <!-- New Nav Bars Added for IMS -->
                             
                        <!-- New Nav Bars Added for IMS -->
                              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Manage <span class="caret"></span>
                                </a>

                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('manage-vendors-product') }}">{{ __('Vendor') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-product-category') }}">{{ __('Product') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-brand-category') }}">{{ __('Brand') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-specification-category') }}">{{ __('Specification') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-warranties-category') }}">{{ __('Warranty') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-gst-category') }}">{{ __('GST') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-sitelocation-category') }}">{{ __('Site Location') }}</a>
                                    
                                  </div>
                            </li>
                            <li class="nav-item">
                                     <a class="nav-link" href="{{ route('manage-purchase-order') }}">{{ __('Purchase Order') }}</a>
                            </li>
                            <li class="nav-item">
                                    <a class="nav-link" href="{{ route('manage-stock') }}">{{ __('Stock Management') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('login') }}">{{ __('Dashboard') }}</a>
                                <a class="dropdown-item" href="{{ route('manage-gst-setting') }}">{{ __('GST Setting') }}</a>

                                 <a class="dropdown-item" href="{{ route('manage-mail-setting') }}">{{ __('Mail Setting') }}</a>
                                
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
<div class="row" id="wishlist">
    <div class="container cart">
        <div class="row">
            <div class="col-lg-4">
                  <div class="row">
                        <h5 style="margin: 15px;margin-top: 0px;">Create Purchase Order</h5>
                        <div class="col-lg-12">
                        <div class="form-group form-group-sm">
                        <label>PO Date <span style="color: red">*</span></label>
                        <input type="date" v-model="item.invoicedate" class="form-control" id="invoicedate">                           
                    </div>
            </div>
                        <div class="col-lg-12">
                        <div class="form-group">
                        <label for="companyname">Company Name <span style="color: red">*</span></label>
                        <select v-model="item.companyname" name="companyname" class="form-control" @change="loadGstAddress(item.companyname)" id="companyname">
                      
                        <option value="" selected="selected">Choose Company</option>
                        <option v-for="companyname in companies" :value="companyname.id">@{{ companyname.company_name }}</option>
                        </select>
                        </div>
                        <div class="form-group" style="display: none">
                        <label for="gstno">GST No. <span style="color: red">*</span></label>
                        <select v-model="item.gstno" name="gstno" class="form-control" id="gstno" >
                        <option value="" selected="selected">GST No.</option>
                        <option v-for="gstno in companiesdetails" :value="gstno.id">@{{ gstno.gst_no }}</option>
                        </select>
                        </div>

                        <div class="form-group" style="display: none">
                        <label for="companyaddress">Company Address <span style="color: red">*</span></label>
                        <select v-model="item.companyaddress" name="companyaddress" class="form-control" id="companyaddress">
                        <option value="" selected="selected">Company Address <span style="color: red">*</span></option>
                        <option v-for="companyaddress in companiesdetails" :value="companyaddress.id">@{{ companyaddress.company_address }}</option>
                        </select>
                        </div>

                        <div class="form-group">
                        <label for="vendor">Vendor Name <span style="color: red">*</span></label>
                        <select v-model="item.vendor" name="vendor" class="form-control" @change="loadVendorAddress(item.vendor)" id="productvendor">
                      
                        <option value="" selected="selected">Choose Vendor</option>
                        <option v-for="vendor in vendors" :value="vendor.id">@{{ vendor.name }}</option>
                        </select>
                        </div>

                        <div class="form-group" style="display: none">
                        <label for="vendoraddress">Vendor Address <span style="color: red">*</span></label>
                        <select v-model="item.vendoraddress" name="vendoraddress" class="form-control" id="vendoraddress">
                        <option value="" selected="selected">Vendor Address</option>
                        <option v-for="vendoraddress in vendoraddresses" :value="vendoraddress.id">@{{ vendoraddress.address }}</option>
                        </select>
                        </div>
    
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group form-group-sm">
                            <label>Product Name <span style="color: red">*</span></label>
                            <!-- <input v-model="item.id" class="form-control" placeholder="Id"> -->

                             <select v-model="item.id" class="form-control" id="productname">
                                  <option value="" selected="selected">Choose Product</option>
                                   @foreach($manageProduct as $manageProd)
                                    <option :key="{{ $manageProd->id }}" :value="{{ $manageProd->id }}">{{ $manageProd->name }}</option>
                                    @endforeach  
                            </select>
                        </div>
                         <div class="form-group form-group-sm">
                            <label>Brand Name <span style="color: red">*</span></label>
                              <select v-model="item.brand" class="form-control" id="productbrand">
                                <option value="" selected="selected">Choose Brand</option>
                                   @foreach($manageBrand as $manageBr)
                                    <option :key="{{  $manageBr->id }}" :value="{{  $manageBr->id }}">{{ $manageBr->name  }}</option>
                                    @endforeach  
                            </select>
                         </div>
                        
                         <div class="form-group form-group-sm">
                            <label>Warranty in Years</label>
                              <select v-model="item.warranty" class="form-control" id="productwarranty">
                                <option value="" selected="selected">Choose Warranty</option>
                                   @foreach($manageWarranty as $manageWarrant)
                                    <option :key="{{ $manageWarrant->id }}" :value="{{ $manageWarrant->id }}">{{ $manageWarrant->warranty_in_years  }}</option>
                                    @endforeach  
                            </select>
                         </div>
                    </div>

                    <div class="col-lg-6">
                          <div class="form-group form-group-sm">
                            <label>Product Specification </label>
                            <select v-model="item.specification" :multiple="true" class="form-control" id="productspecification" style="max-height: 300px;
    height: 125px;
    overflow-y: scroll;">
                                <option value="" selected="selected">Choose Specification</option>
                                @foreach($manageSpecification as $manageSpecific)
                                <option :key="{{ $manageSpecific->id }}" :value="{{ $manageSpecific->id }}">{{ $manageSpecific->description }}</option>
                                @endforeach  
                            </select>
                           
                        </div>
                        <div class="form-group form-group-sm">
                        <label>Price <span style="color: red">*</span></label>
                        <input v-model="item.price" class="form-control" placeholder="Price" id="productprice">
                        </div>
                    </div>

                     <div class="col-lg-12">
                         <div class="form-group form-group-sm">
                            <label>Qty <span style="color: red">*</span></label>
                            <input v-model="item.qty" class="form-control" placeholder="Quantity" id="productqty">
                        </div>
                     <div class="form-group form-group-sm">
                            <label>GST (%) <span style="color: red">*</span></label>
                            <select v-model="item.gst" class="form-control" id="productgst">
                                    <option value="" selected="selected">Choose GST</option>
                                    @foreach($manageGst as $manageGs)
                                    <option :key="{{ $manageGs->gst_in_percentage }}" :value="{{ $manageGs->gst_in_percentage }}">{{ $manageGs->gst_in_percentage }}</option>
                                    @endforeach  
                            </select>
                      </div>

                        <div class="form-group">
                        <label for="site">Site <span style="color: red">*</span></label>
                        <select v-model="item.site" name="site" class="form-control" @change="loadFloors(item.site)" id="sitename">
                      
                        <option value="" selected="selected">Choose Site</option>
                        <option v-for="site in sites" :value="site.id">@{{ site.name }}</option>
                        </select>
                        </div>
                        
                        <div class="form-group" style="display: none">
                        <label for="floor">Floor <span style="color: red">*</span></label>
                        <select v-model="item.floor" name="floor" class="form-control" @change="loadLocations(item.floor)" id="floorname">
                        <option value="" selected="selected">Choose Floor</option>
                        <option v-for="floor in floors" :value="floor.id">@{{ floor.name }}</option>
                        </select>
                        </div>
                        
                        <div class="form-group" style="display: none">
                        <label for="location">Location <span style="color: red">*</span></label>
                        <select v-model="item.location" name="location" class="form-control" id="locationname">
                        <option value="" selected="selected">Choose Location</option>
                        <option v-for="location in locations" :value="location.id"  @change="loadFloors = null">@{{ location.name }}</option>
                        </select>
                        </div>
                    
                        <div class="form-group form-group-sm">
                            <label>Delivary Instruction </label>
                            <input v-model="item.delivaryinstruction" class="form-control" placeholder="Write some notes .." id="delivaryinstruction">
                        </div>
                        <button v-on:click="addItem()" class="btn btn-primary" style="float: right; width: 100%;">Add Item to Purchase Order</button>
                    </div>
                </div>
            </div>

<div class="col-lg-1">
 <hr style="width: 1px; height: 100%; background-color: #000; margin: auto; padding: 0" />
</div>

<div class="col-lg-7" id="mycartdetail">
  
                    <div style=" border: 1px solid orange; border-radius: 20px; overflow: hidden; height: auto !important; padding: 10px ">
                    <table class="table">
                    <tr>
                    <td colspan="3" style="text-align: center; font-size: 20px"><img src="{{ URL::to('/') }}/images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" ></td>
                    </tr>
                    <tr>
                    <th colspan="3" style="text-align: center; font-weight: bold;"><u>PURCHASE ORDER</u></th>
                    </tr>
                    </table>
                    <input type="submit" v-if='itemCount === 0' name="submit" value="Please add at least one item to cart in order to generate PO" disabled class="btn btn-warning" style="float: right; clear: both; width: 100%; border-radius: 0px">
                
                    <table class="table">
                    <tr>
                    <th>PURCHASE ORDER No.-</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  #@{{ items[itemCount - 1].invoiceidpo }} </span>
                     </div></td>
                     <th>Date Requested:</th>
                    <td ><p v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0'>  @{{ items[itemCount - 1].invoicedate }} </span>
                     </p></td>
                    </tr>
                    
                    </table>
                   
                    <hr style="width: 1px;
                    height: 100px;
                    position: absolute;
                    background-color: rgb(0, 0, 0);
                    margin: auto;
                    padding: 0px;
                    left: 50%;" />
                    
                    
                    <table class="table" style="width: 48%; float: left;">
                    <tr>
                    <th>Purchaser  Details:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyname }} </span>
                    </div>
                    </td>
                    </tr>
                    <tr>
                    <th>Address:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyaddress }} </span>
                    </div></td>
                    </tr>
                    <tr>
                    <th>GST No.:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].gstno }} </span>
                    </div></td>
                    </tr>
                    </table>
                    <table class="table" style="width: 48%; float: right;">
                    <tr>
                    <th colspan="3">Vendor Information:</th>
                    </tr>
               
                    <tr>
                    <th>Name</th>
                    <td  colspan="2"> <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendor }} </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Address</th>
                    <td  colspan="2"  ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendoraddress }} </span>
                     </div></td>
                    </tr>
                    </table>

                   <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Price(Per Unit)</th>
                        <th>GST%</th>
                        <th>GST (Amount)</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>@{{ item.id }} </td>
                        <td>@{{ item.name }} - @{{ item.brand }}  @{{ item.specification }}
                        <!-- <br/><i>Brand Name  :  @{{ item.brand }} </i>
                        <br/><i>Vendor Name : @{{ item.vendor }} </i>
                        <br/><i>Warranty in Years : @{{ item.warranty }} <i>
                        <br/><i>Site Location : @{{ item.sitelocation }} </i>
                        <br/><i>Specification : @{{ item.specification }} </i> -->
                        </td>
                        <td>@{{ item.quantity }}</td>
                        <td>INR. @{{ item.price }}</td>
                        <td>@{{ item.gst }}%</td>
                        <td>INR. @{{ item.gstamountperitem }}</td>
                        <td>INR. @{{ item.totalperitem }}</td>
                       <td>
                            <button v-on:click="removeItem(item.id)" class="btn btn-sm btn-danger">remove</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table">
                    <!--  <tr>
                    <th>Items Purchased</th>
                    <td>@{{itemCount}}</td>
                    </tr> -->
                    <!--  <tr>
                    <th>Total Qty:</th>
                    <td>@{{ details.total_quantity }}</td>
                    </tr> -->
                    <tr>
                        <th>GST Total:</th>
                        <td> <div v-for="(item, index) in items" :key='index'>
                        <span v-if='index === 0' > @{{ 'INR. ' + gsttotal }} </span>
                        </div></td>
                    </tr>
                    <tr>
                        <th>Sub Total:</th>
                        <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' > @{{ 'INR. ' + details.sub_total.toFixed(2) }} </span>
                     </div></td>
                    </tr>
                   <!--  <tr>
                        <th>Total:</th>
                        <td>@{{ 'INR. ' + details.total.toFixed(2) }}</td>
                    </tr> -->
                </table>

                 <table class="table">
                    <tr>
                    <th>Deliver to whom:</th>
                    <td  colspan="2"><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  HK Department </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Delivery location:</th>
                    <td  colspan="2"><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivarylocation }} </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Delivery instructions:</th>
                    <td  colspan="2"  ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivaryinstruction }} </span>
                     </div></td>
                    </tr>

                     <tr>
                    <th>Approver’s Signature</th>
                    <td  colspan="2"><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  ............... </span>
                     </div></td>
                    </tr>
                     
                    </table>
                
                    <form  action="{{ url('wishlist/saveinvoice') }}" method="POST">
                    @csrf
                    <input type="submit" v-if='itemCount > 0' name="submit" value="Save PO" class="btn btn-primary" style="float: right; clear: both; width: 200px;">
                 </form>
             </div>
        </div>
    </div>
</div>

<footer class="footer">
<div class="container" style="text-align: center;">

<span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

<script>

    (function($) {
    
        var _token = '<?php echo csrf_token() ?>';
        $(document).ready(function() {
            var wishlist = new Vue({
                el: '#wishlist',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    companies: [],
                    companiesdetails: [],
                    vendors: [],
                    vendoraddresses: [],
                    sites: [],
                    floors: [],
                    locations: [],
                   
                    itemCount: 0,
                    items: [],
                    item: {
                        
                        id: '',
                        name: '',
                        specification: '',
                        brand: '',
                        vendor: '',
                        warranty: '',
                        price: '',
                        qty: '',
                        gst: '',
                        site: '',
                        floor: '',
                        location: '',
                        invoicedate: '',
                        companyname: '',
                        gstno: '',
                        companyaddress: '',
                        vendoraddress: '',
                        delivaryinstruction: '',
                        gsttotal: '',
                        
                    },
                    options: {
                        id: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]

                        
                    },
                      options: {
                        
                        name: [
                            {label: 'AC', key: 'AC'}
                            
                        ]

                      
                    },
                      options: {
                         

                        gst: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    },
                     options: {
                         

                        specification: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    }
                },
                mounted:function(){
                    this.loadItems();
                    this.loadSites();
                    this.loadCompanies();
                    this.loadVendors();

                },
 
                computed: {
                gsttotal: function() {
                if (!this.items) {
                return 0;
                }

                return this.items.reduce(function (gsttotal, item) {
                    return gsttotal + Number(item.gstamountperitem);
                }, 0);
                }
                },
                methods: {
                    loadVendors: function() {

                        var _this = this;

                        this.$http.get('/vendors',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendors = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadVendorAddress: function(id) {

                        var _this = this;

                        this.$http.get('/vendoraddresses/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendoraddresses = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadCompanies: function() {

                        var _this = this;

                        this.$http.get('/companies',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companies = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadGstAddress: function(id) {
                        var _this = this;
                        this.$http.get('/companiesdetails/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companiesdetails = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadSites: function() {
                        var _this = this;
                        this.$http.get('/sites',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.sites = success.body.data;
                             this.loadFloors();
                              this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },


                    loadFloors: function(id) {
                        console.log(id)
                        var _this = this;
                        this.$http.get('/floors/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.floors = success.body.data;
                             this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },

                     loadLocations: function(id) {
                        console.log(id)
                        var _this = this;
                        this.$http.get('/locations/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.locations = success.body.data;
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },
  
                    addItem: function() {
                        var _this = this;
                        console.log(_this.item.invoicedate);
                        this.$http.post('/wishlist',{
                            _token:_token,
                            id:_this.item.id,
                            name:_this.item.name,
                            specification:_this.item.specification,
                            brand:_this.item.brand,
                            vendor:_this.item.vendor,
                            warranty:_this.item.warranty,
                            price:_this.item.price,
                            qty:_this.item.qty,
                            gst:_this.item.gst,
                            site:_this.item.site,
                            floor:_this.item.floor,
                            location:_this.item.location,
                            invoicedate:_this.item.invoicedate,
                            companyname:_this.item.companyname,
                            gstno:_this.item.gstno,
                            companyaddress:_this.item.companyaddress,
                            vendoraddress:_this.item.vendoraddress,
                            delivaryinstruction:_this.item.delivaryinstruction,

                        }).then(function(success) {
                            _this.loadItems();
                            console.log($("#invoicedate").val());
                            $('html, body').animate({
                            scrollTop: $("#mycartdetail").offset().top
                            }, 2000);
                        }, function(error) {
                            console.log($("#invoicedate").val());
                            if($("#invoicedate").val() == ""){
                                 alert("Invoice Date is Blank !!");
                                 $("#invoicedate").focus();
                            } 
                            else if($("#companyname").val() == ""){
                                 alert("Company Name is Blank !!");
                                 $("#companyname").focus();
                            }
                            // else if($("#gstno").val() == ""){
                            //      alert("Choose GST No. !!");
                            //      $("#gstno").focus();
                            // }
                            // else if($("#companyaddress").val() == ""){
                            //      alert("Company Address is Blank !!");
                            //      $("#companyaddress").focus();
                            // }
                            else if($("#productvendor").val() == ""){
                                 alert("Product Vendor is Blank !!");
                                 $("#productvendor").focus();
                            }
                            // else if($("#vendoraddress").val() == ""){
                            //      alert("Vendor Address is Blank !!");
                            //      $("#vendoraddress").focus();
                            // }

                            else if($("#productname").val() == ""){
                                 alert("Product Name is Blank !!");
                                 $("#productname").focus();
                            } 
                            else if($("#productbrand").val() == ""){
                                 alert("Product Brand is Blank !!");
                                 $("#productbrand").focus();
                            } 

                            else if($("#productprice").val() == "" || $.isNumeric($("#productprice").val()) == false){
                                 alert("Please fill price carefully !!");
                                 $("#productprice").focus();
                            }else if($("#productqty").val() == "" || $.isNumeric($("#productqty").val()) == false){
                                 alert("Please fill quantity carefully !!");
                                 $("#productqty").focus();
                            }else if($("#productgst").val() == ""){
                                 alert("Product GST is Blank !!");
                                 $("#productgst").focus();
                            } else if($("#sitename").val() == ""){
                                 alert("Site Name is Blank !!");
                                 $("#sitename").focus();
                            }

                            // else if($("#floorname").val() == ""){
                            //      alert("Floor Name is Blank !!");
                            //      $("#floorname").focus();
                            // }else if($("#locationname").val() == ""){
                            //      alert("Location Name is Blank !!");
                            //      $("#locationname").focus();
                            // }


                        });
                    },
                    removeItem: function(id) {

                        var _this = this;

                        this.$http.delete('/wishlist/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {

                        var _this = this;

                        this.$http.get('/wishlist',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {

                        var _this = this;

                        this.$http.get('/wishlist/details').then(function(success) {
                            _this.details = success.body.data;
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });

        });

    })(jQuery);





    $(function(){
        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#invoicedate').attr('max', maxDate);
    });
</script>
</body>
</html>

 