<div class="container">
    
    
   
    <div class="row">
      
        <div class="col-md-6 form-group pull-left">
        <h5 style="padding-top: 10px">Comapany &amp; GST Settings</h5>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-gst-setting')}}?search='+this.value)"
                       placeholder="Search by Company Name" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-gst-setting')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
        <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-gst-setting/createGstSetting')}}"
        class="btn btn-success" style="width: 100%">Add GST No.</a>
        </div>
    </div>
    </div>
    
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-gst-setting?field=company_name&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Company Name
                </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>

             <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-gst-setting?field=gst_no&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    GST No.
                </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
             


             <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-gst-setting?field=company_address&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Company Address
                </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-gst-setting?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-gst-setting?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($gstSetting as $gt)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $gt->company_name }}</td>
                <td style="vertical-align: middle">{{ $gt->gst_no }}</td>
                <td style="vertical-align: middle">{{ $gt->company_address }}</td>
                 
                <td style="vertical-align: middle; font-style: italic;">{{ $gt->status == 0 ? 'Inactive' : 'Active' }}</td>
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($gt->created_at))}}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-gst-setting/updateGstSetting/'.$gt->id)}}">
                        Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$gt->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$gstSetting->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>