<div class="container">
       <div class="row">
    
        <div class="col-md-2">
        <h5>PO List</h5>
    </div>
    <div class="col-md-7">
  
         <span class="alert alert-info" style="border-radius: 100px; font-size: 12px; font-weight: 900">Generated - {{$totalPoGenerated}}</span>&nbsp;
     <span class="alert alert-warning"  style="border-radius: 100px; font-size: 12px; font-weight: 900">Pending - {{ $totalPoGenerated - ($poApproved + $poRejected) }} </span>&nbsp;
     <span class="alert alert-success"  style="border-radius: 100px; font-size: 12px; font-weight: 900">Approved - {{$poApproved}}</span>&nbsp;
     <span class="alert alert-danger"  style="border-radius: 100px; font-size: 12px; font-weight: 900">Rejected - {{$poRejected}}</span>


    <div style="float: right">
        <a href="{{url('wishlist')}}"
        class="btn btn-success"> + New PO</a>
        </div>
     </div>

        <div class="col-md-3">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-purchase-order')}}?search='+this.value)"
                       placeholder="Search by PO #ID" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-purchase-order')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         
    </div>
     
    @foreach($purchaseOrder as $pure)
    <table class="table table-bordered bg-light" style="margin-top: 10px">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No.</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=invoiceid&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    PO #ID
                </a>
                {{request()->session()->get('field')=='invoiceid'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
          
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    PO Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
              <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=stock_status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Stock Status
                </a>
                {{request()->session()->get('field')=='stock_status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
           
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Date Created
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
           
           
        @endphp



        
                      @foreach($pure as $purchase)

            
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">#{{ $purchase->invoiceid }}</td>
                 <td style="vertical-align: middle"><!-- {{$purchase->status==1?'Created': 'Pending'}} -->
                   
                    
                  

                    @if($purchase->mail_status == 1)
                    <img src="{{ URL::to('/') }}/images/mailsent.png" alt="Mail Sent for Approval" title="Mail Sent for Approval" height="30" width="30" style="margin:5px;">@else <img src="{{ URL::to('/') }}/images/mailnotsent.png" alt="Mail not Sent for Approval" title="Mail not Sent for Approval" height="30" width="30" style="margin: 5px;" >
                    @endif

                    @if($purchase->approval_status == 1)
                    <span class="alert alert-success">Approved</span>
                    @elseif($purchase->approval_status == 2)
                    <span class="alert alert-danger">Rejected</span>
                    @else
                    <span class="alert alert-warning">Pending</span>
                    @endif
                     

                 </td>
                <td style="vertical-align: middle;  font-style: italic;">
                    


                    <input type="hidden" name="invoiceidArr[]" value="{{ $purchase->invoiceid }}"/>

                    
                    <span class="alert" id="display{{ $purchase->invoiceid }}"></span>


                    @if($purchase->totalQuantity == $purchase->receivedStock)
                    <span class="alert alert-success">Complete ({{$purchase->receivedStock}}/{{$purchase->totalQuantity}} Unit Added to Stock)</span>
                    @elseif($purchase->totalQuantity > $purchase->receivedStock && $purchase->receivedStock !== 0)
                    <span class="alert alert-warning">Partial ({{$purchase->receivedStock}}/{{$purchase->totalQuantity}} Unit Added to Stock)</span>
                    @else
                    <span class="alert alert-danger">Pending  ({{$purchase->receivedStock}}/{{$purchase->totalQuantity}} Unit Added to Stock)</span>
                    @endif







                </td>
               
               
                
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($purchase->created_at))}}</td>
                <td style="vertical-align: middle; display: inline-flex;" align="center">
                    <a class="btn btn-primary btn-sm" title="View"
                       href="{{url('manage-purchase-order/updatePurchaseOrder/'.$purchase->id)}}" style="margin-right: 2px;">
                        View</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$purchase->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
            @endforeach

 </tbody>
    </table>


 <!-- <nav>
        <ul class="pagination justify-content-end">
          $pure->links('vendor.pagination.bootstrap-4') 
        </ul>
    </nav> -->

        @endforeach
       
    
    
   
</div>





<script type="text/javascript">
    
$(document).ready(function(){

var poidArr = $("input[name='invoiceidArr[]']")
              .map(function(){return $(this).val();}).get();

jQuery.each(poidArr, function(index, item) {
  
   ajaxStockReceived(item);


});

})







</script>