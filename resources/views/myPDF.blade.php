     <!DOCTYPE html>
     <html>
     <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <title>Purchase Order Reciept <?php echo date("mdyhis");?></title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
          <style>
          table.table {
          clear: both;
          border-collapse: collapse;
          }
          table.table td, .table th {
          border-top: 0px solid #ddd !important;
           font-size: 10px !important;
           padding: 5px !important
          }
    </style>
     </head>
     <body>
      
     
     <?php  

     $reciepts = Session::get('88uuiioo99888_cart_items');
     


     ?> 
         <!-- New PDF Format Starts-->
         <table class="table">
          <tr>
          <td colspan="3" style="text-align: center; font-size: 20px"><img src="images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" ></td>
          </tr>
          <tr>
          <th colspan="3" style="text-align: center; font-size: 20px"><u>PURCHASE ORDER</u></th>
          </tr>
          </table>

          <table class="table">
          <tr>
          <th >PURCHASE ORDER No.-</th>
          <td>#<?php echo $invoiceid ?></td>
          <th>Date Requested:</th>
          <td >
          <?php 
          $l=0;
          foreach ($reciepts as $key => $val) {
          if($l==0){
          echo $val->invoicedate;
          }
          $l++;
          }
          ?>
          </td>
          </tr>
          </table>
        
         
          <table class="table" style="width: 48%; float: left; border-right: 1px solid #000">
          <tr>
          <th>Purchaser  Details:</th>
          <td>
          <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->companyname;
          }
          $l++;
          }
          ?></td>

          </tr>
          <tr>
          <th>Address:</th>
          <td> <?php 
          $l=0;
          
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->companyaddress;
          }
          $l++;
          }
          ?></td>
          </tr>
          <tr>
          <th>GST No.:</th>
          <td> <?php 
          $l=0;
          
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->gstno;
          }
          $l++;
          }
          ?></td>
          </tr>
          </table>
         
          <table class="table" style="width: 48%; float: right;">
          <tr>
          <th colspan="3">Vendor Information:</th>
          </tr>
          <tr>
          <th>Name</th>
          <td  colspan="2">   
          <?php
          $l=0;
          foreach ($reciepts as $key => $val) {
          if($l==0){
          echo $val->vendor;  
          }
          $l++;
          }
          ?>
          </td>
          </tr>
          <tr>
          <th>Address</th>
          <td  colspan="2">
           <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->vendoraddress;
          }
          $l++;
          }
          ?></td>
          </tr>
          </table>
         
          <table class="table">
          <thead>
          <tr>
          <th>#</th>
          <th>Description</th>
          <th>Qty</th>
          <th>Price(Per Unit)</th>
          <th>GST%</th>
          <th>GST (Amount)</th>
          <th>Total</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          $totalGst = 0;
          $totalPrice = 0;
          $totalQty = 0;
          $i = 0;
          $price_with_gst = 0;
          $totalwithqty = 0;
          foreach ($reciepts as $key => $value) {
          ?>
          <tr>
          <td>#<?php echo  $value->id;?></td>
          <td><?php echo  $value->name ." - ".$value->brand."  ".$value->specification;  ?>
          
          </td>
          <td> <?php echo  $value->quantity;?></td>
          <td>INR. <?php echo  $value->price;?></td>
          <td> <?php echo  $value->gst;?>%</td>
          <?php
          
          $totalwithqty = $value->quantity * $value->price;
          $gstvalue = $value->gst/100 * $totalwithqty;
          $price_with_gst = $totalwithqty + ($value->gst/100 * $totalwithqty);
          ?>

          <td> INR. <?php echo  $gstvalue; ?></td>
          <td> INR. <?php echo  $price_with_gst; ?></td>

          </tr>
          <?php 
          $totalQty += $value->quantity;

          $totalGst += $gstvalue;
          $totalPrice += $price_with_gst;
          $i++;
          }
          $i;
          $totalPrice;
          ?>
          </tbody>
          </table>
          
     <br/>
         <table class="table"  style="width: 50%; float: left;">
          <tr>
          <th>GST Total:</th>
         
          </tr>

          <tr>
          <th>Sub Total:</th>
           
          </tr>
           
          <tr>
          <th>Deliver to whom:</th>
           
          </tr>
          <tr>
          <th>Delivery location:</th>
          
          </tr>
          <tr>
          <th>Delivery instructions:</th>
          
          </tr>

          <tr>
          <th>Approver’s Signature</th>
         
          </tr>

          </table>


          <table class="table"  style="width: 50%; float: right; border-collapse: collapse;">
          <tr>
           
          <td style="border-top: 1px solid #ddd">INR. <?php echo  $totalGst;?></td>
          </tr>

          <tr>
          
          <td style="border-top: 1px solid #ddd">INR. <?php echo  $totalPrice;?></td>
          </tr>
          
          <tr>
          
          <td style="border-top: 1px solid #ddd">HK Department</td>
          </tr>
          <tr>
          
          <td style="border-top: 1px solid #ddd"> <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->delivarylocation;
          }
          $l++;
          }
          ?></td>
          </tr>
          <tr>
           
          <td style="border-top: 1px solid #ddd"> <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->delivaryinstruction;
          }
          $l++;
          }
          ?></td>
          </tr>

          <tr>
          
          <td style="border-top: 1px solid #ddd"><?php if($poapprovestatus == 1 || $poapprovestatus == 2){?>
                    <img src="images/signature.png" alt="Dotsquares Logo" height="60" width="120" >
                    <?php } else {?>
                    <i>________________________</i>
                    <?php } if($poapprovestatus == 2){ ?>


                         
                         <span class="alert alert-danger pull-right" style="float: right">Date Rejected : <?php echo date('d/m/Y', strtotime($getDt->updated_at));?></span>
                        <?php } elseif($poapprovestatus == 1){ ?>
                        <span class="alert alert-success pull-right" style="float: right">Date Approved : <?php echo date('d/m/Y', strtotime($getDt->updated_at));?></span>
                        <?php } else{ ?>
                        <span class="alert alert-warning pull-right" style="float: right">Approval Pending !!</span>
                        <?php } ?></td>
          </tr>

          </table>
          <!-- New PDF Format Ends-->

      

      
     </body>
     </html>

