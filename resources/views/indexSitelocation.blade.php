<div class="container">
    
    
   
    <div class="row">
       
        <div class="col-md-4 form-group pull-left">
        <h5 style="padding-top: 10px">Site Location List</h5>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-sitelocation-category')}}?search='+this.value)"
                       placeholder="Search by Site, Floor or Location" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button id="btn-search" type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-sitelocation-category')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-4 form-group pull-right">
        <div class="input-group">
        <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-sitelocation-category/addSite')}}"
        class="btn btn-success" style="width: 32%"> + Site </a>   &nbsp;&nbsp;
         <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-sitelocation-category/addFloor')}}"
        class="btn btn-success" style="width: 32%"> + Floor</a>&nbsp;&nbsp;
         <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-sitelocation-category/addLocation')}}"
        class="btn btn-success" style="width: 32%"> + Location </a>
        </div>
    </div>
    </div>
    
 
 
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                 <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=name&typefield=location&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Location 
                    </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
             
            </th>
             <th style="vertical-align: middle">
                 <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=name&typefield=floor&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Floor 
                    </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
                 
            </th>
            <th style="vertical-align: middle">
                 
                      <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=name&typefield=site&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Site 
                    </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
                
                 
            </th>
            
            
            
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($sitelocation as $siteloc)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $siteloc->locationName }}</td>
                <td style="vertical-align: middle">{{$siteloc->floorName}}</td>
                <td style="vertical-align: middle">{{ $siteloc->siteName }}</td>
                
                <td style="vertical-align: middle" align="center">
                 
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$siteloc->locationID}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$sitelocation->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>


