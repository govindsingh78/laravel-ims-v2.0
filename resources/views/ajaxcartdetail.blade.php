<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMS::Dotsquares</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <!-- Styles -->
    <style>
         .row{
            font-size: 12px !important;
         }
         tr {
            height: 25px !important;
            margin: 0px !important;
            padding: 0px !important;
         }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
 <?php

var_dump(session()->get('88uuiioo99888_cart_items'));

 ?>
<div  id="wishlist">
    <div class="container cart" style="margin-top: 40px; border: 1px solid orange; border-radius: 20px; overflow: hidden; height: auto;">
        <div class="row">
            
            <div class="col-lg-12">
                   
                    

                   

                    <table class="table">
                        <tr>
                        <td colspan="3" style="text-align: center; font-size: 20px"><img src="{{ URL::to('/') }}/images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" ></td>
                        </tr>
                     <tr>
                    <th colspan="3" style="text-align: center; font-weight: 900"><u>PURCHASE ORDER</u></th>
                    </tr>
                    </table>
                        
                    <table class="table">
                    <tr>
                    <th >PURCHASE ORDER No.-</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  #@{{ items[itemCount - 1].invoiceidpo }} </span>
                     </div></td>
                     <th>Date Requested:</th>
                    <td ><p v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0'>  @{{ items[itemCount - 1].invoicedate }} </span>
                     </p></td>
                    </tr>
                    </table>
                    <hr style="width: 1px; height: 100px; position: absolute;   background-color: rgb(0, 0, 0);margin: auto;padding: 0px; left: 50%;" />
                                       
                    <table class="table" style="width: 48%; float: left;">
                    <tr>
                    <th>Purchaser  Details:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyname }} 
                         <input id="poorderid"  type="hidden" name="poorderid" :value="items[itemCount - 1].invoiceidpo">

                    </span>

                   
                     </div>

                     

                        
                    </td>
                    </tr>
                    <tr>
                    <th>Address:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyaddress }} </span>
                    </div></td>
                    </tr>
                    <tr>
                    <th>GST No.:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].gstno }} </span>
                     </div></td>
                    </tr>
                    </table>

                    <table class="table" style="width: 48%; float: right;">
                    <tr>
                    <th colspan="3">Vendor Information:</th>
                    </tr>
                    <tr>
                    <th>Name</th>
                    <td  colspan="2"> <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendor }} </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Address</th>
                    <td  colspan="2"  ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendoraddress }} </span>
                     </div></td>
                    </tr>
                    </table>
                    <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Price(Per Unit)</th>
                        <th>GST%</th>
                        <th>GST (Amount)</th>
                        <th>Total</th>
                        
                         
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>@{{ item.id }} </td>
                        <td>@{{ item.name }} - @{{ item.brand }}  @{{ item.specification }}<br/><span :id="'stockSt'+item.id"></span>
                         
                        </td>
                        <td>@{{ item.quantity }}</td>
                        <td>INR. @{{ item.price }}</td>
                        <td>@{{ item.gst }}%</td>

                        <td>INR. @{{ item.gstamountperitem }}</td>
                        <td>INR. @{{ item.totalperitem }}</td>
                        <td>INR. @{{ item.totalperitem }}</td>

                        
                   
                       
                    </tr>

 
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <th>GST Total:</th>
                        <td> @{{ 'INR. ' + gsttotal }}</td>
                    </tr>
                    <tr>
                        <th>Sub Total:</th>
                        <td>@{{ 'INR. ' + details.sub_total.toFixed(2) }}</td>
                    </tr>
                </table>

                <table class="table">
                    <tr>
                    <th>Deliver to whom:  </th>
                    <td  colspan="2">HK Department</td>
                    </tr>
                    <tr>
                    <th>Delivery location:</th>
                    <td  colspan="2"><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivarylocation }} </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Delivery instructions:</th>
                    <td  colspan="2"  ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivaryinstruction }} </span>
                     </div></td>
                    </tr>

                     <tr>
                    <th>Approver’s Signature</th>
                    <td  colspan="2"><i>_______________</i></td>
                    </tr>
                </table>





                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script>



    (function($) {
        var _token = '<?php echo csrf_token() ?>';
        $(document).ready(function() {
              var _token = '<?php echo csrf_token() ?>';
            var wishlist = new Vue({
                el: '#wishlist',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    companies: [],
                    companiesdetails: [],
                    vendors: [],
                    vendoraddresses: [],
                    sites: [],
                    floors: [],
                    locations: [],
                   
                    itemCount: 0,
                    items: [],
                    item: {
                        
                        id: '',
                        name: '',
                        specification: '',
                        brand: '',
                        vendor: '',
                        warranty: '',
                        price: '',
                        qty: '',
                        gst: '',
                        site: '',
                        floor: '',
                        location: '',
                        invoicedate: '',
                        companyname: '',
                        gstno: '',
                        companyaddress: '',
                        vendoraddress: '',
                        delivaryinstruction: '',
                        gsttotal: '',
                        
                        
                    },
                    options: {
                        id: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]

                        
                    },
                      options: {
                        
                        name: [
                            {label: 'AC', key: 'AC'}
                            
                        ]

                      
                    },
                      options: {
                         

                        gst: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    },
                     options: {
                         

                        specification: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    }
                },
                mounted:function(){
                   
                    this.loadItems();
                    this.loadSites();
                    this.loadCompanies();
                    this.loadVendors();


                   
                 
                },

                computed: {
                  gsttotal: function() {
                        if (!this.items) {
                        return 0;
                        }
                        return this.items.reduce(function (gsttotal, item){
                        return gsttotal + Number(item.gstamountperitem);




                    }, 0);
                  }
                },
                methods: {
                    loadVendors: function() {
                        var _this = this;
                        this.$http.get('/vendors',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendors = success.body.data;
                    
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadVendorAddress: function(id) {
                        var _this = this;
                        this.$http.get('/vendoraddresses/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendoraddresses = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                     },

                     
                   


                    loadCompanies: function() {
                        var _this = this;
                        this.$http.get('/companies',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companies = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadGstAddress: function(id) {
                        var _this = this;
                        this.$http.get('/companiesdetails/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companiesdetails = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadSites: function() {
                        var _this = this;
                        this.$http.get('/sites',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.sites = success.body.data;
                             this.loadFloors();
                              this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadFloors: function(id){
                        console.log(id)
                        var _this = this;
                        this.$http.get('/floors/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.floors = success.body.data;
                             this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadLocations: function(id) {
                        console.log(id)
                        var _this = this;
                        this.$http.get('/locations/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.locations = success.body.data;
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadStock: function(id) {
                         console.log(id)
                         ajaxSaveStock(id);
                    },
                    loadStockReceivedStatusPerUnit: function(id, evt) {
                        this.$emit("click");
                         console.log(id);
                         ajaxSaveReceivedStock(id);
                          console.log("etwyeeytwgggg");
                    },

                   
                    addItem: function() {
                        var _this = this;
                        console.log(_this.item.invoicedate);
                        this.$http.post('/wishlist',{
                            _token:_token,
                            id:_this.item.id,
                            name:_this.item.name,
                            specification:_this.item.specification,
                            brand:_this.item.brand,
                            vendor:_this.item.vendor,
                            warranty:_this.item.warranty,
                            price:_this.item.price,
                            qty:_this.item.qty,
                            gst:_this.item.gst,
                            site:_this.item.site,
                            floor:_this.item.floor,
                            location:_this.item.location,
                            invoicedate:_this.item.invoicedate,
                            companyname:_this.item.companyname,
                            gstno:_this.item.gstno,
                            companyaddress:_this.item.companyaddress,
                            vendoraddress:_this.item.vendoraddress,
                            delivaryinstruction:_this.item.delivaryinstruction,
                            }).then(function(success) {
                            _this.loadItems();
                            console.log($("#invoicedate").val());
                            $('html, body').animate({
                            scrollTop: $("#mycartdetail").offset().top
                            }, 2000);
                        }, function(error) {
                            console.log($("#invoicedate").val());
                            if($("#invoicedate").val() == ""){
                                 alert("Invoice Date is Blank !!");
                                 $("#invoicedate").focus();
                            } 
                            else if($("#companyname").val() == ""){
                                 alert("Company Name is Blank !!");
                                 $("#companyname").focus();
                            }
                            else if($("#gstno").val() == ""){
                                 alert("Choose GST No. !!");
                                 $("#gstno").focus();
                            }
                            else if($("#companyaddress").val() == ""){
                                 alert("Company Address is Blank !!");
                                 $("#companyaddress").focus();
                            }
                            else if($("#productvendor").val() == ""){
                                 alert("Product Vendor is Blank !!");
                                 $("#productvendor").focus();
                            }
                            else if($("#vendoraddress").val() == ""){
                                 alert("Vendor Address is Blank !!");
                                 $("#vendoraddress").focus();
                            }
                            else if($("#productname").val() == ""){
                                 alert("Product Name is Blank !!");
                                 $("#productname").focus();
                            }else if($("#productspecification").val() == ""){
                                 alert("Product Specification is Blank !!");
                                 $("#productspecification").focus();
                            }else if($("#productbrand").val() == ""){
                                 alert("Product Brand is Blank !!");
                                 $("#productbrand").focus();
                            }else if($("#productwarranty").val() == ""){
                                 alert("Product Warranty is Blank !!");
                                 $("#productwarranty").focus();
                            }else if($("#productprice").val() == "" || $.isNumeric($("#productprice").val()) == false){
                                 alert("Please fill price carefully !!");
                                 $("#productprice").focus();
                            }else if($("#productqty").val() == "" || $.isNumeric($("#productqty").val()) == false){
                                 alert("Please fill quantity carefully !!");
                                 $("#productqty").focus();
                            }else if($("#productgst").val() == ""){
                                 alert("Product GST is Blank !!");
                                 $("#productgst").focus();
                            } else if($("#sitename").val() == ""){
                                 alert("Site Name is Blank !!");
                                 $("#sitename").focus();
                            }else if($("#floorname").val() == ""){
                                 alert("Floor Name is Blank !!");
                                 $("#floorname").focus();
                            }else if($("#locationname").val() == ""){
                                 alert("Location Name is Blank !!");
                                 $("#locationname").focus();
                            }
                        });
                    },
                    removeItem: function(id) {
                        var _this = this;
                        this.$http.delete('/wishlist/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {
                        var _this = this;
                        this.$http.get('/wishlist',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {
                        var _this = this;
                        this.$http.get('/wishlist/details').then(function(success) {
                            _this.details = success.body.data;
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });
        });
    })(jQuery);


</script>
<script src="{{ asset('/js/ajax-crud-modal-form.js') }}"></script>
</body>
</html>



<?php
 //echo "<pre>";
 //var_dump((session()->get('88uuiioo99888_cart_items')));

?>
