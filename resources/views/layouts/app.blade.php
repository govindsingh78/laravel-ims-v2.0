<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'IMS::Dotsquares') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

     
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

     @yield('css')


     <style type="text/css">
         body{
            margin: 0;
            padding: 0;
            width: 100%;
            overflow-x: hidden;
            height: auto;
         }
     </style>
 
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/images/logo.png')}}" height="30" width="150" alt="Dotsquares Logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <!-- New Nav Bars Added for IMS -->
                             
                             <!-- New Nav Bars Added for IMS -->
                              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Manage <span class="caret"></span>
                                </a>

                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('manage-vendors-product') }}">{{ __('Vendor') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-product-category') }}">{{ __('Product') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-brand-category') }}">{{ __('Product Brand') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-specification-category') }}">{{ __(' Specification') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-warranties-category') }}">{{ __('Warranty') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-gst-category') }}">{{ __('GST') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-sitelocation-category') }}">{{ __('Site Location') }}</a>
                                    
                                  </div>

                                
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-purchase-order') }}">{{ __('Purchase Order') }}</a>
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-stock') }}">{{ __('Stock Management') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                  

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('login') }}">{{ __('Dashboard') }}</a>


                                         <a class="dropdown-item" href="{{ route('manage-gst-setting') }}">{{ __('GST Setting') }}</a>


                                          <a class="dropdown-item" href="{{ route('manage-mail-setting') }}">{{ __('Mail Setting') }}</a>
                                         
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>



        <footer class="footer">
        <div class="container" style="text-align: center;">

        <span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
        </div>
        </footer>
    </div>
</body>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>



@yield('js')


 <script type="text/javascript">

        jQuery(document).ready(function(){

        jQuery('#search').val('');
        jQuery('button[type=submit]').click();


        })


        </script>


</html>
