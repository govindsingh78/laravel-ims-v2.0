<div class="container">
    
    
   
    <div class="row">
      
        <div class="col-md-6 form-group pull-left">
        <h5 style="padding-top: 10px">Vendor List</h5>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-vendor-and-product-category')}}?search='+this.value)"
                       placeholder="Search by Vendor Name" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-vendor-and-product-category')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
        <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-vendor-and-product-category/create')}}"
        class="btn btn-success">Add New Vendor</a>
        </div>
    </div>
    </div>
     
   
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=name&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Name
                </a>
                {{request()->session()->get('field')=='name'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=email&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Email
                </a>
                {{request()->session()->get('field')=='email'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=address&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Address
                </a>
                {{request()->session()->get('field')=='address'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=contact_no&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Contact No.
                </a>
                {{request()->session()->get('field')=='contact_no'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-vendor-and-product-category?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($vendor as $vend)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $vend->name }}</td>
                <td style="vertical-align: middle">{{ $vend->email }}</td>
                <td style="vertical-align: middle">{{$vend->address}}</td>
                <td style="vertical-align: middle">{{$vend->contact_no}}</td>
                <td style="vertical-align: middle; font-style: italic;">{{  $vend->status == 1 ? 'Active' : 'Inactive' }}</td>
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($vend->created_at))}}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-vendor-and-product-category/update/'.$vend->id)}}">
                        Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$vend->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$vendor->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>