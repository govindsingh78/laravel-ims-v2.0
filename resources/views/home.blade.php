@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">IMS Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   Here list will be showing for all the pending Purchase Order(PO)
                   <br/>
                   <i>Means: Suppose ac has been purchased but not yet delivered once delivered to Dotsquares Pending PO will be moved to Stock Table</i> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
