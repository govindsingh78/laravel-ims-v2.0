
<li class="nav-item">
<a class="nav-link" href="{{ url('/') }}">{{ __('Home') }}</a>
</li>
<li class="nav-item">
<a class="nav-link" href="{{ url('/about') }}">{{ __('About Us') }}</a>
</li>
<li class="nav-item">
<a class="nav-link" href="{{ url('/contact') }}">{{ __('Contact') }}</a>
</li>