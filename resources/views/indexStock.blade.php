<div class="container">
    
    
   
    <div class="row">
       
        <div class="col-md-6 form-group pull-left">
        <h1 style="font-size: 1.3rem; margin-top: 10px;">Manage Stock </h1>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-stock')}}?search='+this.value)"
                       placeholder="Search by PO, Product & Vendor" name="search"
                       type="text" id="search" autocomplete="off" />
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-stock')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
         <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-stock/addStockItem')}}"
        class="btn btn-success" style="width: 100%"> Add to Stock </a> 
        </div>
    </div>
    </div>
     
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No.</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=invoiceID&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    PO #ID
                </a>
                {{request()->session()->get('field')=='invoiceID'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
           
          
            
           
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=prodName&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                  Product 
                </a>
                {{request()->session()->get('field')=='prodName'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>


              <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=site_moved_to&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Current Site Location


                </a>
                {{request()->session()->get('field')=='site_moved_to'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>

            <th style="vertical-align: middle">
            <a href="javascript:ajaxLoad('{{url('manage-stock?field=receivedDate&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">


            Date Received


            </a>
            {{request()->session()->get('field')=='receivedDate'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>


            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        
        @endphp






        @foreach($stockdatas as $stockdata)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle"> #{{ $stockdata->invoiceID }}</td>
                 
                <td style="vertical-align: middle">{{$stockdata->prodName}}</td>

                 <td style="vertical-align: middle;  font-style: italic;">{{$stockdata->site_moved_to}} <br/> {{$stockdata->floor_moved_to}} <br/> {{$stockdata->location_moved_to}}</td>
               
                
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($stockdata->receivedDate))}}</td>
                <td style="vertical-align: middle; display: inline-flex;" align="center">
                     
                    <a class="btn btn-primary btn-sm" title="View Stock Movement"  href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-stock/viewStockMovement/'.$stockdata->stockId)}}" style="margin-right: 5px">
                        Stock History </a>
                        
                        <a class="btn btn-primary btn-sm" title="Edit" href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-stock/updateStockItem/'.$stockdata->stockId)}}" style="margin-right: 5px">
                        Move Stock</a>

                     
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$stockdata->stockId}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$stockdatas->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>