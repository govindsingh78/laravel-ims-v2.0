
 
<div class="modal-header">
    <h5 class="modal-title">Stock Movement</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

    <!-- Lets fetch the product detail here from stock data table -->
            <b><i>Product Information</i></b>
            <br>
            @foreach($stockdatashistory as $stockshistory)
               <p style="border: 2px solid #265192; padding: 10px; border-radius: 25px; font-size: 14px;">PO #ID.  - {{ $stockshistory->invoiceID }}<br>
               Product Name  - {{ $stockshistory->prodName }}<br>
               Vendor Name  - {{ $stockshistory->vendName }}<br>
               Serial No.  - {{ $stockshistory->serialno }}<br>
              Warranty in Years.  - {{ $wiy }}<br>
               Date Received  - {{ date('d M Y', strtotime($stockshistory->receivedDate)) }}<br>
              </p>
             @endforeach

         <b><i>Stock Movement History</i></b>
         <br>
        <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No.</th>
            <th style="vertical-align: middle">
                 
                    Site Detail
                
            </th>
           
            

            <th style="vertical-align: middle">
            

            Date Moved


            
            </th>

            


            
        </tr>
        </thead>
        <tbody >
        @php
            $i=1;
            
        @endphp
        @foreach($stockmovements as $key => $stockmovement)
            <tr class="contentMoved">
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle"> Site Name : {{ $stockmovement->siteName }}<br/>Floor Name : {{ $stockmovement->floorName }}<br/>Location Name : {{ $stockmovement->locationName }}</td>
               
                
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($stockmovement->dateMoved))}}

                @if($key == 0)
                <span class="btn btn-success">Current Site</span>
                @else
                <span class="btn btn-warning">Stock Moved</span> 
                @endif

                </td>



            </tr>
        @endforeach
        </tbody>
    </table>
   


    
   
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    
</div>



 