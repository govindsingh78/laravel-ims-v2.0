<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IMS::Dotsquares</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <!-- Styles -->
    <style>
        .row{
        font-size: 12px !important;
        }
        tr {
        height: 25px !important;
        margin: 0px !important;
        padding: 0px !important;
        }
        .table td, .table th {
        padding: .75rem;
        padding-top: 3px;
        padding-bottom: 3px;
        vertical-align: top;
        border-top: 1px solid #eceeef;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
 <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/images/logo.png')}}" height="30" width="150" alt="Dotsquares Logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto"></ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <!-- New Nav Bars Added for IMS -->
                             
                             <!-- New Nav Bars Added for IMS -->
                              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Manage <span class="caret"></span>
                                </a>
                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('manage-vendors-product') }}">{{ __('Vendor') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-product-category') }}">{{ __('Product') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-brand-category') }}">{{ __('Brand') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-specification-category') }}">{{ __('Specification') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-warranties-category') }}">{{ __('Warranty') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-gst-category') }}">{{ __('GST') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-sitelocation-category') }}">{{ __('Site Location') }}</a>
                                    
                                  </div>

                                
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-purchase-order') }}">{{ __('Purchase Order') }}</a>
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-stock') }}">{{ __('Stock Management') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('login') }}">{{ __('Dashboard') }}</a>

                                           <a class="dropdown-item" href="{{ route('manage-gst-setting') }}">{{ __('GST Setting') }}</a>
                                         
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
       


<div  id="wishlist">
        
        <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 40px">
         @foreach (['danger', 'warning', 'success', 'info'] as $key)
            @if(Session::has($key))
            <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
            @endif
            @endforeach
        </div>
    </div>


    <div class="container" style="border: 1px solid orange; border-radius: 20px; overflow: hidden; height: auto;">
        <div class="row">
            <div class="col-md-12">

                   
                     
                    <table class="table">
                        <tr>
                        <td colspan="3" style="text-align: center; font-size: 20px"><img src="{{ URL::to('/') }}/images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" >

                        @if($poapprovestatus == 2)
                        
                        <div class="col-md-2  alert alert-danger" style="position: absolute;
    top: 2%;
    left: 73%;
    padding: 10px;">Already Rejected !!</div>
                        <form  action="{{ url('manage-approval-process/approvePurchase') }}" method="POST" style="width: 100px;
    position: absolute;
    margin-top: -90px;
    margin-left: 88%;">
                    <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' > 
                    <input id="approvepoid"  type="hidden" name="approvepoid" :value="items[itemCount - 1].invoiceidpo">

                    </span>
                    </div>
                    @csrf
                    <input type="submit" name="submit" value="Approve" class="btn btn-primary pull-right"  onclick="return confirm('Are you sure to approve this Purchase Order !!')">
                    </form> 
                        @elseif($poapprovestatus == 1)
                        <div class="col-md-2  alert alert-success" style="position: absolute;
    top: 2%;
    left: 73%;
    padding: 10px;">Already Approved !!</div>
<form  action="{{ url('manage-approval-process/rejectPurchase') }}" method="POST" style="width: 100px;
    position: absolute;
    margin-top: -90px;
    margin-left: 88%;">

                    <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >   
                    <input id="rejectpoid"  type="hidden" name="rejectpoid" :value="items[itemCount - 1].invoiceidpo">

                    </span>
                    </div>

                    @csrf
                    <input type="submit" name="submit" value="Reject" class="btn btn-danger pull-right" onclick="return confirm('Are you sure to reject this Purchase Order !!')">
                    </form>
                        @else

                       <form  action="{{ url('manage-approval-process/approvePurchase') }}" method="POST" style="width: 100px;
    position: absolute;
    margin-top: -90px;
    margin-left: 80%;">
                    <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' > 
                    <input id="approvepoid"  type="hidden" name="approvepoid" :value="items[itemCount - 1].invoiceidpo">

                    </span>
                    </div>
                    @csrf
                    <input type="submit" name="submit" value="Approve" class="btn btn-primary pull-right"  onclick="return confirm('Are you sure to approve this Purchase Order !!')">
                    </form> 
                    
                    <form  action="{{ url('manage-approval-process/rejectPurchase') }}" method="POST" style="width: 100px;
    position: absolute;
    margin-top: -90px;
    margin-left: 88%;">

                    <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >   
                    <input id="rejectpoid"  type="hidden" name="rejectpoid" :value="items[itemCount - 1].invoiceidpo">

                    </span>
                    </div>

                    @csrf
                    <input type="submit" name="submit" value="Reject" class="btn btn-danger pull-right" onclick="return confirm('Are you sure to reject this Purchase Order !!')">
                    </form>
                     @endif


                        </td>
                        </tr>
                     <tr>
                    <th colspan="3" style="text-align: center; font-weight: 900"><u>PURCHASE ORDER</u></th>
                    </tr>
                    </table>
                       <input type="submit" v-if='itemCount === 0' name="submit" value="Please add at least one item to cart in order to generate PO" disabled class="btn btn-warning" style="float: right; clear: both; width: 100%; border-radius: 0px">
                    <table class="table">
                    <tr>
                    <th >PURCHASE ORDER No.-</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  #@{{ items[itemCount - 1].invoiceidpo }} </span>
                     </div></td>
                     <th>Date Requested:</th>
                    <td ><p v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0'>  @{{ items[itemCount - 1].invoicedate }} </span>
                     </p></td>
                    </tr>
                    </table>
                     
                    <table class="table" style="width: 50%; float: left; border-right: 1px solid #000000">
                    <tr>
                    <th>Purchaser  Details:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyname }} 
                         <input id="poorderid"  type="hidden" name="poorderid" :value="items[itemCount - 1].invoiceidpo">

                    </span>
                    </div>
   
                    </td>
                    </tr>
                    <tr>
                    <th>Address:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].companyaddress }} </span>
                    </div></td>
                    </tr>
                    <tr>
                    <th>GST No.:</th>
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].gstno }} </span>
                     </div></td>
                    </tr>
                    </table>

                    <table class="table" style="width: 50%; float: right;">
                    <tr>
                    <th colspan="3">Vendor Information:</th>
                    </tr>
                    <tr>
                    <th>Name</th>
                    <td  colspan="2"> <div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendor }} </span>
                     </div></td>
                    </tr>
                    <tr>
                    <th>Address</th>
                    <td  colspan="2"  ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].vendoraddress }} </span>
                     </div></td>
                    </tr>
                    </table>
                    <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Price(Per Unit)</th>
                        <th>GST%</th>
                        <th>GST (Amount)</th>
                        <th>Total</th>
                       
                         
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>@{{ item.id }} </td>
                        <td>@{{ item.name }} - @{{ item.brand }}  @{{ item.specification }}<br/><span :id="'stockSt'+item.id"></span>
                         
                        </td>
                        <td>@{{ item.quantity }}</td>
                        <td>INR. @{{ item.price }}</td>
                        <td>@{{ item.gst }}%</td>

                        <td>INR. @{{ item.gstamountperitem }}</td>
                        <td>INR. @{{ item.totalperitem }}</td>
                        
                       

                        
                        
                        <div style="display: none">
                        <a :id="'stockRc'+item.id" class="btn btn-primary  btn-sm checkStock"
                           v-show="loadStockReceivedStatusPerUnit(item.id)" >
                        Stock Status
                         </a>
                     </div>
                       <!--   <a class="btn btn-primary btn-sm" title="Received"  v-bind:href="'#modalReceived'+item.id" data-toggle="modal"
                       data-href="" :id="item.id" style="margin-right: 5px">
                        Stock Received </a> -->
                        
                        
                        <form>
                        <!-- Popup Store -->
                        <div class="modal fade" :id="'modalReceived'+item.id" tabindex="-1" role="dialog" data-backdrop="static">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title">Stock Received Detail  <span :id="'messageSt'+item.id" style="float: right;
    font-size: 14px;
    line-height: 31px;
    margin-left: 42px;"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                        <div v-for="(item, index) in items" :key='index'>
                        <span v-if='index === 0' > PO ID : #@{{ items[itemCount - 1].invoiceidpo }} </span>
                        </div>
                        Product Detail : @{{ item.name }} - @{{ item.brand }}  @{{ item.specification }}
                        <br>
                        Quantity : @{{ item.quantity }}
                        <br>
                        <span :id="'modalstockSt'+item.id"></span>
                        <br/>
                        Available Quantity: <input :id="'availqnty'+item.id" type="number" :value="item.quantity" name="availqnty" :min="1" :max="item.quantity">
                                   

                        <input :id="'prodid'+item.id" type="hidden" name="prodid" :value="item.id">
                       

                        <input :id="'totalQty'+item.id"  type="hidden" name="totalQty" :value="item.quantity">

                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        


                         <button type="button"  :id="'stockpro_'+item.id" class="btn btn-primary stockpro"
                           @click="loadStock(item.id)">
                        Received
                         </button>

                         


                        </div>
                        </div>
                        </div>
                        </div>

                        </form>


                        <!-- <a :id="'tct'+item.id" onclick="alert('hi')">Test Click Trigger</a> -->
                   
                       
                    </tr>

 
                    </tbody>
                </table>
                <table class="table" style="width: 50%; float: left;">
                    <tr>
                        <th>GST Total:</th>
                         
                    </tr>
                    <tr>
                        <th>Sub Total:</th>
                        
                    </tr>
               
                    <tr>
                    <th>Deliver to whom:  </th>
                     
                    <tr>
                    <th>Delivery location:</th>
                   
                    </tr>
                    <tr>
                    <th>Delivery instructions:</th>
                     
                    </tr>

                     <tr>
                    <th>Approver’s Signature</th>
                     
                    </tr>
                </table>
                <table class="table"  style="width: 50%; float: right; border-top: 1px solid">
                    <tr>
                       
                        <td> @{{ 'INR. ' + gsttotal }}</td>
                    </tr>
                    <tr>
                         
                        <td>@{{ 'INR. ' + details.sub_total.toFixed(2) }}</td>
                    </tr>
               
                    <tr>
                     
                    <td >HK Department</td>
                    </tr>
                    <tr>
                    
                    <td ><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivarylocation }} </span>
                     </div></td>
                    </tr>
                    <tr>
                     
                    <td><div v-for="(item, index) in items" :key='index'>
                    <span v-if='index === 0' >  @{{ items[itemCount - 1].delivaryinstruction }} </span>
                     </div></td>
                    </tr>

                     <tr>
                     
                    <td> @if($poapprovestatus == 1 || $poapprovestatus == 2)
                    <img src="{{ URL::to('/') }}/images/signature.png" alt="Dotsquares Logo" height="60" width="120" >
                    @else
                    <i>________________________</i>
                    @endif</td>
                    </tr>
                </table>




 </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
<div class="container" style="text-align: center;">

<span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script  src="https://code.jquery.com/jquery-3.2.1.min.js"  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script>



    (function($) {
        var _token = '<?php echo csrf_token() ?>';
        $(document).ready(function() {
              var _token = '<?php echo csrf_token() ?>';
            var wishlist = new Vue({
                el: '#wishlist',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    companies: [],
                    companiesdetails: [],
                    vendors: [],
                    vendoraddresses: [],
                    sites: [],
                    floors: [],
                    locations: [],
                   
                    itemCount: 0,
                    items: [],
                    item: {
                        
                        id: '',
                        name: '',
                        specification: '',
                        brand: '',
                        vendor: '',
                        warranty: '',
                        price: '',
                        qty: '',
                        gst: '',
                        site: '',
                        floor: '',
                        location: '',
                        invoicedate: '',
                        companyname: '',
                        gstno: '',
                        companyaddress: '',
                        vendoraddress: '',
                        delivaryinstruction: '',
                        gsttotal: '',
                        
                        
                    },
                    options: {
                        id: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]

                        
                    },
                      options: {
                        
                        name: [
                            {label: 'AC', key: 'AC'}
                            
                        ]

                      
                    },
                      options: {
                         

                        gst: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    },
                     options: {
                         

                        specification: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    }
                },
                mounted:function(){
                   
                    this.loadItems();
                    this.loadSites();
                    this.loadCompanies();
                    this.loadVendors();


                   
                 
                },

                computed: {
                  gsttotal: function() {
                        if (!this.items) {
                        return 0;
                        }
                        return this.items.reduce(function (gsttotal, item){
                        return gsttotal + Number(item.gstamountperitem);




                    }, 0);
                  }
                },
                methods: {
                    loadVendors: function() {
                        var _this = this;
                        this.$http.get('/vendors',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendors = success.body.data;
                    
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadVendorAddress: function(id) {
                        var _this = this;
                        this.$http.get('/vendoraddresses/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.vendoraddresses = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                     },

                     
                   


                    loadCompanies: function() {
                        var _this = this;
                        this.$http.get('/companies',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companies = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadGstAddress: function(id) {
                        var _this = this;
                        this.$http.get('/companiesdetails/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.companiesdetails = success.body.data;
                       
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadSites: function() {
                        var _this = this;
                        this.$http.get('/sites',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.sites = success.body.data;
                             this.loadFloors();
                              this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadFloors: function(id){
                        console.log(id)
                        var _this = this;
                        this.$http.get('/floors/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.floors = success.body.data;
                             this.loadLocations();
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadLocations: function(id) {
                        console.log(id)
                        var _this = this;
                        this.$http.get('/locations/'+id,{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.locations = success.body.data;
                            
                        }, function(error) {
                            console.log(error);
                        });
                    },

                    loadStock: function(id) {
                         console.log(id)
                         ajaxSaveStock(id);
                    },
                    loadStockReceivedStatusPerUnit: function(id, evt) {
                        this.$emit("click");
                         console.log(id);
                         ajaxSaveReceivedStock(id);
                          console.log("etwyeeytwgggg");
                    },

                   
                    addItem: function() {
                        var _this = this;
                        console.log(_this.item.invoicedate);
                        this.$http.post('/wishlist',{
                            _token:_token,
                            id:_this.item.id,
                            name:_this.item.name,
                            specification:_this.item.specification,
                            brand:_this.item.brand,
                            vendor:_this.item.vendor,
                            warranty:_this.item.warranty,
                            price:_this.item.price,
                            qty:_this.item.qty,
                            gst:_this.item.gst,
                            site:_this.item.site,
                            floor:_this.item.floor,
                            location:_this.item.location,
                            invoicedate:_this.item.invoicedate,
                            companyname:_this.item.companyname,
                            gstno:_this.item.gstno,
                            companyaddress:_this.item.companyaddress,
                            vendoraddress:_this.item.vendoraddress,
                            delivaryinstruction:_this.item.delivaryinstruction,
                            }).then(function(success) {
                            _this.loadItems();
                            console.log($("#invoicedate").val());
                            $('html, body').animate({
                            scrollTop: $("#mycartdetail").offset().top
                            }, 2000);
                        }, function(error) {
                            console.log($("#invoicedate").val());
                            if($("#invoicedate").val() == ""){
                                 alert("Invoice Date is Blank !!");
                                 $("#invoicedate").focus();
                            } 
                            else if($("#companyname").val() == ""){
                                 alert("Company Name is Blank !!");
                                 $("#companyname").focus();
                            }
                            else if($("#gstno").val() == ""){
                                 alert("Choose GST No. !!");
                                 $("#gstno").focus();
                            }
                            else if($("#companyaddress").val() == ""){
                                 alert("Company Address is Blank !!");
                                 $("#companyaddress").focus();
                            }
                            else if($("#productvendor").val() == ""){
                                 alert("Product Vendor is Blank !!");
                                 $("#productvendor").focus();
                            }
                            else if($("#vendoraddress").val() == ""){
                                 alert("Vendor Address is Blank !!");
                                 $("#vendoraddress").focus();
                            }
                            else if($("#productname").val() == ""){
                                 alert("Product Name is Blank !!");
                                 $("#productname").focus();
                            }else if($("#productspecification").val() == ""){
                                 alert("Product Specification is Blank !!");
                                 $("#productspecification").focus();
                            }else if($("#productbrand").val() == ""){
                                 alert("Product Brand is Blank !!");
                                 $("#productbrand").focus();
                            }else if($("#productwarranty").val() == ""){
                                 alert("Product Warranty is Blank !!");
                                 $("#productwarranty").focus();
                            }else if($("#productprice").val() == "" || $.isNumeric($("#productprice").val()) == false){
                                 alert("Please fill price carefully !!");
                                 $("#productprice").focus();
                            }else if($("#productqty").val() == "" || $.isNumeric($("#productqty").val()) == false){
                                 alert("Please fill quantity carefully !!");
                                 $("#productqty").focus();
                            }else if($("#productgst").val() == ""){
                                 alert("Product GST is Blank !!");
                                 $("#productgst").focus();
                            } else if($("#sitename").val() == ""){
                                 alert("Site Name is Blank !!");
                                 $("#sitename").focus();
                            }else if($("#floorname").val() == ""){
                                 alert("Floor Name is Blank !!");
                                 $("#floorname").focus();
                            }else if($("#locationname").val() == ""){
                                 alert("Location Name is Blank !!");
                                 $("#locationname").focus();
                            }
                        });
                    },
                    removeItem: function(id) {
                        var _this = this;
                        this.$http.delete('/wishlist/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {
                        var _this = this;
                        this.$http.get('/wishlist',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {
                        var _this = this;
                        this.$http.get('/wishlist/details').then(function(success) {
                            _this.details = success.body.data;
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });
        });
    })(jQuery);





 





</script>




<script src="{{ asset('/js/ajax-crud-modal-form.js') }}"></script>
</body>
</html>



<?php
// echo "<pre>";
// var_dump((session()->get('88uuiioo99888_cart_items')));

?>
