     <!DOCTYPE html>
     <html>
     <head>
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
     <title>Purchase Order Reciept <?php echo date("mdyhis");?></title>
       <style>
        .row{
        font-size: 10px !important;
        }
       .ntab tr {
        height: 25px !important;
        margin: 0px !important;
        padding: 0px !important;
        }
        .ntab td, .ntab th {
        padding: .75rem;
        padding-top: 3px;
        padding-bottom: 3px;
        vertical-align: top;
        border-top: 1px solid #eceeef;
        font-size: 12px !important;

        }
    </style>
     </head>
     <body>

      <div class="container" style="border-right: 1px solid #000">    
      
     <div class="row">
      <div class="col-md-12">
     
     <?php  

     $reciepts = Session::get('88uuiioo99888_cart_items');
     $invoiceid = Session::get('invoiceid');
 
     ?> 
          
         <table class="table ntab">
          <tr>
          <td colspan="3" style="text-align: center; font-size: 20px; border-top: 0px"><img src="images/ds-logo-pdf.png" alt="Dotsquares Logo" height="100" width="130" ></td>
          </tr>
          <tr>
          <th colspan="3" style="text-align: center; font-size: 20px"><u>PURCHASE ORDER</u></th>
          </tr>
          </table>

          <table class="table ntab">
          <tr>
          <th >PURCHASE ORDER No.-</th>
          <td>#<?php echo $invoiceid ?></td>
          <th>Date Requested:</th>
          <td >
          <?php 
          $l=0;
          foreach ($reciepts as $key => $val) {
          if($l==0){
          echo $val->invoicedate;
          }
          $l++;
          }
          ?>
          </td>
          </tr>
          </table>
                
          <table class="table ntab"  style="width: 50%; float: left; border-right: 1px solid #000000">
          <tr>
          <th>Purchaser  Details:</th>
          <td>
          <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->companyname;
          }
          $l++;
          }
          ?></td>

          </tr>
          <tr>
          <th>Address:</th>
          <td> <?php 
          $l=0;
          
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->companyaddress;
          }
          $l++;
          }
          ?></td>
          </tr>
          <tr>
          <th>GST No.:</th>
          <td> <?php 
          $l=0;
          
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->gstno;
          }
          $l++;
          }
          ?></td>
          </tr>
          </table>
          
          <table class="table ntab" style="width: 50%; float: right;">
          <tr>
          <th colspan="3">Vendor Information:</th>
          </tr>
          <tr>
          <th>Name</th>
          <td  colspan="2">   
          <?php
          $l=0;
          foreach ($reciepts as $key => $val) {
          if($l==0){
          echo $val->vendor;  
          }
          $l++;
          }
          ?>
          </td>
          </tr>
          <tr>
          <th>Address</th>
          <td  colspan="2">
           &nbsp;&nbsp;<?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->vendoraddress;
          }
          $l++;
          }
          ?></td>
          </tr>
          </table>
          
          <table class="table ntab" >
          <thead>
          <tr>
          <th>#</th>
          <th>Description</th>
          <th>Qty</th>
          <th>Price(Per Unit)</th>
          <th>GST%</th>
          <th>GST (Amount)</th>
          <th>Total</th>
          </tr>
          </thead>
          <tbody>
          <?php 
          $totalGst = 0;
          $totalPrice = 0;
          $totalQty = 0;
          $i = 0;
          $price_with_gst = 0;
          $totalwithqty = 0;
          foreach ($reciepts as $key => $value) {
          ?>
          <tr>
          <td>#<?php echo  $value->id;?></td>
          <td><?php echo  $value->name ." - ".$value->brand."  ".$value->specification;  ?>
          
          </td>
          <td> <?php echo  $value->quantity;?></td>
          <td>INR. <?php echo  $value->price;?></td>
          <td> <?php echo  $value->gst;?>%</td>
          <?php
          
          $totalwithqty = $value->quantity * $value->price;
          $gstvalue = $value->gst/100 * $totalwithqty;
          $price_with_gst = $totalwithqty + ($value->gst/100 * $totalwithqty);
          ?>

          <td> INR. <?php echo  $gstvalue; ?></td>
          <td> INR. <?php echo  $price_with_gst; ?></td>

          </tr>
          <?php 
          $totalQty += $value->quantity;

          $totalGst += $gstvalue;
          $totalPrice += $price_with_gst;
          $i++;
          }
          $i;
          $totalPrice;
          ?>
          </tbody>
          </table>
          
          
          <table class="table ntab"  style="width: 50%; float: left;">
          <tr>
          <th>GST Total:</th>
          </tr>
          <tr>
          <th>Sub Total:</th>
           </tr>
           <tr>
         
          <th>Deliver to whom:</th>
           </tr><tr>
          <th>Delivery location:</th>
          </tr><tr>
          <th>Delivery instructions:</th>
           </tr><tr>
          
          <th>Approver’s Signature</th>
          
          </tr>

          </table>

          <table class="table ntab"  style="width: 50%; float: right; border-top: 1px solid">
          <tr>
          
          <td>INR. <?php echo  $totalGst;?></td>
          </tr>
          <tr>
          <td>INR. <?php echo  $totalPrice;?></td>
          
          </tr>
          <tr>
          <td  >HK Department</td>
           </tr>
          <tr>
          <td  > <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->delivarylocation;
          }
          $l++;
          }
          ?></td>
          </tr>
          <tr>
           
          <td  > <?php 
          $l=0;
          $counton = count($reciepts) -1;
          foreach ($reciepts as $key => $val) {
          if($l == $counton){
          echo $val->delivaryinstruction;
          }
          $l++;
          }
          ?></td>
          
          </tr>
          <tr>
          <td  >@if($poapprovestatus == 1 || $poapprovestatus == 2)
                    <img src="{{ URL::to('/') }}/images/signature.png" alt="Dotsquares Logo" height="60" width="120" >
                    @else
                    <i>________________________</i>
                    @endif


                       <!--  @if($poapprovestatus == 2)
                         <span class="alert alert-danger pull-right" style="float: right">Already Rejected !!</span>
                        @elseif($poapprovestatus == 1)
                        <span class="alert alert-success pull-right" style="float: right">Already Approved !!</span>
                        @else
                        <span class="alert alert-warning pull-right" style="float: right">Approval Pending !!</span>
                        @endif -->


                      @if($poapprovestatus == 2)
                      <span class="alert alert-danger pull-right" style="float: right">Date Rejected : {{ date('d/m/Y', strtotime($getDt->updated_at)) }}  </span>    
                      @elseif($poapprovestatus == 1)
                      <span class="alert alert-success pull-right" style="float: right">Date Approved : {{ date('d/m/Y', strtotime($getDt->updated_at)) }} </span>
                      @else
                      <span class="alert alert-warning pull-right" style="float: right">Approval Pending !!</span>
                      @endif







</td>
          </tr>

          </table>

 

     </div>  </div>
      </div>

    
      
      
     </body>
     </html>

