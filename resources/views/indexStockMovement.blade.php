<div class="container">
    
    
   
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4 form-group pull-left">
        <h1 style="font-size: 1.3rem">Stock History </h1>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-stock-history')}}?search='+this.value)"
                       placeholder="Search by PO, Product & Vendor" name="search"
                       type="text" id="search"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-stock-history')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <!-- <div class="col-md-2 form-group pull-right">
        <div class="input-group">
         <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-stock/addStockItem')}}"
        class="btn btn-success"> Add to Stock </a> 
        </div>
    </div> -->
    </div>
    </div>
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No.</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=siteName&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Site
                </a>
                {{request()->session()->get('field')=='siteName'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
           
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=floorName&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Floor


                </a>
                {{request()->session()->get('field')=='floorName'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            
           
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-stock?field=locationName&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                  Location
                </a>
                {{request()->session()->get('field')=='locationName'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>

            <th style="vertical-align: middle">
            <a href="javascript:ajaxLoad('{{url('manage-stock?field=dateMoved&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">


            Date Moved


            </a>
            {{request()->session()->get('field')=='dateMoved'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>


            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
             
        @endphp
        @foreach($stockmovements as $stockmovement)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle"> #{{ $stockmovement->siteName }}</td>
               
                <td style="vertical-align: middle">{{$stockmovement->floorName}}</td>
               
                   <td style="vertical-align: middle;  font-style: italic;">{{$stockmovement->locationName}}</td>
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($stockmovement->dateMoved))}}</td>
                <td style="vertical-align: middle; display: inline-flex;" align="center">
                     
 
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$stockmovement->smId}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$stockmovements->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>