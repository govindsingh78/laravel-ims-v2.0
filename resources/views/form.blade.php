@if(isset($vendor))
    {!! Form::model($vendor,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($vendor)?'Edit':'New'}} Vendor</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row required">
        {!! Form::label("name","Vendor Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("name",null,["class"=>"form-control".($errors->has('name')?" is-invalid":""),'placeholder'=>'Vendor Name','id'=>'focus']) !!}
            <span id="error-name" class="invalid-feedback"></span>
        </div>
    </div>
    
    <div class="form-group row required">
        {!! Form::label("email","Email",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("email",null,["class"=>"form-control".($errors->has('email')?" is-invalid":""),'placeholder'=>'Email']) !!}
            <span id="error-email" class="invalid-feedback"></span>
        </div>
    </div>


     <div class="form-group row required">
        {!! Form::label("address","Vendor Address",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("address",null,["class"=>"form-control".($errors->has('address')?" is-invalid":""),'placeholder'=>'Vendor Address','id'=>'focus']) !!}
            <span id="error-address" class="invalid-feedback"></span>
        </div>
    </div>


     <div class="form-group row required">
        {!! Form::label("contact_no","Contact No.",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("contact_no",null,["class"=>"form-control".($errors->has('contact_no')?" is-invalid":""),'placeholder'=>'Vendor Contact','id'=>'focus']) !!}
            <span id="error-contact_no" class="invalid-feedback"></span>
        </div>
    </div>



		<div class="form-group row required">
		{!! Form::label("status","Vendor Status",["class"=>"col-form-label col-md-12"]) !!}
		<div class="col-sm-12">

		<select class="form-control" name="status">
		      <option value="">Choose Status</option>
		       @if(isset($vendor))
                      <option value="0" <?php if($vendor->status == 0 ){ echo "selected"; } else{ echo ""; } ?>>Inactive</option>
                      <option value="1" <?php if($vendor->status == 1 ){ echo "selected"; } else{ echo ""; } ?>>Active</option>
                        
                @else
                       <option value="0" >Inactive</option>
                       <option value="1" >Active</option>
                        
                @endif

       
		</select>
		  <span id="error-status" class="invalid-feedback"></span>
		</div>
		  
		</div>









</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}