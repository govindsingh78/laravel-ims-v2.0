@if(isset($purchaseOrder))
    {!! Form::model($purchaseOrder,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($purchaseOrder)?'Edit':'New'}} Order Invoice</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">

        
        <!-- product name for edit -->
       
        <div class="form-group row required">
        {!! Form::label("product_id","Product Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="product_id" id="product_id">
        <option value="">Choose Product</option>
        
            @foreach($manageProduct as $manageProd)
              <option value="{{ $manageProd->id }}" <?php if(isset($purchaseOrder)) { if($purchaseOrder->product_id == $manageProd->id ){ echo "selected"; } else{ echo ""; } }?>>{{ $manageProd->name }}</option>
            @endforeach  
        </select>
        <span id="error-product_id" class="invalid-feedback"></span>
        </div>
        </div>

         <!-- brand name for edit -->


         <div class="form-group row required">
        {!! Form::label("brand_id","Brand Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="brand_id">
        <option value="">Choose Brand</option>
        
            @foreach($manageBrand as $manageBr)
              <option value="{{ $manageBr->id }}" <?php if(isset($purchaseOrder)) { if($purchaseOrder->brand_id == $manageBr->id ){ echo "selected"; } else{ echo ""; } }?>>{{ $manageBr->name }}</option>
            @endforeach  
        </select>
        <span id="error-brand_id" class="invalid-feedback"></span>
        </div>
        </div>



         <!-- vendor name for edit -->


         <div class="form-group row required">
        {!! Form::label("vendor_id","Vendor Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="vendor_id">
        <option value="">Choose Vendor</option>
        
            @foreach($manageVendor as $manageVend)
              <option value="{{ $manageVend->id }}" <?php if(isset($purchaseOrder)) { if($purchaseOrder->vendor_id == $manageVend->id ){ echo "selected"; } else{ echo ""; } } ?>>{{ $manageVend->name }}</option>
            @endforeach  
        </select>
        <span id="error-vendor_id" class="invalid-feedback"></span>
        </div>
        </div>



   <div class="form-group row required">
        {!! Form::label("unit_price","Unit Price",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
        {!! Form::text("unit_price",null,["class"=>"form-control".($errors->has('unit_price')?" is-invalid":""),'placeholder'=>'Unit Price','id'=>'focus']) !!}
        <span id="error-unit_price" class="invalid-feedback"></span>
        </div>
        </div>

          <!-- quantity for edit -->

        <div class="form-group row required">
        {!! Form::label("qty","Quantity",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
        {!! Form::text("qty",null,["class"=>"form-control".($errors->has('qty')?" is-invalid":""),'placeholder'=>'Quantity','id'=>'focus']) !!}
        <span id="error-qty" class="invalid-feedback"></span>
        </div>
        </div>

          <!-- UNIT PRICE for edit -->

      




         <!-- gst value for edit -->


         <div class="form-group row required">
        {!! Form::label("gst_id","GST in %",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="gst_id">
        <option value="">Choose GST(%)</option>
        
            @foreach($manageGst as $manageGs)
              <option value="{{ $manageGs->id }}" <?php if(isset($purchaseOrder)) { if($purchaseOrder->gst_id == $manageGs->id ){ echo "selected"; } else{ echo ""; } } ?>>{{ $manageGs->gst_in_percentage }}</option>
            @endforeach  
        </select>
        <span id="error-gst_id" class="invalid-feedback"></span>
        </div>
        </div>




       
         <!-- total PRICE for edit -->

         <div class="form-group row required">
        {!! Form::label("total_price","Total Price",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
        {!! Form::text("total_price",null,["class"=>"form-control".($errors->has('total_price')?" is-invalid":""),'placeholder'=>'Total Price','id'=>'focus total_price']) !!}
        <span id="error-total_price" class="invalid-feedback"></span>
        </div>
        </div>





         <!-- warranty value for edit -->


         <div class="form-group row required">
        {!! Form::label("warranty_id","Warranty in Years",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="warranty_id">
        <option value="">Choose Warranty</option>
        
            @foreach($manageWarranty as $manageWarrant)
              <option value="{{ $manageWarrant->id }}"  <?php if(isset($purchaseOrder)) { if($purchaseOrder->warranty_id == $manageWarrant->id ){ echo "selected"; } else{ echo ""; } } ?>>{{ $manageWarrant->warranty_in_years }}</option>
            @endforeach  
        </select>
        <span id="error-warranty_id" class="invalid-feedback"></span>
        </div>
        </div>


           <!-- warranty value for edit -->


        

          <!-- stock_status value 0 for pending po and 1 for moved to stock means recieved -->


         <div class="form-group row required">
        {!! Form::label("stock_status","Stock Status",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="stock_status">
        <option value="">Stock Status</option>
        
           
              <option value="0"  <?php if(isset($purchaseOrder)) {  if($purchaseOrder->stock_status == 0 ){ echo "selected"; } else{ echo ""; } }?>>Pending PO</option>
               <option value="1"  <?php if(isset($purchaseOrder)) {  if($purchaseOrder->stock_status == 1 ){ echo "selected"; } else{ echo ""; } }?>>Moved to Stock</option>
            
        </select>
        <span id="error-stock_status" class="invalid-feedback"></span>
        </div>
        </div>



           <!-- status value 1 for valid po and 0 for invallid po -->


        <div class="form-group row required">
        {!! Form::label("site_location_id","Site Detail",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="site_location_id">
        <option value="">Choose Site Location</option>
        
            @foreach($manageSitelocation as $manageSite)
              <option value="{{ $manageSite->id }}"   <?php if(isset($purchaseOrder)) { if($purchaseOrder->site_location_id == $manageSite->id ){ echo "selected"; } else{ echo ""; } } ?>>{{ $manageSite->site }} {{ $manageSite->floor }} {{ $manageSite->location }}</option>
            @endforeach  
        </select>
        <span id="error-site_location_id" class="invalid-feedback"></span>
        </div>
        </div>


 
 


        <div class="form-group row required">
        {!! Form::label("specification_ids","Product Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="specification_ids" multiple="multiple">
        <option value="">Choose Specification</option>
        
            @foreach($manageSpecification as $manageSpecific)
              <option value="{{ $manageSpecific->id }}" >{{ $manageSpecific->description }}</option>
            @endforeach  
        </select>
        <span id="error-specification_ids" class="invalid-feedback"></span>
        </div>
        </div>

      

        

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}


