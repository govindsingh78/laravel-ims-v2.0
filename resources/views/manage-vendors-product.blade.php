
@extends('layouts.app')
@section('content') 
<div class="container">

                <!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Vendors</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <!-- Here form to add vendors -->
         {!! Form::open(['url'=> 'addVendor', 'files' => false, 'id'=>'form_vendor']) !!} 


                             {!! method_field('post') !!}
                             {!! csrf_field() !!}

                        <div class="form-group clearfix col-md-12">
                            {!! Form::label('Vendor Name',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('name','',['id'=>'vname','class'=>'form-control','placeholder'=>'Vendor Name']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('name') }}</div>
                        </div>

                         <div class="form-group clearfix col-md-12">
                            {!! Form::label('Vendor Email',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Vendor Email']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('email') }}</div>
                        </div>

                        <div class="form-group clearfix col-md-12">
                            {!! Form::label('Vendor Address',null,['class'=>'col-sm-6 control-label no-padding-right']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('address',null,['class'=>'form-control','placeholder'=>'Vendor Address']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('address') }}</div>
                        </div>

                        <div class="form-group clearfix col-md-12">
                            {!! Form::label('Contact No.',null,['class'=>'col-sm-6 control-label no-padding-right required_label']) !!} 
                            <div class="col-sm-12">
                                {!! Form::text('contact_no',null,['class'=>'form-control','placeholder'=>'Contact No.']) !!}    
                            </div>
                            <div class="col-sm-12 text-danger">{{ $errors->first('contact_no') }}</div>
                        </div>
                         

                          <div class="form-group clearfix col-md-12">
                            {!! Form::label('Vendor Status',null,['class'=>'col-sm-6 control-label no-padding-right']) !!}
                            <div class="col-sm-12">
                            
                            <select class="form-control" name="status">
                            <option value="">Choose Status</option>
                             <option value="0">Inactive</option>
                            <option value="1">Active</option>
                            
                            </select>
                            
                            </div>
                             <div class="col-sm-12 text-danger">{{ $errors->first('status') }}</div>
                        </div>


                          <div class="form-actions col-md-12">
                            <div class="col-sm-12">
                            {!! Form::button('Close', array('type' => 'button', 'class'=> 'btn btn-secondary pull-right', 'data-dismiss'=> 'modal')) !!}
                            {!! Form::button('Save', array('type' => 'submit', 'class'=> 'btn btn-success pull-right' ,'id' => 'btnSave')) !!}
 


                            </div>
                            <div class="clearfix"></div>
                        </div>
                    {!! Form::close() !!}



      </div>
     
    </div>
  </div>
</div>




    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                   <div class="col-md-4"style="width: auto; float: left"> 
                    Manage Category  
                   </div>
               <div class="col-md-4" style="width: auto; float: right">
                <div class="form-group">                
                   
                   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#basicExampleModal"> + Add Category </button>

                </div>
                </div>
                <div class="col-md-4" style="width: auto; float: right">
                <div class="form-group">
                   <select class="form-control" id="selectCategory">
                        <option>Select Category</option>
                        <option>Vendor(s)</option>
                        <option>Products</option>
                        <option>Brand</option>
                        <option>Capacity</option>
                        <option>Warranty</option>
                        <option>GST</option>
                        <option>Site</option>
                        <option>Floor</option>
                        <option>Area</option>
                    </select>
                </div>
            </div>


            </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   
                    <table class="table table-striped table-hover ">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                          <th>Column heading</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>1</td>
                          <td>Column content</td>
                          <td>Column content</td>
                          <td>Column content</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>Column content</td>
                          <td>Column content</td>
                          <td>Column content</td>
                        </tr>
                         
                      </tbody>
                </div>
            </div>
        </div>
    </div>
</div> 
 
@endsection

