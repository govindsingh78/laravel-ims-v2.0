

$(document).on('click', 'a.page-link', function (event) {
    event.preventDefault();
    ajaxLoad($(this).attr('href'));
});
$(document).on('submit', 'form#frm', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.is-invalid').removeClass('is-invalid');
            if (data.fail) {
                for (control in data.errors) {
                    $('input[name=' + control + ']').addClass('is-invalid');
                    $('#error-' + control).html(data.errors[control]);
                }
            } else {
                $('#modalForm').modal('hide');
                ajaxLoad(data.redirect_url);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});
function ajaxLoad(filename, content) {

	//console.log("Govind"+filename+content);



    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax({
        type: "GET",
        url: filename,
        contentType: false,
        success: function (data) {

        	console.log(data);

            $("#" + content).html(data);
            $('.loading').hide();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}
function ajaxDelete(filename, token, content) {
    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax({
        type: 'POST',
        data: {_method: 'DELETE', _token: token},
        url: filename,
        success: function (data) {
            $('#modalDelete').modal('hide');
            $("#" + content).html(data);
            $('.loading').hide();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}

function ajaxSaveStock(id) {

console.log("test"+id);

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
var prodid = $("#prodid"+id).val();
var poorderid = $("#poorderid").val();
var totalQty = $("#totalQty"+id).val();
var availqnty = $("#availqnty"+id).val();
$.ajax({
type:'POST',
url:'/ajaxRequest',
data:{prodid:prodid, poorderid:poorderid, totalQty:totalQty, availqnty:availqnty},
success:function(data){
// alert(data.success);
$("#messageSt"+id).html('<i style="color: green">'+data.success+'</i>');
// $("#modalReceived1"+id).modal('toggle'); 
setTimeout(function(){
  $("#modalReceived"+id).modal('hide');
}, 2000);
}
});
}

function ajaxSaveReceivedStock(id) {


$("#stockSt"+id).empty();
$("#modalstockSt"+id).empty();

$("#stockRc"+id).show();
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


var prodid = $("#prodid"+id).val();
var poorderid = $("#poorderid").val();
 

var totalQty = $("#totalQty"+id).val();
var availqnty = $("#availqnty"+id).val();

 if(prodid && poorderid && totalQty && availqnty){

$.ajax({
type:'POST',
url:'/checkStockStatus',
data:{prodid:prodid, poorderid:poorderid, totalQty:totalQty, availqnty:availqnty},
success:function(data){
//alert(data.success);

$("#stockRc"+id).hide();
$("#stockSt"+id).html('<i style="color: green">'+data.success+'</i>');
$("#modalstockSt"+id).html('<i style="color: green">'+data.success+'</i>');


}
});

 return false;
}



}





function ajaxStockReceived(poid) {


$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});



if(poid){

$.ajax({
type:'POST',
url:'/checkStockReceived',
data:{poid:poid},
success:function(data){
 
if(data.statuscode == 0){
    $("#display"+poid).html('<span class="alert alert-danger" style="min-width: 150px; max-width: 150px"><i style="color: green">'+data.success+'</i></span>');
} else if(data.statuscode == 1){
    $("#display"+poid).html('<span class="alert alert-warning" style="min-width: 150px; max-width: 150px"><i style="color: green">'+data.success+'</i></span>');
}else{
   $("#display"+poid).html('<span class="alert alert-success" style="min-width: 150px; max-width: 150px"><i style="color: green">'+data.success+'</i></span>');
}


}
});

 return false;
}



}








function ajaxMail() {
    $("#messageMail").html('&nbsp;');
    $("#error-email").html('&nbsp;');
    $("#error-description").html('&nbsp;');
console.log("testMail");

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});


var mailMultiple = $("#mailMultiple").val();

console.log(mailMultiple);

 

var email = $("input[name='email[]']")
              .map(function(){return $(this).val();}).get();



var poorderid = $("#poorderid").val();
var description = $("#description").val();

// var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
// if (!testEmail.test(email)){
//     console.log("Working");
//     $("#error-email").html('<i style="color: red;text-align: left">Please check if email filled properly !!</i>');
//     return false;
// }

// if(description == ''){
//     $("#error-email").html('&nbsp;');
//     $("#error-description").html('<i style="color: red; text-align: left">Please enter description !!</i>');
//     return false;
// }else{
//         $("#messageMail").html('&nbsp;');
//         $("#error-email").html('&nbsp;');
//         $("#error-description").html('&nbsp;');
// }



$.ajax({
type:'POST',
url:'/ajaxMail',
data:{email:email, poorderid:poorderid, description:description, mailMultiple:mailMultiple},
success:function(data){
// alert(data.success);
$("#messageMail").html('<i style="color: green">'+data.success+'</i>');
// $("#modalReceived1"+id).modal('toggle'); 
$("#email").val();
$("#description").val();
setTimeout(function(){
  $("#myModal").modal('hide');
}, 2000);

$("#messageMail").html();

$("#addMoreEmail").html();

}
});
}

  
                        
function ajaxDeleteMoved(filename, token, content) {
    content = typeof content !== 'undefined' ? content : 'content';
    $('.loading').show();
    $.ajax({
        type: 'POST',
        data: {_method: 'DELETE', _token: token},
        url: filename,
        success: function (data) {
            $('#modalDeleteMoved').modal('hide');
            $('.loading').hide();
             
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}



$(function() {

$('#modalForm').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    ajaxLoad(button.data('href'), 'modal_content');
});

$('#modalDelete').on('show.bs.modal', function (event) {
	 
    var button = $(event.relatedTarget);

    $('#delete_id').val(button.data('id'));
    $('#delete_token').val(button.data('token'));
});


$('#modalDeleteMoved').on('show.bs.modal', function (event) {
     
    var button = $(event.relatedTarget);

    $('#delete_id_moved').val(button.data('id'));
    $('#delete_token_moved').val(button.data('token'));
});


$('#modalForm').on('shown.bs.modal', function () {
    $('#focus').trigger('focus')
});

});




function addMoreEmail(){

$('#addMoreEmail').append('<div class="form-group row required"> <div class="col-md-12"><input placeholder="Email Address" id="emailExtra" name="email[]" type="text" class="form-control" style="width: 94%"> <span id="error-email" style="float: right;"></span><img src="http://ims.projectstatus.in/images/add.png" alt="Add extra email" title="Add extra email" height="20" width="20" style="float: right; cursor: pointer; margin-top: -27px" onclick="addMoreEmail()"></div></div>');

}

// $(document).ready(function(){
// $("#tct2").trigger('click');
// })