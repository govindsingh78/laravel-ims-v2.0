<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/', function()
{
   return View::make('auth.login');
});
Route::get('/about', function()
{
   return View::make('pages.about');
});
Route::get('/contact', function()
{
   return View::make('pages.contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');

Route::get('/manage-vendors-product', 'ManageVendorsProductController@index')->middleware('auth')->name('manage-vendors-product');
Route::get('/purchase-order', 'PurchaseOrderController@index')->middleware('auth')->name('purchase-order');
Route::get('/stock-management', 'StockManagementController@index')->middleware('auth')->name('stock-management');
Route::get('/damaged-stock', 'DamagedStockController@index')->middleware('auth')->name('damaged-stock');


Route::post('addVendor',  'ManageVendorsProductController@storeVendor');



  Route::get('/manage-product-category', 'ManageVendorsProductController@indexProduct')->middleware('auth')->name('manage-product-category');

// Route for managing vendor
Route::group(['prefix' => 'manage-vendor-and-product-category'], function () {
    Route::get('/', 'ManageVendorsProductController@index');
    Route::match(['get', 'post'], 'create', 'ManageVendorsProductController@create');
    Route::match(['get', 'put'], 'update/{id}', 'ManageVendorsProductController@update');
    Route::delete('delete/{id}', 'ManageVendorsProductController@delete');
});

//Routes for managing product

Route::group(['prefix' => 'manage-product-category'], function () {
    Route::get('/product', 'ManageVendorsProductController@indexProduct');
    Route::match(['get', 'post'], 'createProduct', 'ManageVendorsProductController@createProduct');
    Route::match(['get', 'put'], 'updateProduct/{id}', 'ManageVendorsProductController@updateProduct');
    Route::delete('deleteProduct/{id}', 'ManageVendorsProductController@deleteProduct');
});


//Routes for managing brand
Route::get('/manage-brand-category', 'ManageVendorsProductController@indexBrand')->middleware('auth')->name('manage-brand-category');

Route::group(['prefix' => 'manage-brand-category'], function () {
    Route::get('/brand', 'ManageVendorsProductController@indexBrand');
    Route::match(['get', 'post'], 'createBrand', 'ManageVendorsProductController@createBrand');
    Route::match(['get', 'put'], 'updateBrand/{id}', 'ManageVendorsProductController@updateBrand');
    Route::delete('deleteBrand/{id}', 'ManageVendorsProductController@deleteBrand');
});

//Routes for managing specification
Route::get('/manage-specification-category', 'ManageVendorsProductController@indexSpecification')->middleware('auth')->name('manage-specification-category');

Route::group(['prefix' => 'manage-specification-category'], function () {
    Route::get('/specification', 'ManageVendorsProductController@indexSpecification');
    Route::match(['get', 'post'], 'createSpecification', 'ManageVendorsProductController@createSpecification');
    Route::match(['get', 'put'], 'updateSpecification/{id}', 'ManageVendorsProductController@updateSpecification');
    Route::delete('deleteSpecification/{id}', 'ManageVendorsProductController@deleteSpecification');
});



//Routes for managing sitelocation
Route::get('/manage-sitelocation-category', 'ManageVendorsProductController@indexSitelocation')->middleware('auth')->name('manage-sitelocation-category');

Route::group(['prefix' => 'manage-sitelocation-category'], function () {
    Route::get('/sitelocation', 'ManageVendorsProductController@indexSitelocation');
    Route::match(['get', 'post'], 'createSitelocation', 'ManageVendorsProductController@createSitelocation');
    Route::match(['get', 'put'], 'updateSitelocation/{id}', 'ManageVendorsProductController@updateSitelocation');
    Route::delete('deleteSitelocation/{id}', 'ManageVendorsProductController@deleteSitelocation');
});


//Routes for managing gst

Route::get('/manage-gst-category', 'ManageVendorsProductController@indexGst')->middleware('auth')->name('manage-gst-category');


Route::group(['prefix' => 'manage-gst-category'], function () {
    Route::get('/gst', 'ManageVendorsProductController@indexGst');
    Route::match(['get', 'post'], 'createGst', 'ManageVendorsProductController@createGst');
    Route::match(['get', 'put'], 'updateGst/{id}', 'ManageVendorsProductController@updateGst');
    Route::delete('deleteGst/{id}', 'ManageVendorsProductController@deleteGst');
});


//Routes for managing warranties


Route::get('/manage-warranties-category', 'ManageVendorsProductController@indexWarranty')->middleware('auth')->name('manage-warranties-category');

Route::group(['prefix' => 'manage-warranties-category'], function () {
    Route::get('/warranty', 'ManageVendorsProductController@indexWarranty');
    Route::match(['get', 'post'], 'createWarranty', 'ManageVendorsProductController@createWarranty');
    Route::match(['get', 'put'], 'updateWarranty/{id}', 'ManageVendorsProductController@updateWarranty');
    Route::delete('deleteWarranty/{id}', 'ManageVendorsProductController@deleteWarranty');
});



//Routes for managing purchase order


Route::get('/manage-purchase-order', 'PurchaseOrderController@indexPurchaseOrder')->middleware('auth')->name('manage-purchase-order');

Route::group(['prefix' => 'manage-purchase-order'], function () {
    Route::get('/purchase-order', 'PurchaseOrderController@indexPurchaseOrder');
    Route::match(['get', 'post'], 'createPurchaseOrder', 'PurchaseOrderController@createPurchaseOrder');
    Route::match(['get', 'put'], 'updatePurchaseOrder/{id}', 'PurchaseOrderController@updatePurchaseOrder');
    Route::delete('deletePurchaseOrder/{id}', 'PurchaseOrderController@deletePurchaseOrder');
});




### Routing for Cart Starts ###
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@add')->name('cart.add');
Route::post('/cart/conditions','CartController@addCondition')->name('cart.addCondition');
Route::delete('/cart/conditions','CartController@clearCartConditions')->name('cart.clearCartConditions');
Route::get('/cart/details','CartController@details')->name('cart.details');
Route::delete('/cart/{id}','CartController@delete')->name('cart.delete');
### Routing for Cart Ends ###



// Modifed


Route::get('admin-login', 'Auth\AdminLoginController@showLoginForm');

Route::post('admin-login', ['as'=>'admin-login','uses'=>'Auth\AdminLoginController@login']);


### Routing for Cart Starts ###
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@add')->name('cart.add');
Route::post('/cart/conditions','CartController@addCondition')->name('cart.addCondition');
Route::delete('/cart/conditions','CartController@clearCartConditions')->name('cart.clearCartConditions');
Route::get('/cart/details','CartController@details')->name('cart.details');
Route::delete('/cart/{id}','CartController@delete')->name('cart.delete');
### Routing for Cart Ends ###


Route::group(['prefix' => 'wishlist'],function()
{
    Route::get('/','WishListController@index')->name('wishlist.index');
    Route::post('/','WishListController@add')->name('wishlist.add');
    Route::get('/details','WishListController@details')->name('wishlist.details');
    // Route::get('saveinvoice', ['as' => 'saveinvoice', 'uses' => 'WishListController@saveinvoice']);
    
     Route::post('saveinvoice', ['as' => 'saveinvoice', 'uses' => 'WishListController@saveinvoice']);
    Route::post('movetostock/{id}', ['as' => 'movetostock', 'uses' => 'WishListController@movetostock']);
    // Route::post('/saveinvoice','WishListController@saveinvoice')->name('wishlist.saveinvoice'); 
    Route::delete('/{id}','WishListController@delete')->name('wishlist.delete');
});
