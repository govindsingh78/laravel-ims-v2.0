<div class="container">
    
    
   
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4 form-group pull-left">
        <h1 style="font-size: 1.3rem">Site Location List</h1>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-sitelocation-category')}}?search='+this.value)"
                       placeholder="Search by Site Name" name="search"
                       type="text" id="search"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-sitelocation-category')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
        <a href="#modalForm" data-toggle="modal" data-href="{{url('manage-sitelocation-category/createSitelocation')}}"
        class="btn btn-success">Add New Site</a>
        </div>
    </div>
    </div>
    </div>
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=site&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Site
                </a>
                {{request()->session()->get('field')=='site'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
             <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=floor&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Floor
                </a>
                {{request()->session()->get('field')=='floor'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=location&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Location
                </a>
                {{request()->session()->get('field')=='location'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-sitelocation-category?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
        @endphp
        @foreach($sitelocation as $siteloc)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">{{ $siteloc->site }}</td>
                <td style="vertical-align: middle">{{$siteloc->floor}}</td>
                <td style="vertical-align: middle">{{ $siteloc->location }}</td>
                
                 <td style="vertical-align: middle; font-style: italic;">{{ $siteloc->status == 0 ? 'Inactive' : 'Active' }}</td>
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($siteloc->created_at))}}</td>
                <td style="vertical-align: middle" align="center">
                    <a class="btn btn-primary btn-sm" title="Edit" href="#modalForm" data-toggle="modal"
                       data-href="{{url('manage-sitelocation-category/updateSitelocation/'.$siteloc->id)}}">
                        Edit</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$siteloc->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$sitelocation->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>