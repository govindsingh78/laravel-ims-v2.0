@if(isset($gst))
    {!! Form::model($gst,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($gst)?'Edit':'New'}} GST</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row required">
        {!! Form::label("gst_in_percentage","GST in %",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("gst_in_percentage",null,["class"=>"form-control".($errors->has('gst_in_percentage')?" is-invalid":""),'placeholder'=>'GST in %','id'=>'focus']) !!}
            <span id="error-gst_in_percentage" class="invalid-feedback"></span>
        </div>
    </div>
     <div class="form-group row required">
        {!! Form::label("status","GST Status",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="status">
        <option value="">Choose Status</option>
        @if(isset($gst))
              <option value="0" <?php if($gst->status == 0 ){ echo "selected"; } else{ echo ""; } ?>>Inactive</option>
              <option value="1" <?php if($gst->status == 1 ){ echo "selected"; } else{ echo ""; } ?>>Active</option>
                
        @else
               <option value="0" >Inactive</option>
               <option value="1" >Active</option>
                
        @endif

        </select>
          <span id="error-status" class="invalid-feedback"></span>
        </div>
          
        </div>
    
   
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}