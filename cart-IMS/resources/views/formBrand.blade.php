@if(isset($brand))
    {!! Form::model($brand,['method'=>'put','id'=>'frm']) !!}
@else
    {!! Form::open(['id'=>'frm']) !!}
@endif
<div class="modal-header">
    <h5 class="modal-title">{{isset($brand)?'Edit':'New'}} Brand</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="form-group row required">
        {!! Form::label("name","Brand Name",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::text("name",null,["class"=>"form-control".($errors->has('name')?" is-invalid":""),'placeholder'=>'Brand Name','id'=>'focus']) !!}
            <span id="error-name" class="invalid-feedback"></span>
        </div>
    </div>
    
     

     


     <div class="form-group row required">
        {!! Form::label("description","Brand Description",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-md-12">
            {!! Form::textarea("description",null,["class"=>"form-control".($errors->has('description')?" is-invalid":""),'placeholder'=>'Brand Description','id'=>'focus']) !!}
            <span id="error-description" class="invalid-feedback"></span>
        </div>
    </div>


    <div class="form-group row required">
        {!! Form::label("status","Brand Status",["class"=>"col-form-label col-md-12"]) !!}
        <div class="col-sm-12">

        <select class="form-control" name="status">
        <option value="">Choose Status</option>
        @if(isset($brand))
              <option value="0" <?php if($brand->status == 0 ){ echo "selected"; } else{ echo ""; } ?>>Inactive</option>
              <option value="1" <?php if($brand->status == 1 ){ echo "selected"; } else{ echo ""; } ?>>Active</option>
                
        @else
               <option value="0" >Inactive</option>
               <option value="1" >Active</option>
                
        @endif

        </select>
          <span id="error-status" class="invalid-feedback"></span>
        </div>
          
        </div>
		




</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger" data-dismiss="modal"> Close</button>
    {!! Form::submit("Save",["class"=>"btn btn-primary"])!!}
</div>
{!! Form::close() !!}