<div class="container">
    
    
   
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-4 form-group pull-left">
        <h1 style="font-size: 1.3rem">Invoice List</h1>
     </div>

        <div class="col-md-4 form-group pull-right">
            <div class="input-group">
                <input class="form-control" id="search"
                       value="{{ request()->session()->get('search') }}"
                       onkeydown="if (event.keyCode == 13) ajaxLoad('{{url('manage-purchase-order')}}?search='+this.value)"
                       placeholder="Search by Product Name" name="search"
                       type="text" id="search"/>
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-success"
                            onclick="ajaxLoad('{{url('manage-purchase-order')}}?search='+$('#search').val())">
                        Search
                    </button>
                </div>
            </div>
        </div>
         <div class="col-md-2 form-group pull-right">
        <div class="input-group">
        <a href="{{url('wishlist')}}"
        class="btn btn-success">Create New Invoice</a>
        </div>
    </div>
    </div>
    </div>
    
    <table class="table table-bordered bg-light">
        <thead class="bg-dark" style="color: white">
        <tr>
            <th width="60px" style="vertical-align: middle;text-align: center">No.</th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=invoiceid&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Invoice #ID
                </a>
                {{request()->session()->get('field')=='invoiceid'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=stock_status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Stock Status
                </a>
                {{request()->session()->get('field')=='stock_status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=status&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                    Invoice Status
                </a>
                {{request()->session()->get('field')=='status'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            
           
            <th style="vertical-align: middle">
                <a href="javascript:ajaxLoad('{{url('manage-purchase-order?field=created_at&sort='.(request()->session()->get('sort')=='asc'?'desc':'asc'))}}')">
                   Invoice Date
                </a>
                {{request()->session()->get('field')=='created_at'?(request()->session()->get('sort')=='asc'?'':''):''}}
            </th>
            <th width="130px" style="vertical-align: middle">Action</th>
        </tr>
        </thead>
        <tbody>
        @php
            $i=1;
             
        @endphp
        @foreach($purchaseOrder as $purchase)
            <tr>
                <th style="vertical-align: middle;text-align: center">{{$i++}}</th>
                <td style="vertical-align: middle">IMS#{{ $purchase->invoiceid }}</td>
                  <td style="vertical-align: middle;  font-style: italic;">{{$purchase->stock_status==1?'Moved to Stock': 'Pending'}}</td>
                <td style="vertical-align: middle">{{$purchase->status==1?'Created': 'Pending'}}</td>
               
                
                <td style="vertical-align: middle">{{date('d-M-Y',strtotime($purchase->created_at))}}</td>
                <td style="vertical-align: middle; display: inline-flex;" align="center">
                    <a class="btn btn-primary btn-sm" title="View"
                       href="{{url('manage-purchase-order/updatePurchaseOrder/'.$purchase->id)}}" style="margin-right: 2px;">
                        View</a>
                    <input type="hidden" name="_method" value="delete"/>
                    <a class="btn btn-danger btn-sm" title="Delete" data-toggle="modal"
                       href="#modalDelete"
                       data-id="{{$purchase->id}}"
                       data-token="{{csrf_token()}}">
                        Delete
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <nav>
        <ul class="pagination justify-content-end">
            {{$purchaseOrder->links('vendor.pagination.bootstrap-4')}}
        </ul>
    </nav>
</div>