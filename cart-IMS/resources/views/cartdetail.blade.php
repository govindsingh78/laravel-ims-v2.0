
<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

     
     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- Styles -->
    <style>
        .cart {
            padding-bottom: 20px;
            padding-top: 20px;
        }
    </style>



</head>
<body>
<!-- <div class="row" id="app">
    <div class="container cart">
        <div class="row">
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>ADD ITEM</h2>
                        <p>(This is using custom database storage)</p>
                        <div class="form-group form-group-sm">
                            <label>ID</label>
                            <input v-model="item.id" class="form-control" placeholder="Id">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Name</label>
                            <input v-model="item.name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Price</label>
                            <input v-model="item.price" class="form-control" placeholder="Price">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Qty</label>
                            <input v-model="item.qty" class="form-control" placeholder="Quantity">
                        </div>
                        <button v-on:click="addItem()" class="btn btn-primary">Add Item</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>ADD CONDITIONS</h2>
                        <div class="form-group form-group-sm">
                            <label>name*</label>
                            <input v-model="cartCondition.name" placeholder="Sale 5%" class="form-control" placeholder="Id">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Type (Any string that defines the type of your condition)*</label>
                            <input v-model="cartCondition.type" placeholder="sale" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Target*</label>
                            <select v-model="cartCondition.target" class="form-control">
                                <option v-for="target in options.target" :key="target.key" :value="target.key">
                                    @{{ target.label }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group form-group-sm">
                            <label>Value*</label>
                            <input v-model="cartCondition.value" placeholder="-12% or -10 or +10 etc" class="form-control" placeholder="Quantity">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <button v-on:click="addCartCondition()" class="btn btn-primary">Add Condition</button>
                    </div>
                    <div class="col-lg-6">
                        <button v-on:click="clearCartCondition()" class="btn btn-primary">Clear Conditions</button>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h2>CART</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>@{{ item.id }}</td>
                        <td>@{{ item.name }}</td>
                        <td>@{{ item.quantity }}</td>
                        <td>@{{ item.price }}</td>
                        <td>
                            <button v-on:click="removeItem(item.id)" class="btn btn-sm btn-danger">remove</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <td>Items on Cart:</td>
                        <td>@{{itemCount}}</td>
                    </tr>
                    <tr>
                        <td>Total Qty:</td>
                        <td>@{{ details.total_quantity }}</td>
                    </tr>
                    <tr>
                        <td>Sub Total:</td>
                        <td>@{{ '$' + details.sub_total.toFixed(2) }} (@{{details.cart_sub_total_conditions_count}} conditions applied)</td>
                    </tr>
                    <tr>
                        <td>Total:</td>
                        <td>@{{ '$' + details.total.toFixed(2) }} (@{{details.cart_total_conditions_count}} conditions applied)</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div> -->




 <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('/images/logo.png')}}" height="30" width="150" alt="Dotsquares Logo" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <!-- New Nav Bars Added for IMS -->
                             
                             <!-- New Nav Bars Added for IMS -->
                              <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    Manage <span class="caret"></span>
                                </a>

                                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('manage-vendors-product') }}">{{ __('Vendor') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-product-category') }}">{{ __('Product') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-brand-category') }}">{{ __('Brand') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-specification-category') }}">{{ __('Specification') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-warranties-category') }}">{{ __('Warranty') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-gst-category') }}">{{ __('GST') }}</a>
                                        <a class="dropdown-item" href="{{ route('manage-sitelocation-category') }}">{{ __('Site Location') }}</a>
                                    
                                  </div>

                                
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('manage-purchase-order') }}">{{ __('Purchase Order') }}</a>
                            </li>
                            <li class="nav-item">
                                    
                                     <a class="nav-link" href="{{ route('stock-management') }}">{{ __('Stock Management') }}</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                  

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('login') }}">{{ __('Dashboard') }}</a>
                                         
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
<div class="row" id="wishlist">
    <div class="container cart">
        <div class="row">
            
            <div class="col-lg-12">
                <h5 style="display: inline-flex; width: 100%">Invoice Detail <span style="float: right; font-style: bold;   margin-left: 100px">  @if(isset($invoiceid)) #IMS {{ $invoiceid }} @endif</span></h5> 


                @if(isset($stock_status))
                        
                    <form  action="{{ url('wishlist/movetostock/'.$invoiceid) }}" method="POST">
                    @csrf
                    @if($stock_status == 0) 
                    <input type="submit" name="submit" value="Move to Stock" class="btn btn-info pull-right" style="float: right; position: absolute; top: -2%;
                    left: 72%;">
                    @else
                    <input class="btn btn-success pull-right" disabled="disabled" style="float: right; position: absolute; top: -2%;
                    left: 75%; width: 100px" value="In Stock"/> 
                    </form>     
                
                        
                @endif
                @endif

              

                <form  action="{{ url('wishlist/printpdf') }}" method="POST">
                @csrf
                <input type="submit" name="submit" value="Generate PDF" class="btn btn-warning pull-right" style="float: right; margin-right: 2px; position: absolute; top: -2%;
    left: 85%;">
                </form>
                <table class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Qty</th>
                        <th>Price</th>
                        <th>GST</th>
                        <!-- <th>Action</th> -->
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in items">
                        <td>#@{{ item.id }}</td>
                        <td>@{{ item.name }} 
                            <br/><i>Brand Name  :  @{{ item.brand }} </i>
                            <br/><i>Vendor Name : @{{ item.vendor }} </i>
                            <br/><i>Warranty in Years : @{{ item.warranty }} <i>
                            <br/><i>Site Location : @{{ item.sitelocation }} </i>

                            
                            <br/><i>Specification : @{{ item.specification }} </i>
                        </td>
                        <td>@{{ item.quantity }}</td>
                        <td>@{{ item.price }}</td>
                        <td>@{{ item.gst }}%</td>
                       <!--  <td>
                            <button v-on:click="removeItem(item.id)" class="btn btn-sm btn-danger">remove</button>
                        </td> -->
                    </tr>
                    </tbody>
                </table>
                <table class="table" style="width: 50%; float: right">
                    <tr>
                        <td>Items Purchased</td>
                        <td>@{{itemCount}}</td>
                    </tr>
                    <tr>
                        <td>Total Qty:</td>
                        <td>@{{ details.total_quantity }}</td>
                    </tr>
                    <tr>
                        <td>Sub Total:</td>
                        <td>@{{ 'INR. ' + details.sub_total.toFixed(2) }}</td>
                    </tr>
                    <tr>
                        <td>Total:</td>
                        <td>@{{ 'INR. ' + details.total.toFixed(2) }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>


 

 <footer class="footer">
        <div class="container" style="text-align: center;">

        <span class="text-muted"><i>&copy; <?php echo date("Y"); ?> Dotsquares IMS. All Rights Reserved. </i></span>
        </div>
        </footer>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
<script src="https://unpkg.com/vue"></script>
<script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script>

    (function($) {

        var _token = '<?php echo csrf_token() ?>';

        $(document).ready(function() {

            var app = new Vue({
                el: '#app',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    itemCount: 0,
                    items: [],
                    item: {
                        id: '',
                        name: '',
                        price: 0.00,
                        qty: 1
                    },
                    cartCondition: {
                        name: '',
                        type: '',
                        target: '',
                        value: '',
                        attributes: {
                            description: 'Value Added Tax'
                        }
                    },

                    options: {
                        target: [                      
                            {label: 'Apply to SubTotal', key: 'subtotal'},
                            {label: 'Apply to Total', key: 'total'}
                        ]
                    }
                },
                mounted:function(){
                    this.loadItems();
                },
                methods: {
                    addItem: function() {

                        var _this = this;

                        this.$http.post('/cart',{
                            _token:_token,
                            id:_this.item.id,
                            name:_this.item.name,
                            price:_this.item.price,
                            qty:_this.item.qty
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    addCartCondition: function() {

                        var _this = this;

                        this.$http.post('/cart/conditions',{
                            _token:_token,
                            name:_this.cartCondition.name,
                            type:_this.cartCondition.type,
                            target:_this.cartCondition.target,
                            value:_this.cartCondition.value,
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    clearCartCondition: function() {

                        var _this = this;

                        this.$http.delete('/cart/conditions?_token=' + _token).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    removeItem: function(id) {

                        var _this = this;

                        this.$http.delete('/cart/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {

                        var _this = this;

                        this.$http.get('/cart',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {

                        var _this = this;

                        this.$http.get('/cart/details').then(function(success) {
                            _this.details = success.body.data;
                            console.log("test");
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });

            var wishlist = new Vue({
                el: '#wishlist',
                data: {
                    details: {
                        sub_total: 0,
                        total: 0,
                        total_quantity: 0
                    },
                    itemCount: 0,
                    items: [],
                    item: {
                        id: '',
                        name: '',
                        specification: '',
                        brand: '',
                        vendor: '',
                        warranty: '',
                        sitelocation: '',
                        price: 0.00,
                        qty: 1,
                        gst: ''
                    },
                    options: {
                        id: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]

                        
                    },
                      options: {
                        
                        name: [
                            {label: 'AC', key: 'AC'}
                            
                        ]

                      
                    },
                      options: {
                         

                        gst: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    },
                     options: {
                         

                        specification: [
                            {label: '5', key: '5'},
                            {label: '10', key: '10'},
                            {label: '12', key: '10'},
                            {label: '18', key: '18'}
                        ]
                    }
                },
                mounted:function(){
                    this.loadItems();
                },
                methods: {
                    addItem: function() {

                        var _this = this;

                        this.$http.post('/wishlist',{
                            _token:_token,
                            id:_this.item.id,
                            name:_this.item.name,
                            specification:_this.item.specification,
                            brand:_this.item.brand,
                            vendor:_this.item.vendor,
                            warranty:_this.item.warranty,
                            sitelocation:_this.item.sitelocation,
                            price:_this.item.price,
                            qty:_this.item.qty,
                            gst:_this.item.gst
                        }).then(function(success) {
                            _this.loadItems();



                        }, function(error) {
                            console.log(error);
                        });
                    },
                    removeItem: function(id) {

                        var _this = this;

                        this.$http.delete('/wishlist/'+id,{
                            params: {
                                _token:_token
                            }
                        }).then(function(success) {
                            _this.loadItems();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadItems: function() {

                        var _this = this;

                        this.$http.get('/wishlist',{
                            params: {
                                limit:10
                            }
                        }).then(function(success) {
                            _this.items = success.body.data;
                            _this.itemCount = success.body.data.length;
                            _this.loadCartDetails();
                        }, function(error) {
                            console.log(error);
                        });
                    },
                    loadCartDetails: function() {

                        var _this = this;

                        this.$http.get('/wishlist/details').then(function(success) {
                            _this.details = success.body.data;
                        }, function(error) {
                            console.log(error);
                        });
                    }
                }
            });

        });

    })(jQuery);
</script>
</body>
</html>



<?php
// echo "<pre>";
// var_dump((session()->get('88uuiioo99888_cart_items')));

?>
