<?php
namespace App\Http\Controllers;
use App\ManageVendorsProduct;

use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
class ManageVendorsProductController extends Controller
{



     public function __construct()
    {
        $this->middleware('auth');
    }


   ###### ---- function used for managing vendors category starts ---- ######

    public function index(Request $request)
    {

        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $vendor = new Vendors();
            if(Schema::hasColumn('vendors', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('name') != -1)
                $vendor = $vendor->where('name', $request->session()->get('name'));

                $vendor = $vendor->where('name', 'like', '%' . $request->session()->get('search') . '%')
                ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
                ->paginate(5);

             
            }else{

                    
                    if ($request->session()->get('name') != -1)
                    $vendor = $vendor->where('name', $request->session()->get('name'));

                    $vendor = $vendor->where('name', 'like', '%' . $request->session()->get('search') . '%')
                    ->orderBy('id', $request->session()->get('sort'))
                    ->paginate(5);

            }

      


        if ($request->ajax())
            return view('index', compact('vendor'));
        else
            return view('ajax', compact('vendor'));
    }

    public function create(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('form');
        else {
            $rules = [
                'name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
                'contact_no' => 'required|numeric',
                'status' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $vendor = new Vendors();
            $vendor->name = $request->name;
            $vendor->email = $request->email;
            $vendor->address = $request->address;
            $vendor->contact_no = $request->contact_no;
            $vendor->status = $request->status;
            $vendor->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-vendor-and-product-category')
            ]);
        }  

        
    }

    public function delete($id)
    {
        Vendors::destroy($id);
        return redirect('/manage-vendor-and-product-category');
    }

    public function update(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('form', ['vendor' => Vendors::find($id)]);
        else {
            $rules = [
               'name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
                'contact_no' => 'required|numeric',
                'status' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $vendor = Vendors::find($id);
            $vendor->name = $request->name;
            $vendor->email = $request->email;
            $vendor->address = $request->address;
            $vendor->contact_no = $request->contact_no;
            $vendor->status = $request->status;
            $vendor->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-vendor-and-product-category')
            ]);
        }
    }


 ###### ---- function used for managing vendors category ends ---- ######



 ###### ---- function used for managing product category starts ---- ######

    public function indexProduct(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $product = new Product();





     if(Schema::hasColumn('products', $request->session()->get('field')))  {
                
        if ($request->session()->get('name') != -1)
            $product = $product->where('name', $request->session()->get('name'));

             $product = $product->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
        } else{

             if ($request->session()->get('name') != -1)
            $product = $product->where('name', $request->session()->get('name'));

             $product = $product->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);

        }


        if ($request->ajax())
            return view('indexProduct', compact('product'));
        else
            return view('ajaxProduct', compact('product'));
    }

    public function createProduct(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formProduct');
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $product = new Product();
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
            
            $product->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-product-category')
            ]);
        }  

        
    }

    public function deleteProduct($id)
    {
        Product::destroy($id);
        return redirect('/manage-product-category');
    }

    public function updateProduct(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formProduct', ['product' => Product::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $product = Product::find($id);
            $product->name = $request->name;
            $product->description = $request->description;
            $product->status = $request->status;
             
            $product->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-product-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ######





   ###### ---- function used for managing brand category starts ---- ######

    public function indexBrand(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $brand = new brand();



         if(Schema::hasColumn('brands', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('name') != -1)
            $brand = $brand->where('name', $request->session()->get('name'));

        $brand = $brand->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);

             
            }else{

                  if ($request->session()->get('name') != -1)
            $brand = $brand->where('name', $request->session()->get('name'));

        $brand = $brand->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }



      



        if ($request->ajax())
            return view('indexBrand', compact('brand'));
        else
            return view('ajaxBrand', compact('brand'));
    }

    public function createBrand(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formBrand');
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $brand = new brand();
            $brand->name = $request->name;
            $brand->description = $request->description;
            $brand->status = $request->status;
            
            $brand->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-brand-category')
            ]);
        }  

        
    }

    public function deleteBrand($id)
    {
        brand::destroy($id);
        return redirect('/manage-brand-category');
    }

    public function updateBrand(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formBrand', ['brand' => brand::find($id)]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $brand = brand::find($id);
            $brand->name = $request->name;
            $brand->description = $request->description;
            $brand->status = $request->status;
             
            $brand->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-brand-category')
            ]);
        }
    }


###### ---- function used for managing brand category Ends ---- ###### 






     ###### ---- function used for managing specification category starts ---- ######

    public function indexSpecification(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('name', $request->has('name') ? $request->get('name') : ($request->session()->has('name') ? $request->session()->get('name') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $specification = new specification();
         $product = Product::all();
       




             if(Schema::hasColumn('specifications', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('name') != -1)
            $specification = $specification->where('name', $request->session()->get('name'));

        $specification = $specification->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);

             
            }else{

                 if ($request->session()->get('name') != -1)
            $specification = $specification->where('name', $request->session()->get('name'));

        $specification = $specification->where('name', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }


        if ($request->ajax())
            return view('indexSpecification', compact('specification', 'product'));
        else
            return view('ajaxSpecification', compact('specification', 'product'));
    }

    public function createSpecification(Request $request)
    {
     
     //echo "create"; die;
         $product = Product::all();
         
        if ($request->isMethod('get'))
            return view('formSpecification', compact('product'));
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'related_to_product_id' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $specification = new specification();
            $specification->name = $request->name;
            $specification->description = $request->description;
            $specification->related_to_product_id = $request->related_to_product_id;
            $specification->status = $request->status;
            
            $specification->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-specification-category')
            ]);
        }  

        
    }

    public function deleteSpecification($id)
    {
        specification::destroy($id);
        return redirect('/manage-specification-category');
    }

    public function updateSpecification(Request $request, $id)
    {

         
        if ($request->isMethod('get'))
            return view('formSpecification', ['specification' => specification::find($id), 'product' => Product::all()]);
        else {
            $rules = [
                'name' => 'required',
                'description' => 'required',
                'related_to_product_id' => 'required',
                'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $specification = specification::find($id);
            $specification->name = $request->name;
            $specification->description = $request->description;
            $specification->related_to_product_id = $request->related_to_product_id;
            $specification->status = $request->status;
            $specification->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-specification-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 


      ###### ---- function used for managing sitelocation category starts ---- ######

    public function indexSitelocation(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('site', $request->has('site') ? $request->get('site') : ($request->session()->has('site') ? $request->session()->get('site') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $sitelocation = new sitelocation();
        


             if(Schema::hasColumn('sitelocations', $request->session()->get('field')))  //check whether users table has email column
            {
                
               if ($request->session()->get('site') != -1)
            $sitelocation = $sitelocation->where('site', $request->session()->get('site'));

        $sitelocation = $sitelocation->where('site', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);

             
            }else{

                if ($request->session()->get('site') != -1)
            $sitelocation = $sitelocation->where('site', $request->session()->get('site'));

        $sitelocation = $sitelocation->where('site', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }




        if ($request->ajax())
            return view('indexSitelocation', compact('sitelocation'));
        else
            return view('ajaxSitelocation', compact('sitelocation'));
    }

    public function createSitelocation(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formSitelocation');
        else {
            $rules = [
                'site' => 'required',
                'location' => 'required',
                'floor' => 'required',
                'status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $sitelocation = new sitelocation();
            $sitelocation->site = $request->site;
            $sitelocation->location = $request->location;
            $sitelocation->floor = $request->floor;
            $sitelocation->status = $request->status;
            
            $sitelocation->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-sitelocation-category')
            ]);
        }  

        
    }

    public function deleteSitelocation($id)
    {
        sitelocation::destroy($id);
        return redirect('/manage-sitelocation-category');
    }

    public function updateSitelocation(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formSitelocation', ['sitelocation' => sitelocation::find($id)]);
        else {
            $rules = [
                'site' => 'required',
                'location' => 'required',
                'floor' => 'required',
                 'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $sitelocation = sitelocation::find($id);
            $sitelocation->site = $request->site;
            $sitelocation->location = $request->location;
            $sitelocation->floor = $request->floor;
            $sitelocation->status = $request->status;
            $sitelocation->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-sitelocation-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 


 ###### ---- function used for managing gst category starts ---- ######

    public function indexGst(Request $request)
    {




        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('gst_in_percentage', $request->has('gst_in_percentage') ? $request->get('gst_in_percentage') : ($request->session()->has('gst_in_percentage') ? $request->session()->get('gst_in_percentage') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $gst = new gst();
      

          if(Schema::hasColumn('gsts', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('gst_in_percentage') != -1)
            $gst = $gst->where('gst_in_percentage', $request->session()->get('gst_in_percentage'));

        $gst = $gst->where('gst_in_percentage', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                 if ($request->session()->get('gst_in_percentage') != -1)
            $gst = $gst->where('gst_in_percentage', $request->session()->get('gst_in_percentage'));

        $gst = $gst->where('gst_in_percentage', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }

        if ($request->ajax())
            return view('indexGst', compact('gst'));
        else
            return view('ajaxGst', compact('gst'));
    }

    public function createGst(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formGst');
        else {
            $rules = [
                'gst_in_percentage' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gst = new gst();
            $gst->gst_in_percentage = $request->gst_in_percentage;
            $gst->status = $request->status;
                    
            $gst->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-gst-category')
            ]);
        }  

        
    }

    public function deleteGst($id)
    {
        gst::destroy($id);
        return redirect('/manage-gst-category');
    }

    public function updateGst(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formGst', ['gst' => gst::find($id)]);
        else {
            $rules = [
                'gst_in_percentage' => 'required',
                'status' => 'required',
                 
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $gst = gst::find($id);
             
            $gst->gst_in_percentage = $request->gst_in_percentage;
            $gst->status = $request->status;
           
            $gst->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-gst-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 


    ###### ---- function used for managing warranties category starts ---- ######

    public function indexWarranty(Request $request)
    {
        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('warranty_in_years', $request->has('warranty_in_years') ? $request->get('warranty_in_years') : ($request->session()->has('warranty_in_years') ? $request->session()->get('warranty_in_years') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));
        $warranty = new warranty();
      

             if(Schema::hasColumn('warranties', $request->session()->get('field')))  //check whether users table has email column
            {
                
                 if ($request->session()->get('warranty_in_years') != -1)
            $warranty = $warranty->where('warranty_in_years', $request->session()->get('warranty_in_years'));

        $warranty = $warranty->where('warranty_in_years', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
            ->paginate(5);
             
            }else{

                   if ($request->session()->get('warranty_in_years') != -1)
            $warranty = $warranty->where('warranty_in_years', $request->session()->get('warranty_in_years'));

        $warranty = $warranty->where('warranty_in_years', 'like', '%' . $request->session()->get('search') . '%')
            ->orderBy('id', $request->session()->get('sort'))
            ->paginate(5);
            }


        if ($request->ajax())
            return view('indexWarranty', compact('warranty'));
        else
            return view('ajaxWarranty', compact('warranty'));
    }

    public function createWarranty(Request $request)
    {
     
     //echo "create"; die;
        
        if ($request->isMethod('get'))
            return view('formWarranty');
        else {
            $rules = [
                'warranty_in_years' => 'required',
                'status' => 'required',
                
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $warranty = new warranty();
            $warranty->warranty_in_years = $request->warranty_in_years;
            $warranty->status = $request->status;
                    
            $warranty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('manage-warranties-category')
            ]);
        }  

        
    }

    public function deleteWarranty($id)
    {
        warranty::destroy($id);
        return redirect('/manage-warranties-category');
    }

    public function updateWarranty(Request $request, $id)
    {
        if ($request->isMethod('get'))
            return view('formWarranty', ['warranty' => warranty::find($id)]);
        else {
            $rules = [
                'warranty_in_years' => 'required',
                 'status' => 'required',
                 
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $warranty = warranty::find($id);
             
            $warranty->warranty_in_years = $request->warranty_in_years;
             $warranty->status = $request->status;
            $warranty->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-warranties-category')
            ]);
        }
    }


###### ---- function used for managing product category Ends ---- ###### 



}




 