<?php
namespace App\Http\Controllers;
use App\PurchaseOrder;


use App\Vendors;
use App\Product;

use App\brand;
use App\specification;
use App\sitelocation;
use App\gst;
use App\warranty;
use App\orderinvoices; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;


use Session;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
    {
        $this->middleware('auth');

    }

   ###### ---- function used for managing purchase order starts ---- ######

    public function indexPurchaseOrder(Request $request)
    {


        Session::forget('invoiceid'); 
        Session::forget('88uuiioo99888_cart_items');

        $request->session()->put('search', $request->has('search') ? $request->get('search') : ($request->session()->has('search') ? $request->session()->get('search') : ''));
        $request->session()->put('invoiceid', $request->has('invoiceid') ? $request->get('invoiceid') : ($request->session()->has('invoiceid') ? $request->session()->get('invoiceid') : -1));
        $request->session()->put('field', $request->has('field') ? $request->get('field') : ($request->session()->has('field') ? $request->session()->get('field') : 'created_at'));
        $request->session()->put('sort', $request->has('sort') ? $request->get('sort') : ($request->session()->has('sort') ? $request->session()->get('sort') : 'desc'));


        $purchaseOrder = new orderinvoices();

                  



            if(Schema::hasColumn('invoiceid', $request->session()->get('field')))  //check whether users table has email column
            {
                
                if ($request->session()->get('invoiceid') != -1)
                $purchaseOrder = $purchaseOrder->where('invoiceid', $request->session()->get('invoiceid'));

                $purchaseOrder = $purchaseOrder->where('invoiceid', 'like', '%' . $request->session()->get('search') . '%')
                ->orderBy($request->session()->get('field'), $request->session()->get('sort'))
                ->paginate(5);

             
            }else{

                    
                    if ($request->session()->get('invoiceid') != -1)
                    $purchaseOrder = $purchaseOrder->where('invoiceid', $request->session()->get('invoiceid'));

                    $purchaseOrder = $purchaseOrder->where('invoiceid', 'like', '%' . $request->session()->get('search') . '%')
                    ->orderBy('id', $request->session()->get('sort'))
                    ->paginate(5);

            }

      


        if ($request->ajax())
            return view('indexPurchaseOrder', compact('purchaseOrder'));
        else
            return view('ajaxPurchaseOrder', compact('purchaseOrder'));
    }

    public function createPurchaseOrder(Request $request)
    {
     
        //All Tables Data in order to manage category & their respective values

                $manageVendor  = Vendors::all();
                $manageProduct = Product::all();
                $manageBrand = brand::all();
                $manageSpecification = specification::all();
                $manageSitelocation = sitelocation::all();
                $manageGst = gst::all();
                $manageWarranty = warranty::all(); 



               //dd($manageVendor);

        
        if ($request->isMethod('get'))
            //return view('formPurchaseOrder');
            return view('cart')->with( ['manageVendor' => $manageVendor, 'manageProduct' => $manageProduct, 'manageBrand' => $manageBrand, 'manageSpecification' => $manageSpecification, 'manageSitelocation' => $manageSitelocation,  'manageGst' => $manageGst, 'manageWarranty' => $manageWarranty] );
        else {




            $rules = [

                'product_id' => 'required',
                'vendor_id' => 'required',
                'brand_id' => 'required',
                'specification_ids' => 'required',
                'qty' => 'required',
                'unit_price' => 'required',
                'gst_id' => 'required',
                'total_price' => 'required',
                'warranty_id' => 'required',
                'site_location_id' => 'required',
                'stock_status' => 'required',
                
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);


                //Here we will be manipulating respective values
                $nameVendor  = Vendors::find($request->vendor_id);
                $nameProduct = Product::find($request->product_id);
                $nameBrand = brand::find($request->brand_id);
                $nameSitelocation = sitelocation::find($request->site_location_id);
                $valueGst = gst::find($request->gst_id);
                $valueWarranty = warranty::find($request->warranty_id); 
               // $valueSpecification = specification::find($request->specification_ids); 

                $purchaseOrder = new PurchaseOrder();
                $purchaseOrder->product_id = $request->product_id;
                $purchaseOrder->product_name = $nameProduct->name;
                $purchaseOrder->vendor_id = $request->vendor_id;
                $purchaseOrder->vendor_name = $nameVendor->name;
                $purchaseOrder->brand_id = $request->brand_id;
                $purchaseOrder->brand_name = $nameBrand->name;

                $purchaseOrder->specification_ids = $request->specification_ids;
                $purchaseOrder->qty = $request->qty;

                $purchaseOrder->unit_price = $request->unit_price;


                $purchaseOrder->gst_id = $request->gst_id;
                $purchaseOrder->gst_value = $valueGst->gst_in_percentage;

                
                $purchaseOrder->total_price = $request->total_price;
                $purchaseOrder->warranty_id = $request->warranty_id;
                $purchaseOrder->warranty_value = $valueWarranty->warranty_in_years;
                $purchaseOrder->site_location_id = $request->site_location_id;
                $purchaseOrder->site_location_name = $nameSitelocation->site." ".$nameSitelocation->floor." ".$nameSitelocation->location;
                $purchaseOrder->stock_status = $request->stock_status;
                
                $purchaseOrder->save();

            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }  

        
    }

    public function deletePurchaseOrder($id)
    {
        orderinvoices::destroy($id);
        return redirect('/manage-purchase-order');
    }

    public function updatePurchaseOrder(Request $request, $id)
    {



        //get the session_var and unserialise and the set the session
        $orderinvoices  = orderinvoices::find($id);
        


        $session_var = unserialize($orderinvoices -> session_var);
        Session::put('invoiceid', $orderinvoices->invoiceid);
        Session::put('88uuiioo99888_cart_items', $session_var);


               


        if ($request->isMethod('get'))
            return view('cartdetail', ['purchaseOrder' => PurchaseOrder::find($id), 'manageVendor' => Vendors::all(), 'manageProduct' => Product::all(), 'manageBrand' => brand::all(), 'manageSpecification' => specification::all(), 'manageSitelocation' => sitelocation::all(),  'manageGst' => gst::all(),  'manageWarranty' => warranty::all(), 'invoiceid' => $orderinvoices->invoiceid,  'stock_status' => $orderinvoices->stock_status]);

        else {
            $rules = [
              
                'stock_status' => 'required',
               
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails())
                return response()->json([
                    'fail' => true,
                    'errors' => $validator->errors()
                ]);
            $purchaseOrder = PurchaseOrder::find($id);
            $purchaseOrder->stock_status = $request->stock_status;
            $purchaseOrder->save();
            return response()->json([
                'fail' => false,
                'redirect_url' => url('/manage-purchase-order')
            ]);
        }
    }


 ###### ---- function used for managing purchase order  ends ---- ######
}
