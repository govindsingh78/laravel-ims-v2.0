<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Auth\LoginController@index');
Auth::routes();
Route::get('/home', 'PurchaseOrderController@indexPurchaseOrder')->middleware('auth')->name('home');
Route::get('/manage-vendors-product', 'ManageVendorsProductController@index')->middleware('auth')->name('manage-vendors-product');
Route::get('/purchase-order', 'PurchaseOrderController@index')->middleware('auth')->name('purchase-order');
Route::get('/stock-management', 'StockManagementController@index')->middleware('auth')->name('stock-management');
Route::get('/damaged-stock', 'DamagedStockController@index')->middleware('auth')->name('damaged-stock');
Route::post('addVendor',  'ManageVendorsProductController@storeVendor');
Route::get('/manage-product-category', 'ManageVendorsProductController@indexProduct')->middleware('auth')->name('manage-product-category');
// Route for managing vendor
Route::group(['prefix' => 'manage-vendor-and-product-category'], function () {
Route::get('/', 'ManageVendorsProductController@index');
Route::match(['get', 'post'], 'create', 'ManageVendorsProductController@create');
Route::match(['get', 'put'], 'update/{id}', 'ManageVendorsProductController@update');
Route::delete('delete/{id}', 'ManageVendorsProductController@delete');
});
//Routes for managing product
Route::group(['prefix' => 'manage-product-category'], function () {
Route::get('/product', 'ManageVendorsProductController@indexProduct');
Route::match(['get', 'post'], 'createProduct', 'ManageVendorsProductController@createProduct');
Route::match(['get', 'put'], 'updateProduct/{id}', 'ManageVendorsProductController@updateProduct');
Route::delete('deleteProduct/{id}', 'ManageVendorsProductController@deleteProduct');
});
//Routes for managing brand
Route::get('/manage-brand-category', 'ManageVendorsProductController@indexBrand')->middleware('auth')->name('manage-brand-category');
Route::group(['prefix' => 'manage-brand-category'], function () {
Route::get('/brand', 'ManageVendorsProductController@indexBrand');
Route::match(['get', 'post'], 'createBrand', 'ManageVendorsProductController@createBrand');
Route::match(['get', 'put'], 'updateBrand/{id}', 'ManageVendorsProductController@updateBrand');
Route::delete('deleteBrand/{id}', 'ManageVendorsProductController@deleteBrand');
});
//Routes for managing specification
Route::get('/manage-specification-category', 'ManageVendorsProductController@indexSpecification')->middleware('auth')->name('manage-specification-category');
Route::group(['prefix' => 'manage-specification-category'], function () {
Route::get('/specification', 'ManageVendorsProductController@indexSpecification');
Route::match(['get', 'post'], 'createSpecification', 'ManageVendorsProductController@createSpecification');
Route::match(['get', 'put'], 'updateSpecification/{id}', 'ManageVendorsProductController@updateSpecification');
Route::delete('deleteSpecification/{id}', 'ManageVendorsProductController@deleteSpecification');
});
//Routes for managing sitelocation
Route::get('/manage-sitelocation-category', 'ManageVendorsProductController@indexSitelocation')->middleware('auth')->name('manage-sitelocation-category');
Route::group(['prefix' => 'manage-sitelocation-category'], function () {
Route::get('/sitelocation', 'ManageVendorsProductController@indexSitelocation');
Route::match(['get', 'post'], 'createSitelocation', 'ManageVendorsProductController@createSitelocation');
Route::match(['get', 'post'], 'addSite', 'ManageVendorsProductController@addSite');
Route::match(['get', 'post'], 'addFloor', 'ManageVendorsProductController@addFloor');
Route::match(['get', 'post'], 'addLocation', 'ManageVendorsProductController@addLocation');
Route::match(['get', 'put'], 'updateSitelocation/{id}', 'ManageVendorsProductController@updateSitelocation');
Route::delete('deleteSitelocation/{id}', 'ManageVendorsProductController@deleteSitelocation');
});
//Routes for managing gst
Route::get('/manage-gst-category', 'ManageVendorsProductController@indexGst')->middleware('auth')->name('manage-gst-category');
Route::group(['prefix' => 'manage-gst-category'], function () {
Route::get('/gst', 'ManageVendorsProductController@indexGst');
Route::match(['get', 'post'], 'createGst', 'ManageVendorsProductController@createGst');
Route::match(['get', 'put'], 'updateGst/{id}', 'ManageVendorsProductController@updateGst');
Route::delete('deleteGst/{id}', 'ManageVendorsProductController@deleteGst');
});
############# GST Setting Tab ##############
Route::get('/manage-gst-setting', 'ManageVendorsProductController@indexGstSetting')->middleware('auth')->name('manage-gst-setting');
Route::group(['prefix' => 'manage-gst-setting'], function () {
Route::get('/gstSetting', 'ManageVendorsProductController@indexGstSetting');
Route::match(['get', 'post'], 'createGstSetting', 'ManageVendorsProductController@createGstSetting');
Route::match(['get', 'put'], 'updateGstSetting/{id}', 'ManageVendorsProductController@updateGstSetting');
Route::delete('deleteGstSetting/{id}', 'ManageVendorsProductController@deleteGstSetting');
});
############# GST Setting Tab ##############
//Routes for managing warranties
Route::get('/manage-warranties-category', 'ManageVendorsProductController@indexWarranty')->middleware('auth')->name('manage-warranties-category');
Route::group(['prefix' => 'manage-warranties-category'], function () {
Route::get('/warranty', 'ManageVendorsProductController@indexWarranty');
Route::match(['get', 'post'], 'createWarranty', 'ManageVendorsProductController@createWarranty');
Route::match(['get', 'put'], 'updateWarranty/{id}', 'ManageVendorsProductController@updateWarranty');
Route::delete('deleteWarranty/{id}', 'ManageVendorsProductController@deleteWarranty');
});
//Routes for managing purchase order
Route::get('/manage-purchase-order', 'PurchaseOrderController@indexPurchaseOrder')->middleware('auth')->name('manage-purchase-order');
Route::group(['prefix' => 'manage-purchase-order'], function () {
Route::get('/purchase-order', 'PurchaseOrderController@indexPurchaseOrder');
Route::match(['get', 'post'], 'createPurchaseOrder', 'PurchaseOrderController@createPurchaseOrder');
Route::match(['get', 'put'], 'updatePurchaseOrder/{id}', 'PurchaseOrderController@updatePurchaseOrder');
// Route::match(['get', 'put'], 'poforApproval/{id}', 'PurchaseOrderController@poforApproval');
Route::match(['get', 'put'], 'updateajaxPurchaseOrder/{id}', 'PurchaseOrderController@updateajaxPurchaseOrder');
Route::post('generate-pdf','PurchaseOrderController@generatePDF');
Route::delete('deletePurchaseOrder/{id}', 'PurchaseOrderController@deletePurchaseOrder');
});
 
Route::group(['prefix' => 'manage-approval-process'], function () {
 
	Route::match(['get', 'put'], 'poforApproval/{id}', 'PurchaseOrderApprovalController@poforApproval');
	Route::match(['get', 'post'], 'approvePurchase', 'PurchaseOrderApprovalController@approvePurchase');
	Route::match(['get', 'post'], 'rejectPurchase', 'PurchaseOrderApprovalController@rejectPurchase');
	 
 
});
### Routing for Cart Starts ###
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@add')->name('cart.add');
Route::post('/cart/conditions','CartController@addCondition')->name('cart.addCondition');
Route::delete('/cart/conditions','CartController@clearCartConditions')->name('cart.clearCartConditions');
Route::get('/cart/details','CartController@details')->name('cart.details');
Route::delete('/cart/{id}','CartController@delete')->name('cart.delete');
### Routing for Cart Ends ###
Route::get('admin-login', 'Auth\AdminLoginController@showLoginForm');
Route::post('admin-login', ['as'=>'admin-login','uses'=>'Auth\AdminLoginController@login']);
### Routing for Cart Starts ###
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@add')->name('cart.add');
Route::post('/cart/conditions','CartController@addCondition')->name('cart.addCondition');
Route::delete('/cart/conditions','CartController@clearCartConditions')->name('cart.clearCartConditions');
Route::get('/cart/details','CartController@details')->name('cart.details');
Route::delete('/cart/{id}','CartController@delete')->name('cart.delete');
### Routing for Cart Ends ###
Route::group(['prefix' => 'wishlist'],function(){
Route::get('/','WishListController@index')->name('wishlist.index');
Route::post('/','WishListController@add')->name('wishlist.add');
Route::get('/details','WishListController@details')->name('wishlist.details');
Route::post('saveinvoice', ['as' => 'saveinvoice', 'uses' => 'WishListController@saveinvoice']);
Route::post('movetostock/{id}', ['as' => 'movetostock', 'uses' => 'WishListController@movetostock']);
Route::delete('/{id}','WishListController@delete')->name('wishlist.delete');
});
############ Stock History Management #################
Route::get('/manage-stock-history', 'ManageStockController@viewStockMovement/{id}')->middleware('auth')->name('manage-stock-history');
Route::group(['prefix' => 'manage-stock-history'], function () {
Route::match(['get', 'post'], 'viewStockMovement/{id}', 'ManageStockController@viewStockMovement');
Route::delete('deleteStockMoved/{id}', 'ManageStockController@deleteStockMoved');
});
############# Stock History Management #################
############ Stock Management #################
Route::get('/manage-stock', 'ManageStockController@indexStock')->middleware('auth')->name('manage-stock');
Route::group(['prefix' => 'manage-stock'], function () {
Route::get('/view-stock', 'ManageStockController@indexStock');
Route::match(['get', 'post'], 'viewStockMovement/{id}', 'ManageStockController@viewStockMovement');
Route::match(['get', 'post'], 'createStock', 'ManageStockController@createStock');
Route::match(['get', 'put'], 'updateStock/{id}', 'ManageStockController@updateStock');
Route::match(['get', 'post'], 'addStockItem', 'ManageStockController@addStockItem');
Route::match(['get', 'put'], 'updateStockItem/{id}', 'ManageStockController@updateStockItem');
Route::delete('deleteStockItem/{id}', 'ManageStockController@deleteStockItem');
Route::delete('deleteStock/{id}', 'ManageStockController@deleteStock');
Route::delete('deleteStockMoved/{id}', 'ManageStockController@deleteStockMoved');
});
############# Stock Management #################
############ Site Floor Location ###############
Route::get('api/dependent-dropdown','APIController@index');
Route::post('api/dependent-dropdown','APIController@index');
Route::get('api/get-floor-list','APIController@getFloorsList');
Route::post('api/get-floor-list/{id}','APIController@getFloorsList');
Route::get('api/get-location-list','APIController@getLocationsList');
Route::post('api/get-location-list','APIController@getLocationsList');
############ Site Floor Location ###############
Route::get('/sites', 'APIController@getSites');
Route::get('/floors/{id}', 'APIController@getFloors');
Route::get('/locations/{id}', 'APIController@getLocations');
Route::get('/companies', 'APIController@getCompanies');
Route::get('/companiesdetails/{id}', 'APIController@getCompaniesDetails');
Route::get('/vendors', 'APIController@getVendors');
Route::get('/vendoraddresses/{id}', 'APIController@getVendorsDetails');
Route::get('ajaxRequest', 'ManageStockController@ajaxRequest');
Route::post('ajaxRequest', 'ManageStockController@ajaxRequestPost');


Route::get('checkStockStatus', 'ManageStockController@checkStockStatus');
Route::post('checkStockStatus', 'ManageStockController@checkStockStatus');

Route::get('checkAjaxforPoid', 'ManageStockController@checkAjaxforPoid');
Route::post('checkAjaxforPoid', 'ManageStockController@checkAjaxforPoid');

Route::get('checkAjaxforStockValidation', 'ManageStockController@checkAjaxforStockValidation');
Route::post('checkAjaxforStockValidation', 'ManageStockController@checkAjaxforStockValidation');


Route::get('ajaxMail', 'ManageStockController@ajaxMail');
Route::post('ajaxMail', 'ManageStockController@ajaxMail');

Route::get('checkStockReceived', 'ManageStockController@checkStockReceived');
Route::post('checkStockReceived', 'ManageStockController@checkStockReceived');




############# Mail Setting Tab ##############
Route::get('/manage-mail-setting', 'ManageVendorsProductController@indexMail')->middleware('auth')->name('manage-mail-setting');
Route::group(['prefix' => 'manage-mail-setting'], function () {
Route::get('/mailSetting', 'ManageVendorsProductController@indexMail');
Route::match(['get', 'post'], 'createMail', 'ManageVendorsProductController@createMail');
Route::match(['get', 'put'], 'updateMail/{id}', 'ManageVendorsProductController@updateMail');
Route::delete('deleteMail/{id}', 'ManageVendorsProductController@deleteMail');
});
############# Mail Setting Tab ##############



