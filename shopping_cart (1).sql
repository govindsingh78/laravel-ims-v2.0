-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2019 at 03:08 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping_cart`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'LG', 'LG', '1', '2019-06-04 07:49:34', '2019-06-04 07:49:34'),
(2, 'Videocon', 'Videocon', '1', '2019-06-04 07:49:58', '2019-06-04 07:49:58');

-- --------------------------------------------------------

--
-- Table structure for table `capacities`
--

CREATE TABLE `capacities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cart_storage`
--

CREATE TABLE `cart_storage` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cart_data` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `damaged_stocks`
--

CREATE TABLE `damaged_stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE `floors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `name`, `site_id`, `created_at`, `updated_at`) VALUES
(6, '1st Floor', 9, '2019-06-07 02:53:10', '2019-06-07 02:53:10'),
(7, '2nd Floor', 12, '2019-06-07 03:16:28', '2019-06-07 03:16:28'),
(8, '3rd Floor', 13, '2019-06-07 05:07:55', '2019-06-07 05:07:55'),
(9, '1st Floor', 11, '2019-06-07 07:41:21', '2019-06-07 07:41:21'),
(11, '10th Floor', 20, '2019-06-07 07:59:34', '2019-06-07 07:59:34'),
(12, '3rd Floor', 20, '2019-06-12 05:33:40', '2019-06-12 05:33:40'),
(14, '1st Floor', 21, '2019-06-12 05:34:05', '2019-06-12 05:34:05'),
(16, '2nd Floor', 21, '2019-06-12 23:35:22', '2019-06-12 23:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `gsts`
--

CREATE TABLE `gsts` (
  `id` int(10) UNSIGNED NOT NULL,
  `gst_in_percentage` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gsts`
--

INSERT INTO `gsts` (`id`, `gst_in_percentage`, `status`, `created_at`, `updated_at`) VALUES
(1, '10', '1', '2019-06-04 07:52:09', '2019-06-04 07:52:09'),
(2, '12', '1', '2019-06-04 07:52:16', '2019-06-04 07:52:16');

-- --------------------------------------------------------

--
-- Table structure for table `gstsettings`
--

CREATE TABLE `gstsettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gst_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gstsettings`
--

INSERT INTO `gstsettings` (`id`, `company_name`, `gst_no`, `company_address`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dotsquares Pvt. Ltd.', '29STQWERTY12345', 'Jhalana Jawahar Nagar Rajasthan - 313001', '1', '2019-06-21 03:45:35', '2019-06-21 03:45:35'),
(2, 'Demo Pvt. Ltd.', 'DEMOGSTNO1213456', 'Vaishali  Nagar Rajasthan - 302021', '1', '2019-06-21 03:46:40', '2019-06-25 07:54:18');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `floor_id`, `created_at`, `updated_at`) VALUES
(4, 'Cafeteria', 6, '2019-06-07 03:14:06', '2019-06-07 03:14:06'),
(5, 'Canteen', 7, '2019-06-07 03:16:39', '2019-06-07 03:16:39'),
(13, 'parking', 11, '2019-06-07 08:00:03', '2019-06-07 08:00:03'),
(14, 'Developer Department', 6, '2019-06-12 05:29:30', '2019-06-12 05:29:30'),
(15, 'Canteen', 11, '2019-06-12 05:29:59', '2019-06-12 05:29:59'),
(16, 'Cafeteria', 14, '2019-06-12 05:34:41', '2019-06-12 05:34:41'),
(17, 'Canteen', 14, '2019-06-12 05:34:51', '2019-06-12 05:34:51'),
(18, 'Developer Department', 14, '2019-06-12 05:35:03', '2019-06-12 05:35:03'),
(20, 'Canteen New', 11, '2019-06-12 23:38:15', '2019-06-12 23:38:15');

-- --------------------------------------------------------

--
-- Table structure for table `manage_vendors_products`
--

CREATE TABLE `manage_vendors_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(18, '2014_10_12_000000_create_users_table', 1),
(19, '2014_10_12_100000_create_password_resets_table', 1),
(20, '2018_01_13_052648_create_cart_storage_table', 1),
(21, '2019_04_10_045434_create_admins_table', 1),
(22, '2019_04_23_054458_create_manage_vendors_products_table', 1),
(23, '2019_04_23_054714_create_purchase_orders_table', 1),
(24, '2019_04_23_054926_create_stock_managements_table', 1),
(25, '2019_04_23_055026_create_damaged_stocks_table', 1),
(26, '2019_04_24_045329_create_vendors_table', 1),
(27, '2019_04_26_053016_create_products_table', 1),
(28, '2019_04_26_092343_create_brands_table', 1),
(29, '2019_04_26_092407_create_capacities_table', 1),
(30, '2019_04_26_092439_create_warranties_table', 1),
(31, '2019_04_26_092505_create_gsts_table', 1),
(32, '2019_04_26_092528_create_sitelocations_table', 1),
(33, '2019_04_26_092953_create_specifications_table', 1),
(34, '2019_06_04_114423_create_orderinvoices_table', 1),
(35, '2019_06_07_050151_create_site_floor_location_tables', 2),
(36, '2019_06_21_085545_create_gstsettings_table', 3),
(37, '2019_06_27_113945_create_stockdatas_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orderinvoices`
--

CREATE TABLE `orderinvoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `invoicedate` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_var` longtext COLLATE utf8mb4_unicode_ci,
  `stock_status` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orderinvoices`
--

INSERT INTO `orderinvoices` (`id`, `invoicedate`, `invoiceid`, `session_var`, `stock_status`, `status`, `created_at`, `updated_at`) VALUES
(54, NULL, '201749246563', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-30\";}}}}', 0, 1, '2019-06-10 05:05:43', '2019-06-10 05:05:43'),
(56, NULL, '201212586689', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:38:\"Udaipur Pratapnagar 10th Floor parking\";s:5:\"price\";d:10000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:10000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 1, '2019-06-10 08:02:15', '2019-06-10 08:02:15'),
(57, NULL, '201563598634', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:38:\"1 ) 1.5 ton 2 ) Cheap Price 3 ) 3 ton \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:50000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-22\";}}}}', 0, 1, '2019-06-11 05:37:54', '2019-06-11 05:37:54'),
(58, NULL, '201267353709', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 1, '2019-06-11 05:51:36', '2019-06-11 05:51:36'),
(59, NULL, '201662153892', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:38:\"1 ) 1.5 ton 2 ) Cheap Price 3 ) 3 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 0, '2019-06-11 07:36:29', '2019-06-11 07:36:29'),
(60, NULL, '201882322792', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:120000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-23\";}}}}', 0, 1, '2019-06-12 01:06:35', '2019-06-12 01:06:35'),
(61, NULL, '201896312323', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:0:{}}', 0, 1, '2019-06-12 01:44:13', '2019-06-12 01:44:13'),
(62, NULL, '201225464117', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:121212;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-30\";}}}}', 0, 0, '2019-06-12 04:09:50', '2019-06-12 04:09:50'),
(63, NULL, '201591606267', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:0:{}}', 0, 0, '2019-06-13 00:55:47', '2019-06-13 00:55:47'),
(64, NULL, '201615024643', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:10000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 1, '2019-06-13 04:01:36', '2019-06-13 04:01:36'),
(65, NULL, '201806818254', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:38:\"Udaipur Pratapnagar 10th Floor parking\";s:5:\"price\";d:5000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 1, 1, '2019-06-13 06:00:28', '2019-06-13 06:00:28'),
(66, NULL, '201888988157', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1212;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 1, '2019-06-13 06:31:44', '2019-06-13 06:31:44'),
(67, NULL, '201850788914', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1212211;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1212211;s:8:\"quantity\";i:2;s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 0, '2019-06-13 07:56:35', '2019-06-13 07:56:35'),
(68, NULL, '201635838661', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:10000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-18\";}}}}', 0, 1, '2019-06-17 06:50:39', '2019-06-17 06:50:39'),
(69, NULL, '201890307174', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:120000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:120000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 1, 1, '2019-06-18 03:22:28', '2019-06-18 03:22:28'),
(70, NULL, '201213562769', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:120000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:14:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:120000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-29\";}}}}', 0, 1, '2019-06-18 03:40:34', '2019-06-18 03:40:34'),
(71, NULL, '201545910500', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:18:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"2019-06-21\";s:11:\"companyname\";i:2;s:5:\"gstno\";i:2;s:14:\"companyaddress\";i:2;s:13:\"vendoraddress\";i:1;}}}}', 0, 0, '2019-06-21 04:12:34', '2019-06-21 04:12:34'),
(72, NULL, '201801032876', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:1200;s:8:\"quantity\";i:4;s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"22/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:5:\"hhggh\";s:11:\"invoiceidpo\";i:201801032876;s:16:\"gstamountperitem\";d:432;s:12:\"totalperitem\";d:4032;}}i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"23/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:12:\"tewtrewterwe\";s:11:\"invoiceidpo\";i:201801032876;s:16:\"gstamountperitem\";d:1440;s:12:\"totalperitem\";d:13440;}}}}', 0, 0, '2019-06-21 23:33:59', '2019-06-21 23:33:59'),
(73, NULL, '201425816161', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";i:5;s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"22/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:7:\"ewewewe\";s:11:\"invoiceidpo\";i:201425816161;s:16:\"gstamountperitem\";d:600;s:12:\"totalperitem\";d:5600;}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";i:4;s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"22/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:26:\"Need to Call @ 23627627263\";s:11:\"invoiceidpo\";i:201425816161;s:16:\"gstamountperitem\";d:480;s:12:\"totalperitem\";d:4480;}}}}', 1, 1, '2019-06-23 23:36:14', '2019-06-23 23:36:14'),
(74, NULL, '201210810715', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";i:2;s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"29/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:19:\"Call me @ 9 o\'clock\";s:11:\"invoiceidpo\";i:201210810715;s:16:\"gstamountperitem\";d:200;s:12:\"totalperitem\";d:2200;}}}}', 1, 1, '2019-06-24 01:50:34', '2019-06-24 01:50:34'),
(75, NULL, '201804584367', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:100;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"30/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:16:\"Call @4575745495\";s:11:\"invoiceidpo\";i:201804584367;s:16:\"gstamountperitem\";d:10;s:12:\"totalperitem\";d:110;}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:41:\"Jawahar nagar 4 kha 5 1st Floor Cafeteria\";s:5:\"price\";d:100;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"30/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:21:\"Jawahar nagar 4 kha 5\";s:19:\"delivaryinstruction\";s:16:\"Call @4575745495\";s:11:\"invoiceidpo\";i:201804584367;s:16:\"gstamountperitem\";d:24;s:12:\"totalperitem\";d:224;}}}}', 1, 1, '2019-06-24 01:53:58', '2019-06-24 01:53:58'),
(76, NULL, '201394343111', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:28:\"1 ) 1.5 ton 2 ) Cheap Price \";s:5:\"brand\";s:8:\"Videocon\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"29/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:17:\"Call @ 3847347878\";s:11:\"invoiceidpo\";i:201394343111;s:16:\"gstamountperitem\";d:100;s:12:\"totalperitem\";d:1100;}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:2000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"30/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:22:\"Call me @ 495495845948\";s:11:\"invoiceidpo\";i:201394343111;s:16:\"gstamountperitem\";d:400;s:12:\"totalperitem\";d:4400;}}}}', 0, 1, '2019-06-24 05:38:20', '2019-06-24 05:38:20'),
(77, NULL, '201501048666', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:0:\"\";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";i:0;s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"27/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";N;s:11:\"invoiceidpo\";i:201501048666;s:16:\"gstamountperitem\";d:100;s:12:\"totalperitem\";d:1100;}}}}', 0, 0, '2019-06-25 03:20:12', '2019-06-25 03:20:12'),
(78, NULL, '201441160644', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:12:\"1 ) 1.5 ton \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"30/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";N;s:11:\"invoiceidpo\";i:201441160644;s:16:\"gstamountperitem\";d:100;s:12:\"totalperitem\";d:1100;}}}}', 0, 0, '2019-06-25 03:54:53', '2019-06-25 03:54:53'),
(79, NULL, '201703776704', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"2\";s:12:\"sitelocation\";s:27:\"Jhalana 1st Floor Cafeteria\";s:5:\"price\";d:100;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"28/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";N;s:11:\"invoiceidpo\";i:201703776704;s:16:\"gstamountperitem\";d:10;s:12:\"totalperitem\";d:110;}}}}', 0, 0, '2019-06-25 03:55:44', '2019-06-25 03:55:44'),
(80, NULL, '201830743213', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:0:{}}', 0, 0, '2019-06-27 03:33:37', '2019-06-27 03:33:37'),
(81, NULL, '201849787351', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:2:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:7:\"Jhalana\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"22/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:16:\"gggggggggggggggg\";s:11:\"invoiceidpo\";i:201849787351;s:16:\"gstamountperitem\";d:100;s:12:\"totalperitem\";d:1100;}}i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:7:\"Jhalana\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"1\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"22/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 302021\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:16:\"gggggggggggggggg\";s:11:\"invoiceidpo\";i:201849787351;s:16:\"gstamountperitem\";d:100;s:12:\"totalperitem\";d:1100;}}}}', 0, 1, '2019-06-27 03:35:26', '2019-06-27 03:35:26'),
(82, NULL, '201824344246', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:2;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:2;s:4:\"name\";s:14:\"Coffee Machine\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:19:\"Udaipur Pratapnagar\";s:5:\"price\";d:12000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:12;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"23/06/2019\";s:11:\"companyname\";s:20:\"Dotsquares Pvt. Ltd.\";s:5:\"gstno\";s:15:\"29STQWERTY12345\";s:14:\"companyaddress\";s:40:\"Jhalana Jawahar Nagar Rajasthan - 313001\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:19:\"Udaipur Pratapnagar\";s:19:\"delivaryinstruction\";s:12:\"hgshshagahgg\";s:11:\"invoiceidpo\";i:201824344246;s:16:\"gstamountperitem\";d:2880;s:12:\"totalperitem\";d:26880;}}}}', 0, 1, '2019-06-27 03:41:42', '2019-06-27 03:41:42'),
(83, NULL, '201863008580', 'O:32:\"Darryldecode\\Cart\\CartCollection\":1:{s:8:\"\0*\0items\";a:1:{i:1;O:32:\"Darryldecode\\Cart\\ItemCollection\":2:{s:9:\"\0*\0config\";a:6:{s:14:\"format_numbers\";b:0;s:8:\"decimals\";i:0;s:9:\"dec_point\";s:1:\".\";s:13:\"thousands_sep\";s:1:\",\";s:7:\"storage\";s:19:\"App\\DatabaseStorage\";s:6:\"events\";N;}s:8:\"\0*\0items\";a:23:{s:2:\"id\";i:1;s:4:\"name\";s:2:\"AC\";s:13:\"specification\";s:16:\"1 ) Cheap Price \";s:5:\"brand\";s:2:\"LG\";s:6:\"vendor\";s:12:\"Govind Singh\";s:8:\"warranty\";s:1:\"1\";s:12:\"sitelocation\";s:7:\"Jhalana\";s:5:\"price\";d:1000;s:8:\"quantity\";s:1:\"2\";s:3:\"gst\";i:10;s:10:\"attributes\";O:41:\"Darryldecode\\Cart\\ItemAttributeCollection\":1:{s:8:\"\0*\0items\";a:0:{}}s:10:\"conditions\";a:0:{}s:9:\"invoiceid\";s:6:\"#12345\";s:11:\"invoicedate\";s:10:\"23/06/2019\";s:11:\"companyname\";s:14:\"Demo Pvt. Ltd.\";s:5:\"gstno\";s:16:\"DEMOGSTNO1213456\";s:14:\"companyaddress\";s:34:\"Vaishali  Nagar Rajasthan - 302021\";s:13:\"vendoraddress\";s:18:\"44 crowland avenue\";s:16:\"delivarylocation\";s:7:\"Jhalana\";s:19:\"delivaryinstruction\";s:8:\"uuuuuuuu\";s:11:\"invoiceidpo\";i:201863008580;s:16:\"gstamountperitem\";d:200;s:12:\"totalperitem\";d:2200;}}}}', 0, 1, '2019-06-27 23:20:24', '2019-06-27 23:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('govind.singh@dotsquares.com', '$2y$10$ixVEA4IPU3ZYEWnkB9rDYOzLaOpA7mqsEyHapOojoFKlgULUbUXmC', '2019-06-06 23:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AC', 'AC Content', '1', '2019-06-04 07:49:06', '2019-06-04 07:49:06'),
(2, 'Coffee Machine', 'Coffee Machine', '1', '2019-06-04 07:49:19', '2019-06-04 07:49:19');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE `purchase_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specification_ids` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit_price` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gst_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gst_value` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warranty_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warranty_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_location_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_location_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sitelocations`
--

CREATE TABLE `sitelocations` (
  `id` int(10) UNSIGNED NOT NULL,
  `site` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sitelocations`
--

INSERT INTO `sitelocations` (`id`, `site`, `location`, `floor`, `status`, `created_at`, `updated_at`) VALUES
(1, '6 kha 9 Jawahar Nagar Jaipur (Raj) - 302004', 'Cafeteria', '1st Floor', '1', '2019-06-04 07:52:28', '2019-06-04 07:52:28'),
(2, '4 kha 5 Jawahar Nagar Jaipur (Raj) - 302004', 'Canteen', '2nd Floor', '1', '2019-06-04 07:52:38', '2019-06-04 07:52:38'),
(3, '6 kha 9 Jawahar Nagar Jaipur (Raj) - 302004', 'Cafeteria', '1st Floor', '1', '2019-06-06 23:59:54', '2019-06-06 23:59:54'),
(4, '6 kha 9 Jawahar Nagar Jaipur (Raj) - 302004', 'Cafeteria', '1st Floor', '1', '2019-06-07 00:00:24', '2019-06-07 00:00:24');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `id` int(10) UNSIGNED NOT NULL,
  `sortname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`id`, `sortname`, `name`, `created_at`, `updated_at`) VALUES
(9, NULL, 'Jhalana', '2019-06-07 01:18:53', '2019-06-07 01:18:53'),
(20, NULL, 'Udaipur Pratapnagar', '2019-06-07 07:59:14', '2019-06-07 07:59:14'),
(21, NULL, 'Jawahar nagar 4 kha 5', '2019-06-12 05:33:59', '2019-06-12 05:33:59'),
(24, NULL, 'New Site Udaipur', '2019-06-12 23:29:25', '2019-06-12 23:29:25');

-- --------------------------------------------------------

--
-- Table structure for table `specifications`
--

CREATE TABLE `specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_to_product_id` int(11) NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `specifications`
--

INSERT INTO `specifications` (`id`, `name`, `description`, `related_to_product_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Capacity', '1.5 ton', 1, '1', '2019-06-04 07:50:23', '2019-06-04 07:50:23'),
(2, 'Other', 'Cheap Price', 1, '1', '2019-06-04 07:51:37', '2019-06-04 07:51:37'),
(3, 'Capacity', '3 ton', 1, '1', '2019-06-07 05:19:06', '2019-06-07 05:19:06');

-- --------------------------------------------------------

--
-- Table structure for table `stockdatas`
--

CREATE TABLE `stockdatas` (
  `id` int(10) UNSIGNED NOT NULL,
  `poid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiveddate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serialno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warrantyyears` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stockdatas`
--

INSERT INTO `stockdatas` (`id`, `poid`, `receiveddate`, `productname`, `serialno`, `vendorname`, `warrantyyears`, `site`, `floor`, `location`, `created_at`, `updated_at`) VALUES
(2, '56', '2019-06-29', '1', '5767862387218', '1', '2', '9', '6', '14', '2019-06-27 08:23:06', '2019-06-27 08:23:06'),
(4, '54', '2019-07-27', '1', '57678623872189', '1', '2', '9', '6', '14', '2019-07-01 00:35:44', '2019-07-01 00:35:44');

-- --------------------------------------------------------

--
-- Table structure for table `stock_managements`
--

CREATE TABLE `stock_managements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Govind Singh', 'govind.singh@dotsquares.com', NULL, '$2y$10$pBckLGGwiOK/YgKVhNWcRevac/4wpSgjQO0icb7laviF79.Y5rcj.', 'MHmcy0QbNPlmjGZEcKfeKS2qB9YnkATPTljgXo7fcswverRevuL4Ubsa8D1z', '2019-06-04 07:48:24', '2019-06-04 07:48:24');

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `email`, `name`, `address`, `contact_no`, `status`, `created_at`, `updated_at`) VALUES
(1, 'govind.singh@dotsquares.com', 'Govind Singh', '44 crowland avenue', '8769970787', '1', '2019-06-04 07:48:41', '2019-06-04 07:48:41');

-- --------------------------------------------------------

--
-- Table structure for table `warranties`
--

CREATE TABLE `warranties` (
  `id` int(10) UNSIGNED NOT NULL,
  `warranty_in_years` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warranties`
--

INSERT INTO `warranties` (`id`, `warranty_in_years`, `status`, `created_at`, `updated_at`) VALUES
(1, '2', '1', '2019-06-04 07:51:53', '2019-06-04 07:51:53'),
(2, '1', '1', '2019-06-04 07:52:00', '2019-06-04 07:52:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `capacities`
--
ALTER TABLE `capacities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_storage`
--
ALTER TABLE `cart_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_storage_id_index` (`id`);

--
-- Indexes for table `damaged_stocks`
--
ALTER TABLE `damaged_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `floors`
--
ALTER TABLE `floors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sites` (`name`,`site_id`);

--
-- Indexes for table `gsts`
--
ALTER TABLE `gsts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gstsettings`
--
ALTER TABLE `gstsettings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `floors` (`name`,`floor_id`);

--
-- Indexes for table `manage_vendors_products`
--
ALTER TABLE `manage_vendors_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderinvoices`
--
ALTER TABLE `orderinvoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoiceid` (`invoiceid`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitelocations`
--
ALTER TABLE `sitelocations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stockdatas`
--
ALTER TABLE `stockdatas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_managements`
--
ALTER TABLE `stock_managements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vendors_email_index` (`email`);

--
-- Indexes for table `warranties`
--
ALTER TABLE `warranties`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `capacities`
--
ALTER TABLE `capacities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `damaged_stocks`
--
ALTER TABLE `damaged_stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `floors`
--
ALTER TABLE `floors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `gsts`
--
ALTER TABLE `gsts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gstsettings`
--
ALTER TABLE `gstsettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `manage_vendors_products`
--
ALTER TABLE `manage_vendors_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `orderinvoices`
--
ALTER TABLE `orderinvoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sitelocations`
--
ALTER TABLE `sitelocations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `specifications`
--
ALTER TABLE `specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `stockdatas`
--
ALTER TABLE `stockdatas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stock_managements`
--
ALTER TABLE `stock_managements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `warranties`
--
ALTER TABLE `warranties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
